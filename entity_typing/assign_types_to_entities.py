#!/usr/bin/env python

######################################################
# Script to add entity types to MoNTEE relations
#
# Liane Guillou and Sander Bijl de Vroe
# University of Edinburgh, 2021
######################################################

import argparse
import json

def read_figer_heirarchy(file_name):
    """
    Read the entity to FIGER type mapping file and return a dictionary mapping
    from entity string to the first level1 type listed
    :param file_name: FIGER map file
    :return: dictionary of entities to types
    """
    figer_types = {}
    with open(file_name) as f:
        for line in f:
            line = line.rstrip('\n')
            line_elements = line.split('\t')
            entity = line_elements[1]
            e2_type_list = line_elements[2].split(" ")
            e4_type_list = line_elements[4].split(" ")
            try:
                level1_type = e2_type_list[0].split("/")[1] # Get the first type
            except:
                try:
                    level1_type = e4_type_list[0].split("/")[1] # Get the first type
                except:
                    level1_type = "thing"
            figer_types[entity] = level1_type
    return figer_types


def add_entity_types_to_relations(in_file_name, out_file_name, figer_types):
    """
    Read the news_gen.json file linne by line and add types for each relation
    If no type is found, default to "thing"
    :param in_file_name: new_gen.json file containing untyped relations
    :param out_file_name: new_gen.json file with typed relations
    :param figer_types: dictionary of entities to types
    :return: n/a
    """
    with open(in_file_name, 'r') as i, open(out_file_name, 'w') as o:
        for line in i:
            json_line = json.loads(line)
            # Binary relations
            binary_relations = json_line["rels"]
            typed_binary_relations = []
            for relation in binary_relations:
                rel_string = relation["r"]
                rel_string_parts = rel_string.split("::")
                arg1 = rel_string_parts[1].replace("_", " ")
                arg2 = rel_string_parts[2].replace("_", " ")
                arg1_type = figer_types[arg1] if arg1 in figer_types else "thing"
                arg2_type = figer_types[arg2] if arg2 in figer_types else "thing"
                typed_rel_string_parts = rel_string_parts[0:3] + [arg1_type, arg2_type] + rel_string_parts[3:]
                typed_rel_string = "::".join(typed_rel_string_parts)
                typed_binary_relations.append({"r": typed_rel_string})
            # Unary relations
            unary_relations = json_line["rels_unary"]
            typed_unary_relations = []
            for relation in unary_relations:
                rel_string = relation["r"]
                rel_string_parts = rel_string.split("::")
                arg1 = rel_string_parts[1].replace("_", " ")
                arg1_type = figer_types[arg1] if arg1 in figer_types else "thing"
                typed_rel_string_parts = rel_string_parts[0:2] + [arg1_type] + rel_string_parts[2:]
                typed_rel_string = "::".join(typed_rel_string_parts)
                typed_unary_relations.append({"r": typed_rel_string})
            json_line["rels"] = typed_binary_relations
            json_line["rels_unary"] = typed_unary_relations
            o.write(json.dumps(json_line))


def main(args):
    figer_types_hierarchy = read_figer_heirarchy(args.figerMapFile)
    add_entity_types_to_relations(args.inFile, args.outFile, figer_types_hierarchy)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--inFile", help="Input file containing untyped relations (JSON format)")
    parser.add_argument("--outFile", help="Output file containing typed relations (JSON format)")
    parser.add_argument("--figerMapFile", help="Mapping file: entities to FIGER types")
    args = parser.parse_args()

    main(args)