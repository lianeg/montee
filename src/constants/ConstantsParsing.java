package constants;

public class ConstantsParsing {

	// --- Server Resource Consumption ---
	public static int maxMBallowd = 60000; // Increased to deal with memory build-up when writing CoreNLP / parser output
	public static int numThreads = 20;


	// --- Processing level ---
	public static boolean docLevelProcessing = true; // TRUE: process documents, FALSE: process sentence-by-sentence
													// If set to FALSE, can be superceded by includeCoreference or
													// includeTemporal, both of which require document level processing


	// --- Parsing ---
	public static boolean onlineCCGParsing = true; // TRUE: run EasyCCG, FALSE: use the rotating parser
	// Offline parsing (with Rotating parser):
	public static boolean readCCGParsesFromFile = false; // TRUE: read CCG parse trees from file (can be used with either parser)
														// Supercedes the value of onlineCCGParsing
	// Online parsing (with EasyCCG):
	public static String easyCcgParserModelPath = "/Users/liane/Work/Code/entGraph/easyccg_model";


	// --- CoreNLP ---
	public static boolean includeCoreference = false; // Resolve anaphoric reference for personal pronouns (runs CoreNLP's coref system)
	public static boolean useDocDateForTimeResolution = false; // Time expression resolution (if set to true, will used docDate,
																// and reload CoreNLP annotators each time a new docDate is encountered.
																// If false, time resolution will use the fixed date: 2013-02-15)
	public static boolean makeNERDeteministic = false; // (If false) Limit the size of the known lower case words set using property
														// maxAdditionalKnownLCWords set to 0 to prevent ordering issues
														// (i.e. when this is nonzero running on document1 then document2
														// can have different results than running on document2 then document1
	public static boolean readCoreNLPFromFile = false; // Read CoreNLP json objects from file (to be used in conjunction with offline parsing)


	// --- Modal Lexicon ---
	public static boolean tagModals = true; // If true: tag relations governed by modals with a "MOD" tag e.g. MOD__win.1 Arsenal (Arsenal might win)
											// tag those with a negative modal (e.g. definitely not) as "NEG" NEG__win.1 Arsenal (It is impossible that Arsenal wins)
	public static boolean tagAttitudeVerbs = true; // If true: tag relations governed by reporting verbs with a "ATT_SAY" or "ATT_THINK" tag e.g. ATT_SAY__win.1 Arsenal (John said Arsenal won)
	public static boolean tagConditionals = true; // If true: tag relations governed by conditionals (e.g. "if") with a "COND" tag
	public static boolean tagCounterfactuals = true; // If true: tag relations governed by counter factuals (e.g. "had") with a "COUNTER" tag
	public static String lexiconPath = "/Users/liane/Work/Code/Montee/modal_lexicon"; // Path to the lexicon files for modals and reporting verbs


	// --- Relation output ---
	// General settings:
	public static boolean nytRelExtraction = false; // Set to True for relation extraction over NYT data
	public static boolean convToEntityLinked = false;// Must be always false, we do linking separately!
	public static boolean writeDebugString = false; // For debugging only. Should be false when running experiments
	public static boolean onlyNounOrNE = false; // Set to "true" to remove pronouns in case of GG binary relations and all unary relations
	public static boolean excludeExistentialThere = true; // Set to "true" to disallow any unary or binary relation containing existential there (POS tag "Ex")
	public static final boolean lemmatizePred = true; // eaten.might.1 => eat.might.1 // Must be true for normal entailment graphs
	public static boolean lemmatizeArguments = true; // If true, lemmatises arguments: infections -> infection, cats -> cat
	public static boolean removeModifiers = false; // E.g. Spurs managed to win the game
													// False managed.to__(win.1,win.2) spurs game
													// True: (win.1,win.2) spurs game
	public static boolean removebasicEvnetifEEModifer = false; // Was false for TACL experiments
																// False: will add (win.1,win.2) in addition to managed.to__(win.1,win.2)
																// True: will remove (win.1,win.2), leaving only managed.to__(win.1,win.2)
	public static boolean filterUntensed = false;// For deduplicating when a tense is added, eg [receiving; receiving.will]->[receiving.will]
	public static boolean includeTemporal = false; // Include time stamps in relation output. For accurate time stamps set
													// value of useDocDateForTimeResolution to true
	public static boolean includeAuxiliariesInPredicate = false; // Include auxiliariy verbs like has in the predicate
																// E.g. X has been to Y (has.been.1,has.been.to.2) X Y
	public static boolean includeParticlesInPredicate = true; // E.g. grow.up.in vs. grow.in
																// For the sentence: Angela Merkel grew up in Germany)
																// This flag is added for consistency with the old
																// pipeline which did not include particles
	public static boolean includeTenseAndAspect = false; // Extract tense information from main verb in predicate (if False, no tense information will be included)
	public static boolean chainPredicatesOnCopularBe = true;
	public static boolean generateRelationForAdjectiveCopulas = true; // If set to true: handle adjectival copulas that link a second argument to the main verb
																	  // E.g. "Arsenal is capable of winning the match"
																	  // [is.capable.of.2(1:e, 4:e), winning.2(4:e, 6:match), is.capable.1(1:e, 0:arsenal), is.1(1:e, 0:arsenal)]
																	  // generate missing relation --> winning.1 arsenal
																	  // to extract the binary:
																	  // MOD__(win.1,win.2) arsenal match
																	  // Applies to both binaries and unaries

	// Binary relation settings:
	public static boolean acceptOnlyAlphabeticBinaryRelations = true; // If set to true, exclude any binary relation that contains junk in the predicate or arguments
																		// e.g. (?.1,?.2) 1/2 1/2
	public static boolean acceptGGBinary = true; // Accept binary relations that contain two general entities. If set to false, only relations with a named entity will be included
	public static boolean includeBareCopularBinaries = true; // Allow (be.1,be,2) washington guest (e.g. in "Washington was occasionally a guest")
															// Set to true when building "binary relation only" graphs or
															// when processing the Levy dataset
	public static boolean removeCopularBeBinary = true; // False: (be.treatment.1,be.treatment.for.2) vancomycin infection (false)
														// True: (treatment.1,treatment.for.2) vancomycin infection (true)
	public static boolean includeCopularBeForBinaryPresentParticiples = false; // True: (be.visit.1, be.visit.2) Obama Hawaii / False: (visit.1,visit.2) Obama Hawaii ("Obama is/was visiting Hawaii")
																			  // N.B. Setting to True will only have an effect if removeCopularBeBinary is set to False
	public static boolean removeCopularBeBinaryForAdjectiveRels = true; // Example sentence: Caffeine is present in Chocolate (present is an adjective)
																		// True: (present.1,present.in.2) caffeine chocolate
																		// False: (be.present.1,be.present.in.2) caffeine chocolate
	public static boolean replaceLeftBeWithRightPredStringForTwoHops = true; // True: (part.of.1, part.of.2) germany eu
																	// False: (be.1,be.part.of.2) germany eu
																	// Used for Two-Hop relations only
	// Unary relations:
	public static boolean writeUnaryRels = true; // Include unary relations in the output
	public static boolean excludeUnariesFromGLUEParses = false; // Exclude unary relations extracted from sentences in which the GLUE rule appears in parser output
	public static boolean acceptAdjectiveUnary = true; // Set to true to remove unaries like be.funny.2 tom (where "funny" is an adjective)
	public static boolean addTwoHopBinaryCopularAsUnary = true; // Set to "true" to add a unary for a two-hop binary relation involving a copular
																// E.g. Germany is part of the EU -> be.part.of.EU.1 Germany
	public static boolean removeCopularBeUnary = false; // False: be.funny.1 bob / True: funny.1 bob
	public static boolean includeCopularBeForUnaryPresentParticiples = false; // True: be.swim.1 John / False: swim.1.John ("John is/was swimming")
																			 // N.B. Setting to True will only have an effect if removeCopularBeUnary is False

	// --- Levy processing ---
	public static boolean restrictToRelsWithoutModifiers = false; // If set to "true" any relation containing a modifier
																  // will not be considered when extracting the "best"
																  // relation from the Levy dataset

}
