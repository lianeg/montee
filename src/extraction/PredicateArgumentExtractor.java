package extraction;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.collect.Iterables;
import com.google.common.graph.MutableValueGraph;
import com.google.common.graph.ValueGraphBuilder;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import edu.stanford.nlp.coref.CorefCoreAnnotations;
import edu.stanford.nlp.coref.data.CorefChain;
import edu.stanford.nlp.coref.data.Dictionaries;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.time.TimeAnnotations;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.Pair;

import edin.ccg.ParserInterfaceScala;
import edin.ccg.representation.predarg.DepLink;
import edin.ccg.ParseResultForJava;

import constants.ConstantsParsing;
import extraction.parse.*;

import org.apache.commons.lang3.ArrayUtils;
import org.xml.sax.SAXException;
import uk.co.flamingpenguin.jewel.cli.ArgumentValidationException;

import javax.xml.parsers.ParserConfigurationException;

public class PredicateArgumentExtractor implements Runnable {


    String line;
    Boolean docProcessing;

    StanfordCoreNLP corenlppipeline;
    Properties props;

    ParserInterfaceScala ccgParser;

    JsonParser jsonParser = new JsonParser();

    public static HashMap<String, Pair<Integer, String>> modalLexicon = new HashMap<>();
    public static HashMap<String, Pair<Integer, String>> attitudeVerbSayLexicon = new HashMap<>();
    public static HashMap<String, Pair<Integer, String>> attitudeVerbThinkLexicon = new HashMap<>();
    public static HashMap<String, Pair<Integer, String>> conditionalsLexicon = new HashMap<>();
    public static LexiconTrie multiWordLexiconTrie = new LexiconTrie();
    public static HashMap<Integer, Pair<Pair<Integer, String>, Pair<String, String>>> multiWordLexiconProps = new HashMap<>();

    public static Pattern ccgTagFeaturePattern = Pattern.compile("^\\(*S\\[.*");

    public static Map<Integer, Integer> strengthMap = new HashMap<Integer, Integer>() {
        {
            put(0, 0); // Definitely not -> negation
            put(1, 1); // Unlikely -> modal
            put(2, 1); // Possibly -> modal
            put(3, 1); // Probably -> modal
            put(4, 2); // Definitely -> factive
            put(-1, 1); // Deontic -> modal
            put(-2, 1); // Intention -> modal
            put(-3, 1); // Desire -> modal
            put(-4, 1); // Not desire -> modal
        }
    };


    public PredicateArgumentExtractor(String line, Boolean docProcessing) {
        this.line = line;
        this.docProcessing = docProcessing;

        // Create a StanfordCoreNLP object, with POS tagging, lemmatization, and NER
        this.props = new Properties();
        if (ConstantsParsing.makeNERDeteministic) {
            this.props.setProperty("maxAdditionalKnownLCWords", "0");
        }
        // Specify CoreNLP annotators
        if (ConstantsParsing.includeCoreference) {
            this.props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner,parse,coref");
            // Flags for coreference resolution (options: deterministic [rule-based], statistical, neural)
            this.props.setProperty("coref.algorithm", "neural");
        } else {
            this.props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner");
        }
        // Flags for the CoreNLP pipeline (control tokenisation and sentence splitting)
        this.props.setProperty("tokenize.class", "PTBTokenizer");
        this.props.setProperty("tokenize.language", "en");
        this.props.setProperty("ssplit.eolonly", "true");
        // Flags for SU Time
        this.props.setProperty("ner.useSUTime", "true");
        this.props.setProperty("sutime.includeRange", "true"); // Optional
        this.props.setProperty("sutime.markTimeRanges", "true"); // Optional
        this.props.setProperty("ner.docdate.useFixedDate", "2013-02-15"); // First date in the NewsSpike dataset (used as a default value)

        // Create instance of the CoreNLP pipeline
        this.corenlppipeline = new StanfordCoreNLP(props);

        // Create instance of the CCG parser
        // If tokenisation is set, uses the PTB Tokeniser
        if (ConstantsParsing.readCCGParsesFromFile) {
            // Read CCG parses from file - no model required
            this.ccgParser = new ParserInterfaceScala(null, false);
        } else {
            if (ConstantsParsing.onlineCCGParsing) {
                // Online CCG parsing - use the EasyCCG model
                this.ccgParser = new ParserInterfaceScala(ConstantsParsing.easyCcgParserModelPath, false);
            } else {
                // Offline CCG parsing - don't need a CCG parser
                this.ccgParser = null;
            }
        }

        // Read the modals, attitude verbs, and conditionals and form lexicons
        if (ConstantsParsing.tagModals || ConstantsParsing.tagAttitudeVerbs || ConstantsParsing.tagConditionals) {
            readLexiconFile(ConstantsParsing.lexiconPath, "Montee_lexicon");
        }

    }

    public void setLine(String line) {
        this.line = line;
    }


    //	@Override
    public void run() {
        String mainStr;
        String mainStrOnlyNEs;

        try {
            JsonObject obj = (JsonObject) jsonParser.parse(line).getAsJsonObject();
            String text = obj.get("s").getAsString();
            String date = obj.get("date").getAsString();
            long articleId = obj.get("articleId").getAsLong();
            long lineId = obj.get("lineId").getAsLong();
            String coreNLPStr = (obj.has("coreNLPOutput")) ? obj.get("coreNLPOutput").getAsString() : "";
            String ccgParserStr = (obj.has("ccgParserOutput")) ? obj.get("ccgParserOutput").getAsString() : "";
            List<String> ccgParserStrs = Arrays.asList(ccgParserStr.split("\n"));
            String timeML = (obj.has("timeML")) ? obj.get("timeML").getAsString() : "";

            // Update coreNLP pipeline with the new date
            if (ConstantsParsing.useDocDateForTimeResolution) {
                // Convert document date-string to date
                SimpleDateFormat string_to_date = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
                Date dateDate = string_to_date.parse(date);
                // Convert date to StanfordCoreNLP-friendly date-string
                SimpleDateFormat date_to_string = new SimpleDateFormat("yyyy-MM-dd");
                String strDate = date_to_string.format(dateDate);
                if (!props.getProperty("ner.docdate.useFixedDate").equals(strDate)) {
                    props.setProperty("ner.docdate.useFixedDate", strDate);
                    corenlppipeline = new StanfordCoreNLP(props);
                }
            }

            // Split the document into a list of sentences
            List<String> textList = Arrays.asList(text.split("\n"));

            // Extract predicate argument strings for each sentence in the document
            // Also extract the coreNLP output (empty string if readCoreNLPFromFile set to true)
            Pair<List<String[]>, Pair<Pair<String, String>, String>> result = extractPredArgsStrs(textList, coreNLPStr, ccgParserStrs, timeML);
            List<String[]> predArgStrs = result.first;
            String mergedEntitySentences = result.second.first.first;
            String coreNLPOutput = result.second.first.second;
            String ccgParserOutput = result.second.second;

            // Output results for each sentence in the document, in order
            for (int i = 0; i < predArgStrs.size(); i++) {
                String[] predArgStr = predArgStrs.get(i);
                mainStr = "";

                if (ConstantsParsing.nytRelExtraction) {
                    mainStr += "#line: " + predArgStr[5];
                }
                else {
                    mainStr += "#line: " + textList.get(i) + "\n";
                }

                if (!ConstantsParsing.writeDebugString) {
                    long outputLineId = docProcessing ? i : lineId;
                    mainStr += "#lineId: " + outputLineId + "\n";
                    mainStr += "#articleId: " + articleId + "\n";
                    mainStr += "#date: " + date + "\n";
                }

                mainStrOnlyNEs = mainStr;

                mainStr += predArgStr[0];
                if (ConstantsParsing.writeUnaryRels && !predArgStr[4].equals("")) {
                    mainStr += "#unary rels:\n";
                    mainStr += predArgStr[4];
                }
                mainStrOnlyNEs += predArgStr[1];

                LinesHandler.mainStrs.add(mainStr);
                LinesHandler.mainStrsOnlyNEs.add(mainStrOnlyNEs);
            }

            // Write output of parser / coreNLP to file (use original JSON format, and add new keys)
            String s = String.join("\n", textList);
            String parsedStr = Util.getJsonStringParsedOutput(s, date, articleId, lineId, mergedEntitySentences, coreNLPOutput, ccgParserOutput);
            LinesHandler.parsedStrs.add(parsedStr);

        } catch (Exception e) {
            String articleIDvalue = ""; // "articleId"
            String lineIDvalue = ""; // "lineId"
            // Get articleID and lineID
            Pattern articleIDpattern = Pattern.compile("\"articleId\":\\s*[0-9]+");
            Pattern lineIDpattern = Pattern.compile("\"lineId\":\\s*-*[0-9]+");
            Matcher articleIDMatcher = articleIDpattern.matcher(line);
            if (articleIDMatcher.find()) {
                articleIDvalue = articleIDMatcher.group(0).split(":")[1];
            }
            Matcher lineIDMatcher = lineIDpattern.matcher(line);
            if (lineIDMatcher.find()) {
                lineIDvalue = lineIDMatcher.group(0).split(":")[1];
            }
            System.err.println("exception for article " + articleIDvalue + ", line " + lineIDvalue);
            e.printStackTrace();
        }
    }

    // Process texts with CoreNLP to get POS tags, NER tag, lemmas
    public CoreDocument processCoreNLP(List<String> sents) {
        String text = String.join("\n", sents);
        // Run CoreNLP
        CoreDocument doc = new CoreDocument(text);
        corenlppipeline.annotate(doc);
        return doc;
    }

    // Parse sentence
    public List<ParseResultForJava> runCCGParser(List<String> tokSents) {
        List<String> sents = cleanTokenisedSentences(tokSents);

        List<ParseResultForJava> results = ccgParser.parse(sents);
        return results;
    }

    // Get dependencies for a sentence that has already been parsed
    public List<ParseResultForJava> getCCGParserDeps(List<String> parseTrees) {
        List<ParseResultForJava> results = new ArrayList<ParseResultForJava>();
        if (true || parseTrees.contains("__no_parse__")) {
            // One or more sentences were not parsed (this can happen with EasyCCG)
            for (String parseTree : parseTrees) {
                if (parseTree.equals("__no_parse__")) {
                    results.add(null);
                } else {
                    try {
                        ParseResultForJava result = ccgParser.depsFromTreeStringSingle(parseTree);
                        results.add(result);
                    } catch (ArrayIndexOutOfBoundsException oobe) {
                        // No dependencies found
                    }
                }
            }
        } else {
            // All sentences have parse trees, but some may not have dependencies
            try {
                results = ccgParser.depsFromTreeStringMany(parseTrees);
            } catch (ArrayIndexOutOfBoundsException oobe) {
                // No dependencies found
            }
        }
        return results;
    }


    // Read in the combined lexicon file containing modals, attitude verbs, and conditionls
    public static void readLexiconFile(String filePath, String fileName) {
        List<HashMap<String, Pair<Integer, String>>> result = new ArrayList<>();

        String fp = String.join("/",filePath,fileName);
        try {
            BufferedReader br = new BufferedReader(new FileReader(fp));
            String line = "";
            Integer idx = 0;
            while (line != null) {
                line = br.readLine();
                if (line != null && !line.startsWith("#")) {
                    String[] elements = line.split("\\t");
                    String lemma = elements[0];
                    Integer strength = Integer.parseInt(elements[5]);
                    Integer mappedStrength = strengthMap.get(strength);
                    Pair<Integer, String> modalProps = new Pair(mappedStrength, elements[6]);
                    if (lemma.contains(" ")) {
                        // Multi-word modal / conditional found
                        // Add the list of strings (lexicalEntry) to the Trie structure
                        List<String> lexicalEntry = Arrays.asList(lemma.split(" "));
                        multiWordLexiconTrie.insert(lexicalEntry, idx);
                        // Add the properties and tag to the HashMap structure
                        String category = getCategoryFromTagList(elements);
                        Pair<String, String> lemmaAndCat = new Pair(lemma, category);
                        Pair<Pair<Integer, String>, Pair<String, String>> temp = new Pair(modalProps, lemmaAndCat);
                        multiWordLexiconProps.put(idx, temp);
                        idx ++;
                    }
                    else {
                        // Single-word modal / conditional / attitude verb found
                        // TODO - sort out strategy for precedence / merging
                        if (elements[1].equals("MOD")) {
                            modalLexicon.put(lemma, modalProps);
                        } else if (elements[2].equals("ATT_SAY")) {
                            attitudeVerbSayLexicon.put(lemma, modalProps);
                        } else if (elements[3].equals("ATT_THINK")) {
                            attitudeVerbThinkLexicon.put(lemma, modalProps);
                        } else if (elements[4].equals("COND")) {
                            conditionalsLexicon.put(lemma, modalProps);
                        }
                    }
                }
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        result.add(modalLexicon);
        result.add(attitudeVerbSayLexicon);
        result.add(attitudeVerbThinkLexicon);
        result.add(conditionalsLexicon);
    }

    // Read elements[1] to elements[4] and return the first non-empty string that this contains
    public static String getCategoryFromTagList(String[] elements) {
        for(int i=1; i<5; i++){
            if (!elements[i].equals("")) {
                return elements[i];
            }
        }
        return "";
    }


    // Given an index value, find the corresponding node in the graph
    public static LexicalGraphNode getNodeByIndex(Integer i, MutableValueGraph g) {
        Set<LexicalGraphNode> nodes = g.nodes();
        for (LexicalGraphNode n : nodes) {
            if (n.nodeIndex.equals(i)) {
                return n;
            }
        }
        return null;
    }

    // Get a list of nodes in the graph belonging to a specific SemanticCategoryType
    public static List<LexicalGraphNode> getSemCatTypeNodes(MutableValueGraph g, SemanticCategoryType semCatType) {
        List<LexicalGraphNode> eventNodes = new ArrayList<>();
        Set<LexicalGraphNode> nodes = g.nodes();
        for (LexicalGraphNode n : nodes) {
            if (n.lexItem.getSemCatType().equals(semCatType)) {
                eventNodes.add(n);
            }
        }
        eventNodes.sort(Comparator.comparing(LexicalGraphNode::nodeIndex));
        return eventNodes;
    }

    // Get a list of nodes in the graph marked as a modal
    public static HashMap<LexicalGraphNode, Boolean> getModalNodes(MutableValueGraph g, List<LexicalGraphNode> negNodes) {
        HashMap<LexicalGraphNode, Boolean> modalNodes = new HashMap<>();
        Set<LexicalGraphNode> nodes = g.nodes();
        for (LexicalGraphNode n : nodes) {
            if (n.lexItem.getModalPOS() != null) {
                Boolean negated = checkIfNegated(g, n, negNodes);
                modalNodes.put(n, negated);
            }
        }
        return modalNodes;
    }

    // Get a list of nodes in the graph marked as attitude verbs
    public static List<LexicalGraphNode> getAttitudeNodes(MutableValueGraph g) {
        List<LexicalGraphNode> attitudeNodes = new ArrayList<>();
        Set<LexicalGraphNode> nodes = g.nodes();
        for (LexicalGraphNode n : nodes) {
            if (n.lexItem.getAttitudeType() != null) {
                attitudeNodes.add(n);
            }
        }
        attitudeNodes.sort(Comparator.comparing(LexicalGraphNode::nodeIndex));
        return attitudeNodes;
    }

    // Get a list of nodes in the graph marked as a conditonals
    public static List<LexicalGraphNode> getConditionalNodes(MutableValueGraph g) {
        List<LexicalGraphNode> conditionalNodes = new ArrayList<>();
        Set<LexicalGraphNode> nodes = g.nodes();
        for (LexicalGraphNode n : nodes) {
            // If the node is conditional, AND it has not already been marked as a counterfactual (i.e. "if" linking to "had")
            if (n.lexItem.getConditional()) {
                conditionalNodes.add(n);
            }
        }
        conditionalNodes.sort(Comparator.comparing(LexicalGraphNode::nodeIndex));
        return conditionalNodes;
    }

    // Get a list of nodes in the graph marked as time expressions
    public static List<LexicalGraphNode> getTimeNodes(MutableValueGraph g) {
        List<LexicalGraphNode> timeNodes = new ArrayList<>();
        Set<LexicalGraphNode> nodes = g.nodes();
        for (LexicalGraphNode n : nodes) {
            // If the node is a time expressions
            if (n.lexItem.getTimeExpression() != null) {
                timeNodes.add(n);
            }
        }
        return timeNodes;
    }

    // Get a list of nodes in the graph that are counterfactual
    public static List<LexicalGraphNode> getCounterfactualNodes(MutableValueGraph g) {
        List<LexicalGraphNode> counterfactualNodes = new ArrayList<>();
        Set<LexicalGraphNode> nodes = g.nodes();
        for (LexicalGraphNode n : nodes) {
            if (n.lexItem.getCounterfactual()) {
                counterfactualNodes.add(n);
            }
        }
        counterfactualNodes.sort(Comparator.comparing(LexicalGraphNode::nodeIndex));
        return counterfactualNodes;
    }

    // Get a list of nodes in the graph with lemma "be" (copulas)
    public static List<LexicalGraphNode> getCopulaNodes(MutableValueGraph g) {
        List<LexicalGraphNode> copulaNodes = new ArrayList<>();
        Set<LexicalGraphNode> nodes = g.nodes();
        for (LexicalGraphNode n : nodes) {
            if (n.lexItem.getLemma().equals("be")) {
                copulaNodes.add(n);
            }
        }
        copulaNodes.sort(Comparator.comparing(LexicalGraphNode::nodeIndex));
        return copulaNodes;
    }

    // Copy the time expression from a time node to a verb node if a path exists between the two nodes (in either direction)
    public static MutableValueGraph copyTimeToVerbs(MutableValueGraph g, List<LexicalGraphNode> timeNodes, List<LexicalGraphNode>verbNodes) {
        // Check if a time expression links to a verbNode, which at this point could be a true verb or a preposition
        for (LexicalGraphNode timeNode : timeNodes) {
            for (LexicalGraphNode verbNode : verbNodes) {
                if (timeNode.nodeIndex != verbNode.nodeIndex) {
                    Boolean pathExists = checkIfPathBetweenNodes(g, verbNode, timeNode, Boolean.TRUE);
                    if (!pathExists) {
                        pathExists = checkIfPathBetweenNodes(g, timeNode, verbNode, Boolean.TRUE);
                    }
                    if (pathExists) {
                        verbNode.lexItem.setTimeExpression(timeNode.lexItem.getTimeExpression());
                    }
                }
            }
        }
        // Get list of prepositions and true verbs
        List<LexicalGraphNode> prepositionNodes = new ArrayList<>();
        List<LexicalGraphNode> trueVerbNodes = new ArrayList<>();
        for (LexicalGraphNode vNode : verbNodes) {
            if (vNode.lexItem.getPreposition()) {
                prepositionNodes.add(vNode);
            }
            else {
                trueVerbNodes.add(vNode);
            }
        }
        // Check if link between preposition node and verb node, and copy time
        for (LexicalGraphNode pNode : prepositionNodes) {
            for (LexicalGraphNode tvNode : trueVerbNodes) {
                Boolean pathExists = checkIfPathBetweenNodes(g, pNode, tvNode, Boolean.FALSE);
                if (pathExists && tvNode.lexItem.getTimeExpression() == null && pNode.lexItem.getTimeExpression() != null) {
                    tvNode.lexItem.setTimeExpression(pNode.lexItem.getTimeExpression());
                }
            }
        }
        return g;
    }

    // Identify leaves in CCG parse string, and extract the CCG tag
    public static List<String> getCcgTagsFromTree(String tree) {
        List<String> ccgTags = new ArrayList<>();
        Pattern leafPattern = Pattern.compile("\\(<L\\s[\\S]+\\sX\\sX\\s[\\S]+\\sX>\\)");
        Matcher leafMatcher = leafPattern.matcher(tree);
        while (leafMatcher.find()) {
            String leaf = tree.substring(leafMatcher.start(), leafMatcher.end());
            String ccgTag = leaf.split("\\s+")[1];
            ccgTags.add(ccgTag);
        }
        return ccgTags;
    }

    // Build a lexical graph from the parse and CoreNLP annotations
    public static MutableValueGraph buildLexicalGraph(List<LexicalItem> lexicalItems, List<DepLink> depLinks) {
        MutableValueGraph lexGraph = ValueGraphBuilder.directed().build();

        // Add nodes (annotated with information from CoreNLP)
        for (LexicalItem l : lexicalItems) {
            Integer nodeIndex = l.getIndex();
            LexicalGraphNode lexNode = new LexicalGraphNode(nodeIndex, l);
            lexGraph.addNode(lexNode);
        }

        // Add dependency links (from the Parser)
        for (DepLink d : depLinks) {
            LexicalGraphNode n1 = getNodeByIndex(d.headPos(), lexGraph);
            LexicalGraphNode n2 = getNodeByIndex(d.depPos(), lexGraph);
            String depSlot;

            // For instances of conditional "if" that link to a "had", set both nodes to be counterfactual, and set "if" to be not conditional
            if (n1.lexItem.getLemma().equals("if") && n2.lexItem.getToken().equals("had") && n1.lexItem.getConditional()) {
                n1.lexItem.setCounterfactual(Boolean.TRUE);
                n2.lexItem.setCounterfactual(Boolean.TRUE);
                n1.lexItem.setConditional(Boolean.FALSE);
            }

            // If the POS tag of the head is IN / TO and the dependent is an "event", and n2's index is before n1's, flip the direction
            if (((n1.lexItem.getPosTag().equals("IN") && !n1.lexItem.getLemma().equals("if")) || n1.lexItem.getPosTag().equals("TO")) && n2.lexItem.getSemCatType().equals(SemanticCategoryType.EVENT) && n2.nodeIndex < n1.nodeIndex) {
                depSlot = d.depSlot() + "a";
                lexGraph.putEdgeValue(n2, n1, depSlot);
            }
            // If the POS tag of the head is IN and the dependent is an adjective with tag JJR (comparative)
            // or JJS (superlative), or a regular adjective ("JJ"), then flip the directions
            else if (n1.lexItem.getPosTag().equals("IN") && (n2.lexItem.getPosTag().equals("JJR") || n2.lexItem.getPosTag().equals("JJS") || n2.lexItem.getSemCatType().equals(SemanticCategoryType.TYPEMOD) || (n2.lexItem.getPosTag().equals("JJ") && !n2.lexItem.getSemCatType().equals(SemanticCategoryType.TYPE))) && n2.nodeIndex < n1.nodeIndex) {
                depSlot = d.depSlot() + "a";
                lexGraph.putEdgeValue(n2, n1, depSlot);
            }
            else {
                lexGraph.putEdgeValue(n1, n2, Integer.toString(d.depSlot()));
            }
        }

        return lexGraph;
    }

    // Get a mapping of head nodes to a list of their dependent nodes
    public static HashMap<Integer, List<Integer>> getDepLinkMap(List<DepLink> depLinks) {
        HashMap<Integer, List<Integer>> depLinkMap = new HashMap<Integer, List<Integer>>();
        for (DepLink d : depLinks) {
            if (depLinkMap.containsKey(d.headPos())) {
                List<Integer> listdep = depLinkMap.get(d.headPos());
                listdep.add(d.depPos());
                depLinkMap.put(d.headPos(), listdep);
            } else {
                List<Integer> listdep = new ArrayList<Integer>(Arrays.asList(d.depPos()));
                depLinkMap.put(d.headPos(), listdep);
            }
        }
        return depLinkMap;
    }

    // Construct lexical items and return a lexical graph
    public static MutableValueGraph getLexicalGraph(ParseResultForJava parsedSent, CorenlpSentence annotatedSent) {
        List<LexicalItem> lexicalItems = new ArrayList<>();

        // Get list of ccgTags, one for each token in the sentence
        List<String> ccgTags = getCcgTagsFromTree(parsedSent.tree());

        // Create hashmap of deplinks for identifying an auxiliary verb
        HashMap<Integer, List<Integer>> depLinkMap = getDepLinkMap(parsedSent.deps());

        // Loop through the tokens in the parsed sentence
        List<SemanticCategoryType> semCatTypes = new ArrayList<>();

        Integer modalStrength = null;
        String modalPOS = null;
        Boolean conditional = Boolean.FALSE;
        Boolean counterfactual = Boolean.FALSE;
        String modalString = null;
        String attVerbString = null;
        String conditionalString = null;
        Integer tagRestAsMultiWord = 0;

        for (int i = 0; i < annotatedSent.tokens().size(); i++) {
            // Add lexical item for each token
            String token = annotatedSent.tokens().get(i).toLowerCase();
            String lemma = annotatedSent.lemmas().get(i).toLowerCase();
            String ccgTag = ccgTags.get(i);
            String posTag = annotatedSent.posTags().get(i);
            String neTag = annotatedSent.nerTags().get(i);
            String antecedent = annotatedSent.antecedents().get(i);
            SemanticCategoryType semCatType = SemanticCategory.getSemanticCategoryType(lemma, posTag, ccgTag);
            String tense = VerbTense.getVerbTense(posTag, ccgTag);
            semCatTypes.add(semCatType);
            String auxVerb = null;
            if (tagRestAsMultiWord == 0) {
                modalStrength = null;
                modalPOS = null;
                conditional = Boolean.FALSE;
                counterfactual = Boolean.FALSE;
                modalString = null;
                attVerbString = null;
                conditionalString = null;
            }
            String attitudeType = null;
            Boolean tagged = Boolean.FALSE;
            // Single-word modal / conditional / attitude verb
            if (tagRestAsMultiWord == 0) {
                if (ConstantsParsing.tagModals) {
                    Pair<Integer, String> modalProps = null;
                    if (modalLexicon.containsKey(lemma)) {
                        modalProps = modalLexicon.get(lemma);
                        modalString = lemma;
                    } else if (modalLexicon.containsKey(token.toLowerCase())) {
                        modalProps = modalLexicon.get(token.toLowerCase());
                        modalString = token.toLowerCase();
                    }
                    if (modalProps != null) {
                        tagged = Boolean.TRUE;
                        modalStrength = modalProps.first;
                        modalPOS = modalProps.second;
                    }
                }
                if (ConstantsParsing.tagAttitudeVerbs) {
                    if (attitudeVerbSayLexicon.containsKey(lemma) || attitudeVerbSayLexicon.containsKey(token.toLowerCase())) {
                        attitudeType = "say";
                        tagged = Boolean.TRUE;
                        attVerbString = (attitudeVerbSayLexicon.containsKey(lemma)) ? lemma : token.toLowerCase();
                    } else if (attitudeVerbThinkLexicon.containsKey(lemma) || attitudeVerbThinkLexicon.containsKey(token.toLowerCase())) {
                        attitudeType = "think";
                        tagged = Boolean.TRUE;
                        attVerbString = (attitudeVerbThinkLexicon.containsKey(lemma)) ? lemma : token.toLowerCase();
                    }
                }
                if (ConstantsParsing.tagConditionals && conditionalsLexicon.containsKey(lemma)) {
                    conditional = Boolean.TRUE;
                    tagged = Boolean.TRUE;
                    conditionalString = lemma;
                }
                // Tagging counterfactuals that are not linked to by
                if (ConstantsParsing.tagCounterfactuals && token.equals("had") && (ccgTag.equals("((S/S)/(S[pt]\\NP))/NP") || ccgTag.equals("(((S\\NP)\\(S\\NP))/(S[pt]\\NP))/NP"))) {
                    counterfactual = Boolean.TRUE;
                }

                // Look for instances of "going to" - tag these as modal
                if (!tagged && ConstantsParsing.tagModals && token.equals("going")) {
                    if (annotatedSent.tokens().size() >= i+2 && annotatedSent.tokens().get(i+1).equals("to")) {
                        tagged = Boolean.TRUE;
                        modalStrength = strengthMap.get(3);
                        modalPOS = "VB";
                        modalString = "going to";
                    }
                }


                // If not already tagged, check if there is a multi-word modal / conditional
                if (!tagged && (ConstantsParsing.tagModals || ConstantsParsing.tagConditionals)) {
                    List<String> threeTokens = Arrays.asList("", "", "");
                    for (int j = 0; j < 3; j++) {
                        if (i + j < annotatedSent.lemmas().size()) {
                            threeTokens.set(j, annotatedSent.lemmas().get(i + j).toLowerCase());
                        }
                    }
                    Pair<Integer, Integer> tagMultiWord = null;
                    if (!threeTokens.get(1).equals("")) {
                        tagMultiWord = checkIfInMultiWordTrieLexicon(threeTokens);
                    }
                    if (tagMultiWord != null) {
                        Pair<Pair<Integer, String>, Pair<String, String>> multiWordProps = multiWordLexiconProps.get(tagMultiWord.second);
                        if (multiWordProps.second.second.equals("MOD")) {
                            modalStrength = multiWordProps.first.first;
                            modalPOS = multiWordProps.first.second;
                            modalString = multiWordProps.second.first;
                        } else if (multiWordProps.second.second.equals("COND")) {
                            conditional = Boolean.TRUE;
                            conditionalString = multiWordProps.second.first;
                        }
                        tagRestAsMultiWord = tagMultiWord.first;
                    }
                }
            }
            Boolean verb = (posTag.length() > 1 && posTag.substring(0, 2).equals("VB")) ? Boolean.TRUE : Boolean.FALSE;
            // Determine if the token is an auxiliary verb
            if (semCatType.equals(SemanticCategoryType.EVENT) && depLinkMap.containsKey(i)) {
                // Get the child nodes of the current node (from the deplinks)
                List<Integer> childIndices = depLinkMap.get(i);
                Boolean verbFound = Boolean.FALSE;
                for (Integer ci : childIndices) {
                    String ciLemma = annotatedSent.lemmas().get(ci).toLowerCase();
                    String ciPosTag = annotatedSent.posTags().get(ci);
                    String ciCcgTag = ccgTags.get(ci);
                    SemanticCategoryType ciSemCatType = SemanticCategory.getSemanticCategoryType(ciLemma, ciPosTag, ciCcgTag);
                    if (ciSemCatType.equals(SemanticCategoryType.EVENT)
                            && ciPosTag != "" && (ciPosTag.substring(0, 1).equals("V") || ciPosTag.substring(0, 1).equals("M"))) {
                        verbFound = Boolean.TRUE;
                    }
                }
                if (verbFound) {
                    auxVerb = AuxiliaryVerb.getAuxVerb(lemma, semCatType);
                }
            }
            Boolean preposition = Boolean.FALSE;
            if (posTag.equals("IN") || posTag.equals("TO")) {
                preposition = Boolean.TRUE;
            }
            Pair<Calendar,Calendar> timeExpression = annotatedSent.timeExpressions().get(i);
            Integer index = i;
            LexicalItem lexItem = new LexicalItem(token, lemma, posTag, tense, neTag, antecedent, semCatType, verb,
                    auxVerb, modalStrength, modalPOS, attitudeType, conditional, counterfactual, modalString, attVerbString,
                    conditionalString, preposition, ccgTag, timeExpression, index);
            lexicalItems.add(lexItem);

            if (tagRestAsMultiWord > 0) {
                tagRestAsMultiWord--;
            }
        }

        // Build the graph
        MutableValueGraph lg = buildLexicalGraph(lexicalItems, parsedSent.deps());

        return lg;
    }

    // Check if the three tokens, or the first two in the list match an entry in the LexiconTrie
    public static Pair<Integer, Integer> checkIfInMultiWordTrieLexicon(List<String> threeTokens) {
        Integer idx = multiWordLexiconTrie.search(threeTokens);
        if (!idx.equals(-1)) {
            Pair<Integer, Integer> result = new Pair(3, idx);
            return result;
        }
        else {
            // Try just the first two tokens
            List<String> twoTokens = threeTokens.subList(0,2);
            Integer idx2 = multiWordLexiconTrie.search(twoTokens);
            if (!idx2.equals(-1)) {
                Pair<Integer, Integer> result = new Pair(2, idx2);
                return result;
            }
        }
        return null;
    }

    // Return a list of multi-word named entities
    public static List<CoreEntityMention> getMultiWordNEs(CoreSentence s) {
        List<CoreEntityMention> coreEMs = s.entityMentions();
        List<CoreEntityMention> multiCoreEMs = new ArrayList<>();
        for (CoreEntityMention cem : coreEMs) {
            Integer firstTok = cem.tokens().get(0).index();
            Integer lastTok = cem.tokens().get(cem.tokens().size() - 1).index();
            if (!firstTok.equals(lastTok)) {
                multiCoreEMs.add(cem);
            }
        }
        return multiCoreEMs;
    }

    // Get the argument string of a node
    public static String getArgument(LexicalGraphNode n) {
        // Event node. Return "e"
        if (n.lexItem.getSemCatType().equals(SemanticCategoryType.EVENT)) {
            return "e";
        } // Pronoun node. Return antecedent string
        else if (n.lexItem.getPosTag().equals("PRP") && n.lexItem.getAntecedent() != null) {
            return n.lexItem.getAntecedent();
        }
        // Everything else
        else {
            return n.lexItem.getToken();
        }
    }

    // Get the tense of the predicate
    public static String getPredicateTense(List<LexicalGraphNode> nodeChain, List<LexicalGraphNode> auxNodes) {
        // Check if the headNode is a future tense marker
        LexicalGraphNode headNode = nodeChain.get(0);
        if (AuxiliaryVerb.futureTenseAdverbials.contains(headNode.lexItem.getToken())) {
            return "future";
        }
        String tense = null;
        if (auxNodes.size() > 0) {
            // If there is an auxiliary verb (that is not a future tense adverbial), get the tense from the first auxiliary
            LexicalGraphNode headAuxNode = auxNodes.get(0);
            tense = headAuxNode.lexItem.getTense();
        }
        else {
            // If there are no auxiliaries, get the tense from the last verb in the chain
            for (LexicalGraphNode node : nodeChain) {
                if (node.lexItem.getVerb()) {
                    tense = node.lexItem.getTense();
                }
            }
        }
        return tense;
    }

    // Get the grammatical aspect of the predicate
    public static String getPredicateAspect(String auxLemma, String aux, String ccgTagFeature) {
        String aspect = null;
        // If the ccgTagFeature is null (e.g. in the case of a proposition), we can't determine aspect
        if (ccgTagFeature == null) {
            return aspect;
        }
        if (ccgTagFeature.equals("ng") && auxLemma.contains("be")) { // progressive
            if (auxLemma.contains("have") && aux.contains("been")) {
                aspect = "perfect-progressive";
            }
            else {
                aspect = "progressive";
            }
        }
        else if (ccgTagFeature.equals("pt") && auxLemma.contains("have")) { // perfect?
            aspect = "perfect";
        }
        else {
            aspect = "simple";
        }
        return aspect;
    }

    // Get the feature on the CCG tag, if one is available
    // This will be used with verbs only
    public static String getCCGTagFeature(String ccgTag) {
        // Get the CCG tag feature, e.g. what is contained in the [] tags (sentential or verb phrase)
        String ccgTagFeature = null;
        Matcher matcher = ccgTagFeaturePattern.matcher(ccgTag);
        // If there is a sentential / verb phrase feature
        if (matcher.find()) {
            ccgTagFeature = matcher.group(0).split("\\[")[1].split("]")[0];
        }
        return ccgTagFeature;
    }

    // Build a relation object uisng information from the head node and node
    public static Relation getRelation(List<LexicalGraphNode> nodeChain, Optional<String> predSlot, Boolean negation,
                                       Boolean hasModal, HashMap<LexicalGraphNode, Boolean> connectedModals, Boolean isAttitudeSay,
                                       Boolean isAttitudeThink, Boolean isConditional, Set<String> modalStrings,
                                       Set<String> conditionalStrings, Set<String> attVerbStrings, Boolean isCounterfactual,
                                       Set<String> counterfactualStrings) {
        LexicalGraphNode headNode = nodeChain.get(0);
        LexicalGraphNode node = nodeChain.get(nodeChain.size() - 1);
        List<LexicalGraphNode> nodeChainInner = nodeChain.subList(0, nodeChain.size() - 1);

        String argument = getArgument(node);

        // Later we'll filter out relations where the arguments are not entity or type,
        // so this simple check for the argumentType is enough for now
        String argumentType = (node.lexItem.getNeTag().equals("O") || node.lexItem.getPosTag().equals("CD")) ? "G" : "E";
        String argumentPOSTag = node.lexItem.getPosTag();
        String argumentCCGTag = node.lexItem.getCcgTag();

        // The PoS tag may suggest a proper name, but the NER system does not detect a named entity
        // In this case, set the argumentType to be "E" (entity) so that any relations containing the argument are
        // correctly handled by the "ConstantsParsing.acceptGGBinary" filter later on
        if (argumentPOSTag.startsWith("NNP")) {
            argumentType = "E";
        }

        // Get the predicate and auxiliary strings
        Integer predIndex = null;
        Integer copularBeIndex = null;
        List<String> predicateList = new ArrayList<String>();
        List<String> predicateLemmatisedList = new ArrayList<String>();
        List<String> auxiliaryList = new ArrayList<String>();
        List<String> auxiliaryLemmatisedList = new ArrayList<String>();

        Pair<Calendar,Calendar> date = null;

        Boolean foundFirstAuxNode = Boolean.FALSE;
        LexicalGraphNode firstNonAuxNode = headNode;

        List<LexicalGraphNode> auxNodes = new ArrayList<>();

        Boolean predContainsAdjective = Boolean.FALSE;

        for (LexicalGraphNode n : nodeChainInner) {
            if (n.lexItem.getAuxVerb() == null) {
                predicateList.add(n.lexItem.getToken());
                predicateLemmatisedList.add(n.lexItem.getLemma());
                if (predIndex == null) {
                    predIndex = n.lexItem.getIndex();
                }
                if (!foundFirstAuxNode) {
                    foundFirstAuxNode = Boolean.TRUE;
                    firstNonAuxNode = n;
                }
                if (n.lexItem.getPosTag().contains("JJ") || n.lexItem.getSemCatType().equals(SemanticCategoryType.TYPEMOD)) { // && !n.lexItem.getSemCatType().equals(SemanticCategoryType.TYPE)) {
                    predContainsAdjective = Boolean.TRUE;
                }
                if (n.lexItem.getLemma().equals("be")) {
                    copularBeIndex = n.nodeIndex;
                }
            } else {
                auxiliaryList.add(n.lexItem.getToken());
                auxiliaryLemmatisedList.add(n.lexItem.getLemma());
                auxNodes.add(n);
            }
            if (n.lexItem.getTimeExpression() != null) {
                date = n.lexItem.getTimeExpression();
            }
        }

        Boolean passive = (firstNonAuxNode.lexItem.getCcgTag().contains("[pss]")) ? Boolean.TRUE : Boolean.FALSE;
        String predCCGFeature = getCCGTagFeature(firstNonAuxNode.lexItem.getCcgTag());
        Boolean predIsCopular = (firstNonAuxNode.lexItem.getLemma().equals("be")) ? Boolean.TRUE : Boolean.FALSE;
        Boolean predIsDoubleHopRelationOnEntityOrType = Boolean.FALSE;
        Boolean predIsVerb = firstNonAuxNode.lexItem.getVerb();
        Boolean predIsPreposition = firstNonAuxNode.lexItem.getPreposition();

        String argumentLemmatised = node.lexItem.getLemma();

        String predicate = String.join(".", predicateList);
        String predicateLemmatised = String.join(".", predicateLemmatisedList);
        String auxiliary = String.join(".", auxiliaryList);
        String auxiliaryLemmatised = String.join(".", auxiliaryLemmatisedList);
        String predTense = getPredicateTense(nodeChain, auxNodes);
        String predAspect = getPredicateAspect(auxiliaryLemmatised, auxiliary, predCCGFeature);
        Integer argIndex = node.nodeIndex;
        String argSlot = predSlot.orElse(null);
        SemanticCategoryType argSemCatType = node.lexItem.getSemCatType();
        Boolean resolvedCoref = (node.lexItem.getAntecedent() == null) ? Boolean.FALSE : Boolean.TRUE;
        HashMap<Integer, Pair<String, Integer>> connectedModalsIdxs = constructConnectedModalsIdxs(connectedModals);

        // Remove modals from modalStrings if they form part of the predicate string
        modalStrings = removeModalsIfContainedInPredicate(modalStrings, predicateLemmatised, predicate);

        Relation relation = new Relation(negation, passive, predCCGFeature, predicate, predicateLemmatised, auxiliary,
                auxiliaryLemmatised, hasModal, connectedModalsIdxs, isAttitudeSay, isAttitudeThink, isConditional,
                modalStrings, attVerbStrings, conditionalStrings, isCounterfactual, counterfactualStrings,
                predIsCopular, predIsDoubleHopRelationOnEntityOrType, predIsVerb, predIsPreposition,
                predContainsAdjective, predTense, predAspect, predIndex, copularBeIndex, argument, argumentLemmatised, argumentType,
                argumentPOSTag, argumentCCGTag, argIndex, argSlot, argSemCatType, date, resolvedCoref);
        return relation;
    }

    // Remove modals from the set if they form part of the predicate string
    public static Set<String> removeModalsIfContainedInPredicate(Set<String> modalStrings, String predicateLemmatised, String predicate) {
        Set<String> result = new HashSet<>();
        for (String modString : modalStrings) {
            String modStringWithDots = modString.replace(" ", ".");
            if (!(predicate.contains(modStringWithDots) || predicateLemmatised.contains(modStringWithDots))) {
                result.add(modString);
            }
        }
        return result;
    }

    // Adjust the modal strength if the modal is negated, i.e. connected to a "not" or "never"
    public static Integer getNegatedModalStrength(Integer originalStrength, Boolean negated) {
        if (!negated) {
            return originalStrength;
        }
        else {
            return 1;
        }
    }

    // From the set of connected modals, construct a hashmpa that maps from the node ID to a pair containing the
    // lemma and its strength
    public static HashMap<Integer, Pair<String, Integer>> constructConnectedModalsIdxs(HashMap<LexicalGraphNode, Boolean> connectedModals) {
        HashMap<Integer, Pair<String, Integer>> connectedModalsIdxs = new HashMap<>();
        for (LexicalGraphNode n : connectedModals.keySet()) {
            Integer originalStrength = n.lexItem.getModalStrength();
            Integer negatedStrength = getNegatedModalStrength(originalStrength, connectedModals.get(n));
            Pair<String, Integer> lemmaStrength = new Pair(n.lexItem.getLemma(), negatedStrength);
            connectedModalsIdxs.put(n.nodeIndex, lemmaStrength);
        }
        return connectedModalsIdxs;
    }

    // Determine if the headNode has a negation attached
    public static Boolean checkIfNegated(MutableValueGraph g, LexicalGraphNode headNode, List<LexicalGraphNode> negNodes) {
        for (LexicalGraphNode n : negNodes) {
            Boolean pathExists = checkIfPathBetweenNodes(g, n, headNode, Boolean.FALSE);
            if (pathExists) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    // Determine if the eventNode has a conditional attached
    public static Set<String> getConnectedConditionals(MutableValueGraph g, LexicalGraphNode eventNode,
                                                       List<LexicalGraphNode> conditionalNodes) {
        Set<String> conditionals = new HashSet<>();

        for (LexicalGraphNode conditionalNode : conditionalNodes) {
            Boolean isPath = checkIfPathBetweenNodes(g, conditionalNode, eventNode, Boolean.FALSE);
            if (isPath) {
                conditionals.add(conditionalNode.lexItem.getConditionalString());
            }
        }
        return conditionals;
    }

    // Determine if the eventNode has a counterfactual attached
    public static Set<String> getConnectedCounterfactuals(MutableValueGraph g, LexicalGraphNode eventNode,
                                                       List<LexicalGraphNode> counterfactualNodes) {
        Set<String> counterfactuals = new HashSet<>();

        for (LexicalGraphNode counterfactualNode : counterfactualNodes) {
            Boolean isPath = checkIfPathBetweenNodes(g, counterfactualNode, eventNode, Boolean.FALSE);
            if (isPath) {
                counterfactuals.add("had");
            }
        }
        return counterfactuals;
    }

    // Determine if the eventNode has a attitude verb attached
    public static Pair<Set<String>, Set<String>> getConnectedAttitudeVerbs(MutableValueGraph g, LexicalGraphNode eventNode,
                                                                           List<LexicalGraphNode> attitudeNodes) {

        Set<String> attVerbsSay = new HashSet<>();
        Set<String> attVerbsThink = new HashSet<>();

        for (LexicalGraphNode attitudeNode : attitudeNodes) {
            Boolean isPath = checkIfPathBetweenNodes(g, attitudeNode, eventNode, Boolean.FALSE);
            if (isPath) {
                if (attitudeNode.lexItem.getAttitudeType().equals("say")) {
                    attVerbsSay.add(attitudeNode.lexItem.getAttVerbString());
                } else if (attitudeNode.lexItem.getAttitudeType().equals("think")) {
                    attVerbsThink.add(attitudeNode.lexItem.getAttVerbString());
                } else {
                    continue;
                }
            }
        }

        Pair<Set<String>, Set<String>> result = new Pair(attVerbsSay, attVerbsThink);
        return result;
    }

    // Determine if the eventNode has a modal attached
    public static Pair<HashMap<LexicalGraphNode, Boolean>, Set<String>> getConnectedModals(MutableValueGraph g, LexicalGraphNode eventNode,
                                                           HashMap<LexicalGraphNode, Boolean> modalNodes, List<LexicalGraphNode> copulaNodes) {
        HashMap<LexicalGraphNode, Boolean> connectedModalNodes = new HashMap<>();
        Set<String> modalStrings = new HashSet<>();

        for (LexicalGraphNode modalNode : modalNodes.keySet()) {
            if (modalNode.nodeIndex != eventNode.nodeIndex) { // LKG Dec 2020 (avoid connecting win as a modal to win as an event)
                // Recurse and see if there is a direct path
                Boolean hasModal = checkIfPathBetweenNodes(g, modalNode, eventNode, Boolean.FALSE);
                if (hasModal) {
                    connectedModalNodes.put(modalNode, modalNodes.get(modalNode));
                    modalStrings.add(modalNode.lexItem.getModalString());
                }
                // If there is no path, check if there is a path via a copula
                else {
                    for (LexicalGraphNode copulaNode : copulaNodes) {
                        // Check for path between copula and modal
                        Boolean isCopulaToModalLink = checkIfPathBetweenNodes(g, copulaNode, modalNode, Boolean.FALSE);
                        Boolean isCopulaToEventLink = checkIfPathBetweenNodes(g, copulaNode, eventNode, Boolean.FALSE);
                        if (isCopulaToModalLink && isCopulaToEventLink) {
                            connectedModalNodes.put(modalNode, modalNodes.get(modalNode));
                            modalStrings.add(modalNode.lexItem.getModalString());
                        }
                    }
                }
            }
        }
        Pair<HashMap<LexicalGraphNode, Boolean>, Set<String>> result = new Pair(connectedModalNodes, modalStrings);
        return result;
    }

    // Determine whether there is a path from node n1 to node n2
    public static Boolean checkIfPathBetweenNodes(MutableValueGraph g, LexicalGraphNode n1, LexicalGraphNode n2, Boolean forTimeLinking) {

        // Don't look for paths from the node to itself
        if (n1.nodeIndex == n2.nodeIndex) {
            return Boolean.FALSE;
        }

        int N = g.nodes().size();

        // Store whether node in graph is discovered or not
        boolean[] discovered = new boolean[N];

        // Create a queue used to do BFS
        Queue<Integer> q = new ArrayDeque<>();

        // Do BFS traversal from node n1
        // Mark node n1 as discovered
        discovered[n1.nodeIndex] = true;

        // Push node n1 into the queue
        q.add(n1.nodeIndex);

        // start BFS traversal from vertex i
        recursiveBFS(g, q, discovered, forTimeLinking);

        if (discovered[n2.nodeIndex]) {
            return Boolean.TRUE;
        }

        return Boolean.FALSE;
    }

    // Perform BFS recursively on graph
    public static void recursiveBFS(MutableValueGraph g, Queue<Integer> q, boolean[] discovered, Boolean forTimeLinking) {
        if (q.isEmpty())
            return;

        // Pop front node from queue
        Integer v = q.poll();

        // Do for every edge (v -> u)
        LexicalGraphNode vNode = getNodeByIndex(v, g);
        Set<LexicalGraphNode> children = g.successors(vNode);
        for (LexicalGraphNode u : children) {
            // Don't allow traversal if the slot value between node n1 and n2 is a "2a" (indicating a flipped direction)
            Optional<String> edgeBetweenNodes = g.edgeValue(vNode,u);
            if (!forTimeLinking && !edgeBetweenNodes.isEmpty() && edgeBetweenNodes.equals(Optional.of("2a"))) {
                continue;
            }
            else if (!discovered[u.nodeIndex])
            {
                // mark it discovered and push it into queue
                if (discovered[u.nodeIndex]) {
                    break;
                }
                discovered[u.nodeIndex] = true;
                q.add(u.nodeIndex);
            }
        }

        // Recurse
        recursiveBFS(g, q, discovered, forTimeLinking);
    }


    // Recursively find a child node that is not a preposition or auxiliary
    public static List<List<LexicalGraphNode>> getNodeChain(MutableValueGraph g, LexicalGraphNode node, Boolean getChildren,
                                                            Boolean getNegationChain, List<LexicalGraphNode> visitedNodes, Boolean headNodeIsCopula) {
        List<List<LexicalGraphNode>> retChains = new ArrayList<>();

        // Check for cycles
        if (visitedNodes.contains(node)) {
            // Cycle found. Break out of the recursive loop
            return retChains;
        } else {
            visitedNodes.add(node);
        }

        if ((node.lexItem.getPosTag().contains("JJ") && !node.lexItem.getSemCatType().equals(SemanticCategoryType.TYPE))
                || node.lexItem.getPosTag().equals("JJS") || node.lexItem.getPosTag().equals("JJR")
                || node.lexItem.getSemCatType().equals(SemanticCategoryType.TYPEMOD)
                || node.lexItem.getPreposition() || node.lexItem.getAuxVerb() != null ||
                (getNegationChain && node.lexItem.getSemCatType().equals(SemanticCategoryType.EVENT))) {
            // Need to traverse the graph as the current node is a preposition or auxiliary verb
            // Remove from the set anything that is not a preposition, auxiliary, or a verb
            List<LexicalGraphNode> subNodes = new ArrayList<LexicalGraphNode>();
            if (getChildren) {
                subNodes.addAll(g.successors(node));
            } else {
                subNodes.addAll(g.predecessors(node));
            }
            if (node.lexItem.getAuxVerb() != null) {
                List<LexicalGraphNode> removals = new ArrayList<>();
                // Remove non-verb nodes
                for (LexicalGraphNode sc : subNodes) {
                    if (!sc.lexItem.getSemCatType().equals(SemanticCategoryType.EVENT)) {
                        removals.add(sc);
                    }
                }
                for (LexicalGraphNode r : removals) {
                    subNodes.remove(r);
                }
            }

            for (LexicalGraphNode subNode : subNodes) {
                if (subNode.lexItem.getSemCatType().equals(SemanticCategoryType.EVENT) &&
                        !subNode.lexItem.getPreposition() && subNode.lexItem.getAuxVerb() == null
                        && (ConstantsParsing.chainPredicatesOnCopularBe && !subNode.lexItem.getLemma().equals("be"))) {
                    // Node is the main verb
                    List<LexicalGraphNode> leafList = new LinkedList<>();
                    if (getChildren) {
                        leafList.add(0, subNode);
                        leafList.add(0, node);
                    } else {
                        leafList.add(0, node);
                        leafList.add(0, subNode);
                    }
                    retChains.add(leafList);
                } else {
                    // Node is not a main verb
                    List<List<LexicalGraphNode>> nodeLists = getNodeChain(g, subNode, getChildren, getNegationChain, visitedNodes, headNodeIsCopula);
                    for (List<LexicalGraphNode> nodeList : nodeLists) {
                        if (!nodeList.contains(node)) {
                            nodeList.add(0, node);
                            retChains.add(nodeList);
                        }
                    }
                }
            }
        } else if (ConstantsParsing.chainPredicatesOnCopularBe && node.lexItem.getLemma().equals("be")) {

            List<LexicalGraphNode> leafList = new LinkedList<>();
            List<LexicalGraphNode> subNodes = new ArrayList<LexicalGraphNode>();
            if (getChildren) {
                subNodes.addAll(g.successors(node));
            } else {
                subNodes.addAll(g.predecessors(node));
            }

            for (LexicalGraphNode subNode : subNodes) {

                if ((subNode.lexItem.getIndex() > node.lexItem.getIndex()) && !headNodeIsCopula) {
                    if (getChildren) {
                        if (!leafList.contains(subNode)) {
                            leafList.add(0, subNode);
                        }
                        if (!leafList.contains(node)) {
                            leafList.add(0, node);
                        }
                    } else {
                        if (!leafList.contains(node)) {
                            leafList.add(0, node);
                        }
                        if (!leafList.contains(subNode)) {
                            leafList.add(0, subNode);
                        }
                    }

                    retChains.add(leafList);
                }
            }
        } else {
            // Leaf node found
            List<LexicalGraphNode> subNodes = new ArrayList<LexicalGraphNode>();
            if (getChildren) {
                subNodes.addAll(g.successors(node));
            } else {
                subNodes.addAll(g.predecessors(node));
            }
            if (subNodes.size() == 0) {
                List<LexicalGraphNode> leafList = new LinkedList<>();
                leafList.add(node);
                retChains.add(leafList);
            }

        }
        return retChains;
    }


    // Get the chain of auxiliary verbs
    public static List<LexicalGraphNode> getAuxiliaryVerbChain(MutableValueGraph g, LexicalGraphNode node, List<LexicalGraphNode> auxVerbChain) {
        Set<LexicalGraphNode> parents = g.predecessors(node);
        if (!parents.isEmpty()) {
            for (LexicalGraphNode parent : parents) {
                if (parent.lexItem.getAuxVerb() != null) {
                    auxVerbChain.add(parent);
                    auxVerbChain = getAuxiliaryVerbChain(g, parent, auxVerbChain);
                }
            }
        }
        return auxVerbChain;
    }

    // Get the lowest strength value of a set of modals
    public static Integer getLowestModalStrength(HashMap<Integer, Pair<String, Integer>> connectedModals) {
        Integer strength = null;
        for (Integer modalIdx : connectedModals.keySet()) {
            if (strength == null || connectedModals.get(modalIdx).second < strength) {
                strength = connectedModals.get(modalIdx).second;
            }
        }
        return strength;
    }

    // Extract a set of relations from the graph
    public static Set<Relation> getSemanticParse(MutableValueGraph g, LexicalGraphNode headNode,
                                                 List<LexicalGraphNode> negNodes, HashMap<LexicalGraphNode, Boolean> modalNodes,
                                                 List<LexicalGraphNode> attitudeNodes, List<LexicalGraphNode> conditionalNodes,
                                                 List<LexicalGraphNode> counterfactualNodes) {
        Set<Relation> relations = new HashSet<Relation>();
        // Check if the head node is a preposition which should attach to a verb. If so exclude it.
        // For example "Colorado struggled to get anything past Smith" - "to" is attached to "struggled"
        // so find relations for "struggled" (will get struggled.to) but not "to"
        if (headNode.lexItem.getPreposition()) {
            Set<LexicalGraphNode> headNodePredecessors = g.predecessors(headNode);
            for (LexicalGraphNode pre : headNodePredecessors) {
                if (pre.lexItem.getSemCatType().equals(SemanticCategoryType.EVENT)) {
                    return relations;
                }
            }
        }
        // Traverse the graph starting at the headNode
        // Get children
        Set<LexicalGraphNode> children = g.successors(headNode);
        for (LexicalGraphNode child : children) {
            List<LexicalGraphNode> chain = new ArrayList<LexicalGraphNode>();
            List<LexicalGraphNode> particles = new ArrayList<LexicalGraphNode>();
            // Add the main verb to the chain
            chain.add(headNode);
            // Check for particles attached to the verb (appear after the main verb)
            // and for auxiliary verbs (appear before the main verb)
            Set<LexicalGraphNode> parents = g.predecessors(headNode);
            if (!parents.isEmpty()) {
                for (LexicalGraphNode parent : parents) {
                    // RP = particle
                    if (ConstantsParsing.includeParticlesInPredicate) {
                        if (parent.lexItem.getPosTag().equals("RP") && g.successors(parent).size() == 1) {
                            particles.add(parent);
                        }
                    }
                    // Auxiliary
                    if (parent.lexItem.getAuxVerb() != null) {
                        // Recursively check for auxiliary verbs
                        List<LexicalGraphNode> auxAuxVerbs = new ArrayList<>();
                        auxAuxVerbs.add(parent);
                        auxAuxVerbs = getAuxiliaryVerbChain(g, parent, auxAuxVerbs);
                        for (LexicalGraphNode av : auxAuxVerbs) {
                            chain.add(0, av);
                        }
                    }
                }
            }
            // Construct head chains: auxiliary verbs + main verb + particles
            List<List<LexicalGraphNode>> headChains = new ArrayList<>();
            if (!particles.isEmpty()) {
                for (LexicalGraphNode particle : particles) {
                    List<LexicalGraphNode> particleHeadChain = new ArrayList<>();
                    particleHeadChain.addAll(chain);
                    particleHeadChain.add(particle);
                    headChains.add(particleHeadChain);
                }
            } else {
                headChains.add(chain);
            }
            // Traverse the child node of the main verb
            List<LexicalGraphNode> visitedNodes = new ArrayList<>();
            Boolean headNodeIsCopula = (headNode.lexItem.getLemma().equals("be")) ? Boolean.TRUE : Boolean.FALSE;
            List<List<LexicalGraphNode>> childChains = getNodeChain(g, child, Boolean.TRUE, Boolean.FALSE, visitedNodes, headNodeIsCopula);
            Boolean negation = checkIfNegated(g, headNode, negNodes);
            // Get set of copula nodes
            List<LexicalGraphNode> copulaNodes = getCopulaNodes(g);
            Pair<HashMap<LexicalGraphNode, Boolean>, Set<String>> connectedModals = getConnectedModals(g, headNode, modalNodes, copulaNodes);
            HashMap<LexicalGraphNode, Boolean> connectedModalNodes = connectedModals.first;
            Boolean hasModal = (connectedModalNodes.size() > 0) ? Boolean.TRUE : Boolean.FALSE;
            Set<String> modals = connectedModals.second;
            Pair<Set<String>, Set<String>> connectedAttitudeVerbs = getConnectedAttitudeVerbs(g, headNode, attitudeNodes);
            Boolean isAttitudeSay = (connectedAttitudeVerbs.first.isEmpty()) ? Boolean.FALSE : Boolean.TRUE;
            Boolean isAttitudeThink = (connectedAttitudeVerbs.second.isEmpty()) ? Boolean.FALSE : Boolean.TRUE;
            Set<String> attitudeVerbs = Util.mergeMultipleSets(Arrays.asList(connectedAttitudeVerbs.first,connectedAttitudeVerbs.second));
            Set<String> conditionals = getConnectedConditionals(g, headNode, conditionalNodes);
            Boolean isConditional = (conditionals.isEmpty()) ? Boolean.FALSE : Boolean.TRUE;
            Set<String> counterfactuals = getConnectedCounterfactuals(g, headNode, counterfactualNodes);
            Boolean isCounterfactual = (counterfactuals.isEmpty()) ? Boolean.FALSE : Boolean.TRUE;

            // Get the edgeValue from the chain
            // if the chain has a particle in it look for the value between first and third node
            // else look for the value between first and second node
            Optional<String> predSlot = null;
            for (List<LexicalGraphNode> headChain : headChains) {
                for (List<LexicalGraphNode> subChildChain : childChains) {
                    List<LexicalGraphNode> childChain = new ArrayList<LexicalGraphNode>();
                    childChain.addAll(headChain);
                    childChain.addAll(subChildChain);

                    // Strip all particles and auxiliaries to get the predSlot (and adjectives)
                    List<LexicalGraphNode> predSlotChain = new ArrayList<>();
                    for (LexicalGraphNode cNode : childChain) {
                        if (cNode.lexItem.getAuxVerb() == null && (!cNode.lexItem.getPosTag().equals("RP") )) {
                            predSlotChain.add(cNode);
                        }
                    }

                    if (predSlotChain.size() > 1) {
                        // Due to combining the PoS and CCG tags to identify verbs, there can be disagreements
                        // which when reducing the chains down to main verbs, prepositions, and arguments
                        // can leave a chain of only a single element
                        if (((predSlotChain.get(1).lexItem.getPosTag().contains("JJ") || predSlotChain.get(1).lexItem.getSemCatType().equals(SemanticCategoryType.TYPEMOD)) && !predSlotChain.get(1).lexItem.getSemCatType().equals(SemanticCategoryType.TYPE)) && predSlotChain.size() > 2) {
                            predSlot = g.edgeValue(predSlotChain.get(1), predSlotChain.get(2));
                        }
                        else {
                            predSlot = g.edgeValue(predSlotChain.get(0), predSlotChain.get(1));
                        }

                        Relation rel = getRelation(childChain, predSlot, negation, hasModal, connectedModalNodes,
                                isAttitudeSay, isAttitudeThink, isConditional, modals, conditionals, attitudeVerbs,
                                isCounterfactual, counterfactuals);
                        if (rel != null && !predSlot.isEmpty()) {
                            relations.add(rel);
                        }
                    }
                }
            }
        }
        return relations;
    }

    // Get the PoS tag of a multi-word named entity
    public static String getMultiNEPosTag(List<String> posTags) {
        if (posTags.contains("NNPS")) {
            return "NNPS";
        } else if (posTags.contains("NNP")) {
            return "NNP";
        } else if (posTags.contains("NNS")) {
            return "NNS";
        } else if (posTags.contains("NN")) {
            return "NN";
        } else return "";
    }

    // Get the PoS tag of a multi-word named entity
    public static Integer getMultiCorefClusterIDs(List<Integer> cluserIDs) {
        for (Integer c : cluserIDs) if (c != null) return c;
        return null;
    }

    // Get the antecedent of a pronoun. Do not allow pronominal mentions to be added as antecedents
    public static String getAntecedent(String posTag, CorefChain chain) {
        if (chain != null && posTag.equals("PRP")) {
            CorefChain.CorefMention representativeMention = chain.getRepresentativeMention();
            if (!representativeMention.mentionType.equals(Dictionaries.MentionType.PRONOMINAL)) {
                String mention = representativeMention.mentionSpan.replace(" ", "-");
                return mention;
            }
        }
        return null;
    }

    // Get calendar value for a given CoreEntity Mention
    public static Pair<Calendar, Calendar> getDateTime(CoreEntityMention cem, Calendar docTimeStamp) {
        // Get time expression
        Pair<Calendar, Calendar> dateRange = null;
        String dateString = "";
        if (cem.coreMap().get(TimeAnnotations.TimexAnnotation.class) != null) {
            try {
                dateString = cem.coreMap().get(TimeAnnotations.TimexAnnotation.class).value();
                if (dateString.equals("PRESENT_REF")) { // Set date range to be the document date
                    dateRange = new Pair<Calendar, Calendar>(docTimeStamp,docTimeStamp);
                }
                else {
                    dateRange = cem.coreMap().get(TimeAnnotations.TimexAnnotation.class).getRange();
                }
            } catch (Exception e) { // Not enough information to extract an actual date
                dateRange = null;
            }
        }
        return dateRange;
    }

    // Get internal representation of sentences in which multi-word named entities are merged into a single token
    public static CorenlpSentence getEntityMergedSentences(CoreSentence as, List<String> l, List<Integer> c, List<CoreEntityMention> multiNEs, Map<Integer, CorefChain> corefChains, Calendar docTimeStamp) {
        Boolean multiEntity = Boolean.FALSE;
        Integer multiEntityEnd = -1;
        List<String> tokens = new ArrayList<>();
        List<String> unmergedTokens = new ArrayList<>();
        List<String> lemmas = new ArrayList<>();
        List<String> posTags = new ArrayList<>();
        List<String> verbTenses = new ArrayList<>();
        List<String> nerTags = new ArrayList<>();
        List<Integer> corefClusterIDs = new ArrayList<>();
        List<String> antecedents = new ArrayList<>();
        List<CoreEntityMention> entityMentions = as.entityMentions();
        List<Pair<Calendar,Calendar>> timeExpressions = new ArrayList<>();
        HashMap<Integer, CoreEntityMention> multientities = new HashMap<Integer, CoreEntityMention>();
        HashMap<Integer, CoreEntityMention> singleentities = new HashMap<Integer, CoreEntityMention>();
        // Get token start index of every multi-word entity (indices start at 1)
        for (CoreEntityMention cem : multiNEs) {
            Integer startIndex = cem.tokens().get(0).index();
            multientities.put(startIndex, cem);
        }
        // Get token start index of every single-word entity (indices start at 1)
        for (CoreEntityMention cem : as.entityMentions()) {
            Integer firstTok = cem.tokens().get(0).index();
            Integer lastTok = cem.tokens().get(cem.tokens().size() - 1).index();
            if (firstTok.equals(lastTok)) {
                singleentities.put(firstTok, cem);
            }
        }
        // Construct merged sentences
        for (int i = 0; i < as.tokens().size(); i++) {
            unmergedTokens.add(as.tokens().get(i).value());
            // If it is the start of a multi-word named entity span
            if (multientities.containsKey(i + 1)) {
                CoreEntityMention cem = multientities.get(i + 1);
                multiEntity = Boolean.TRUE;
                multiEntityEnd = cem.tokens().get(cem.tokens().size() - 1).index();
                String multiText = cem.text().replace(" ", "-");
                String posTag = getMultiNEPosTag(as.posTags().subList(i, multiEntityEnd));
                Integer corefID = getMultiCorefClusterIDs(c.subList(i, multiEntityEnd));
                String nerTag = cem.entityType();
                tokens.add(multiText);
                lemmas.add(multiText);
                posTags.add(posTag);
                verbTenses.add(null); // We don't have the CCG tag yet so add a dummy value
                nerTags.add(nerTag);
                Pair<Calendar,Calendar> timeExpression = getDateTime(cem, docTimeStamp);
                timeExpressions.add(timeExpression);
                if (ConstantsParsing.includeCoreference) {
                    corefClusterIDs.add(corefID);
                    CorefChain corefChain = corefChains.get(corefID);
                    String antecedent = getAntecedent(posTag, corefChain);
                    antecedents.add(antecedent);
                } else {
                    corefClusterIDs.add(null);
                    antecedents.add(null);
                }
            } else {
                if (!multiEntity) { // Not a multi-word named entity - normal token
                    String posTag = as.posTags().get(i);
                    String nerTag = as.nerTags().get(i);
                    String lemma = l.get(i);
                    Integer corefClusterID = c.get(i);
                    tokens.add(as.tokens().get(i).word());
                    lemmas.add(lemma);
                    posTags.add(posTag);
                    verbTenses.add(null); // We don't have the CCG tag yet so add a dummy value
                    nerTags.add(nerTag);
                    Pair<Calendar,Calendar> dateRange = null;
                    if (singleentities.containsKey(i + 1)) {
                        CoreEntityMention cem = singleentities.get(i + 1);
                        dateRange = getDateTime(cem, docTimeStamp);
                    }
                    timeExpressions.add(dateRange);
                    if (ConstantsParsing.includeCoreference) {
                        corefClusterIDs.add(corefClusterID);
                        CorefChain corefChain = corefChains.get(corefClusterID);
                        String antecedent = getAntecedent(posTag, corefChain);
                        antecedents.add(antecedent);
                    } else {
                        corefClusterIDs.add(null);
                        antecedents.add(null);
                    }
                }
                if (i + 1 == multiEntityEnd) { // End of multi-word named entity reached
                    multiEntity = Boolean.FALSE;
                    multiEntityEnd = -1;
                }
            }

        }
        String text = String.join(" ", tokens);
        CorenlpSentence s = new CorenlpSentence(text, tokens, lemmas, posTags, verbTenses, nerTags,
                corefClusterIDs, antecedents, entityMentions, timeExpressions, unmergedTokens);
        return s;
    }

    // Remove auxiliary verbs from a list of nodes
    public static List<LexicalGraphNode> removeAuxiliaryVerbs(List<LexicalGraphNode> nodes) {
        List<LexicalGraphNode> noAux = new ArrayList<>();
        for (LexicalGraphNode n : nodes) {
            if (n.lexItem.getAuxVerb() == null) {
                noAux.add(n);
            }
        }
        return noAux;
    }

    // Format the strings for the semantic parse
    public static Set<String> formatSemanticParseOutput(Set<Relation> relations) {
        Set<String> semanticParse = new HashSet<String>();
        for (Relation r : relations) {
            String s = r.getPredicate() + "." + r.getArgSlot()
                    + "(" + r.getPredIndex() + ":e, "
                    + r.getArgIndex() + ":" + r.getArgument() + ")";
            semanticParse.add(s);
        }
        return semanticParse;
    }

    // Get the modifier string
    public static VerbModifier getModifierNegationAndTense(Set<Relation> modRels) {
        List<String> modifierElements = new ArrayList<String>();
        String modifier = "";
        Boolean negation = Boolean.FALSE;
        Boolean modal = Boolean.FALSE;
        Boolean modNeg = Boolean.FALSE;
        String tense = null;
        Set<Integer> modifiersCollected = new HashSet<Integer>();
        Boolean addToModifer = Boolean.TRUE;
        if (!modRels.isEmpty()) {
            // Sort the relations by their argIndex
            List<Relation> modRelsList = new ArrayList<>(modRels);
            Collections.sort(modRelsList, new RelationArgIndexComparator());
            for (Relation modRel : modRels) {
                if (modRel.getArgument().equals("e")) {
                    addToModifer = Boolean.TRUE;
                    // Don't allow preposition-only modifiers
                    if (modRel.getPredIsPreposition() && modifierElements.isEmpty()) {
                        addToModifer = Boolean.FALSE;
                    }
                    // Get the modifier string
                    if (addToModifer && !modifiersCollected.contains(modRel.getPredIndex())) {
                        modifierElements.add(modRel.getPredicate());
                    }
                    // Get the modifier tense
                    tense = modRel.getPredTense();
                    // Check if the modifier node is negated
                    Pair<Pair<Boolean, Boolean>, Boolean> x = getModalNegationForRelation(modRel);
                    modal = x.first.first;
                    negation = x.first.second;
                    modNeg = x.second;
                    // Add the modifier to the list of those already collected
                    modifiersCollected.add(modRel.getPredIndex());
                }
            }
        }
        if (!modifierElements.isEmpty()) {
            modifier = String.join(".", modifierElements) + "__";
        }
        VerbModifier result = new VerbModifier(modifier, modal, negation, modNeg, tense);
        return result;
    }

    // Remove the copular be and its unlemmatised form from the auxililary strings
    public static Pair<String,String> removeCopularBeFromAuxiliaryString(Relation r) {
        String[] auxParts = r.getAuxiliary().split("\\.");
        String[] auxLemParts = r.getAuxiliaryLemmatised().split("\\.");
        // Get the index of "be" in the lemmatised auxiliary
        String auxRem = "";
        String auxLemRem = "";
        for (int i = 0; i < auxLemParts.length; i++) {
            if (auxLemParts[i].equals("be")) {
                auxRem = auxParts[i];
                auxLemRem = auxLemParts[i];
            }
        }
        List<String> auxPartsList = new ArrayList<String>(Arrays.asList(auxParts));
        List<String> auxLemPartsList = new ArrayList<String>(Arrays.asList(auxLemParts));
        auxPartsList.remove(auxRem);
        auxLemPartsList.remove(auxLemRem);
        String auxString = String.join(".", auxPartsList);
        String auxLemString = String.join(".", auxLemPartsList);

        return new Pair<String, String>(auxString,auxLemString);
    }

    public static Pair<Pair<Boolean, Boolean>, Boolean> getModalNegationForRelation(Relation r) {
        Boolean modal = Boolean.FALSE;
        Boolean negation = Boolean.FALSE;
        Boolean negFromModal = Boolean.FALSE;
        List<Integer> connectedModStrengths = new ArrayList<>();
        for (Pair<String, Integer> connectedMod : r.getConnectedModalsIdxs().values()) {
            connectedModStrengths.add(connectedMod.second);
        }
        if (connectedModStrengths.contains(1)) {
            modal = Boolean.TRUE;
        }
        else if (connectedModStrengths.contains(0)) {
            negFromModal = Boolean.TRUE;
        }
        if (r.getNegation()) {
            negation = Boolean.TRUE;
        }
        Pair<Boolean, Boolean> temp = new Pair(modal, negation);
        Pair<Pair<Boolean, Boolean>, Boolean> result = new Pair(temp, negFromModal);
        return result;
    }

    // Determine whether to include modal (MOD) and negation (NEG) tags in the predicate
    public static Pair<String, String> getModalNegationTagsEventRelation(Relation rel, VerbModifier verbMod, Boolean includeRelWithNoModifier,
                                                                         Set<Relation> modRels) {
        String modal = "";
        String negation = "";
        Pair<Pair<Boolean, Boolean>, Boolean> mainVerbModalNegInfo = getModalNegationForRelation(rel);
        Boolean mainVerbModal = mainVerbModalNegInfo.first.first;
        Boolean mainVerbNeg = mainVerbModalNegInfo.first.second;
        Boolean mainVerbModNeg = mainVerbModalNegInfo.second;
        Boolean modifierVerbModal = verbMod.getModal();
        Boolean modifierVerbNeg = verbMod.getNegation();
        Boolean modifierVerbModNeg = verbMod.getModNeg();
        if (includeRelWithNoModifier || modRels.isEmpty()) {
            // Remove modifier from predicate, e.g. return bare relation
            modal = mainVerbModal ? "MOD__" : modifierVerbModal ? "MOD__" : ""; // If either the main verb or the modifier is connected to a modal
            if (modal.equals("")) { // Modal takes precedence over negation; only check for negation if there is no modal
                negation = mainVerbNeg ? "NEG__" : modifierVerbNeg ? "NEG__" : mainVerbModNeg ? "LNEG__" : "";
            }
        }
        else {
            // Include modifier from predicate
            negation = (mainVerbNeg || modifierVerbNeg) ? "NEG__" : "";
        }
        Pair<String, String> result = new Pair(modal, negation);
        return result;
    }

    // Return a single tag for the relation denoting the level of factivity:
    // NEG: negation
    // MOD: modal
    // COND: conditional
    // ATT_SAY: attitude verb "say"
    // ATT_THINK: attitude verb "think"
    // ATT_BOTH: attitude verb, both say and think
    public static String getRelationFactivityTag(String neg, String modal, String attitudeVerb, String conditional,
                                                 String counterfactual) {
        if (modal.equals("MOD__")) {
            return modal;
        }
        else if (attitudeVerb.startsWith("ATT")) {
            if (neg.equals("NEG__") || neg.equals("LNEG__")) {
                String negAtt = attitudeVerb + neg;
                return negAtt;
            }
            else {
                return attitudeVerb;
            }
        }
        else if (conditional.equals("COND__")) {
            return conditional;
        }
        else if (counterfactual.equals("COUNT__")) {
            return counterfactual;
        }
        else if (neg.equals("NEG__") || (neg.equals("LNEG__"))) {
            return neg;
        }
        else {
            return "";
        }
    }

    // Format the string of the unary relation (machine readable)
    public static String formatUnaryRelString(Relation r, Set<Relation> modRels, Calendar docTime, Boolean includeRelWithNoModifier) {
        VerbModifier verbMod = getModifierNegationAndTense(modRels);
        String modifier = (ConstantsParsing.removeModifiers || includeRelWithNoModifier) ? "" : verbMod.getModifier();
        Boolean predContainsModifier = Boolean.FALSE;
        if (modifier.equals("") && r.getPredicate().contains(".be")){
            if (!includeRelWithNoModifier) {
                modifier = r.getPredicate().split("\\.be")[0] + "__";
            }
            predContainsModifier = Boolean.TRUE;
        }
        Pair<String, String> modalNeg = getModalNegationTagsEventRelation(r, verbMod, includeRelWithNoModifier, modRels);
        String neg = modalNeg.second;
        String modal = modalNeg.first;
        String attitudeVerb = (r.getIsAttitudeSay()) ? "ATT_SAY__" : (r.getIsAttitudeThink()) ? "ATT_THINK__"
                : (r.getIsAttitudeThink() && r.getIsAttitudeSay()) ? "ATT_BOTH__" : "";
        String conditional = (r.getIsConditional()) ? "COND__" : "";
        String counterfactual = (r.getIsCounterfactual()) ? "COUNT__" : "";
        String startDate = (r.getDate() == null) ? "null" : r.getDate().first.getTime().toString();
        String endDate = (r.getDate() == null) ? "null" : r.getDate().second.getTime().toString();
        String temporalString = (ConstantsParsing.includeTemporal) ? " [" + startDate + "] [" + endDate + "]" : "";
        String tenseString = (ConstantsParsing.includeTenseAndAspect) ? " " + r.getPredTense() : "";
        if (ConstantsParsing.includeTenseAndAspect && r.getPredTense() == null) {
            tenseString = " " + verbMod.getTense();
        }
        String aspectString = (ConstantsParsing.includeTenseAndAspect) ? " " + r.getPredAspect() : "";
        String predicateString = (ConstantsParsing.lemmatizePred) ? r.getPredicateLemmatised() : r.getPredicate();
        String auxString = r.getAuxiliary();
        String auxLemString = r.getAuxiliaryLemmatised();
        // Strip the copular from the auxiliary?
        if (ConstantsParsing.includeCopularBeForUnaryPresentParticiples && ConstantsParsing.includeAuxiliariesInPredicate) {
            if (r.getAuxiliaryLemmatised().contains("be")) {
                Pair<String,String> auxRemd = removeCopularBeFromAuxiliaryString(r);
                auxString = auxRemd.first;
                auxLemString = auxRemd.second;
            }
        }
        // Remove "be." from the start of the predicate?
        if (ConstantsParsing.removeCopularBeUnary && predicateString.startsWith("be.")) {
            predicateString = predicateString.substring(3);
        }
        // Add "be." to the start of the predicate if there is a copular auxiliary
        if (ConstantsParsing.includeCopularBeForUnaryPresentParticiples && !ConstantsParsing.removeCopularBeUnary
                && r.getAuxiliaryLemmatised().contains("be") && r.getpredCCGFeature().equals("ng")) {
            predicateString = "be." + predicateString;
        }
        // If the predicate string contains the modifier, strip the modifier from the predicate
        if (predContainsModifier) {
            String mod = "";
            if (ConstantsParsing.lemmatizePred) {
                mod = r.getPredicateLemmatised().split("\\.be")[0] + ".";
            }
            else {
                mod = r.getPredicate().split("\\.be")[0] + ".";
            }
            predicateString = predicateString.replace(mod,"");
        }
        String openingTag = (ConstantsParsing.includeAuxiliariesInPredicate) ? auxString + "[" : "";
        String closingTag = (ConstantsParsing.includeAuxiliariesInPredicate) ? "] " : " ";
        String arg = (ConstantsParsing.lemmatizeArguments) ? r.getArgumentLemmatised() : r.getArgument();
        Integer predIndex = (r.getCopularBeIndex() == null) ? r.getPredIndex() : r.getCopularBeIndex();

        // Get the predicate and connected modals, and update the modal stats
        if (ConstantsParsing.writeUnaryRels && (includeRelWithNoModifier || modRels.isEmpty())) {
            String predStringForModalStats = predicateString + "." + r.getArgSlot().charAt(0);
            updateModalStats(r, predStringForModalStats, modal, neg);
        }

        // Get the tag: MOD, ATT_x, COND, NEG
        String tag = getRelationFactivityTag(neg, modal, attitudeVerb, conditional, counterfactual);
        String relString =
                openingTag
                        + tag
                        + modifier
                        + predicateString + "." + r.getArgSlot().charAt(0) + closingTag
                        + arg + " "
                        + predIndex + " "
                        + r.getArgumentType() + " 0"
                        + tenseString
                        + aspectString
                        + temporalString;
        return relString;
    }

    // Format the string of the binary relation (machine readable)
    public static String formatBinaryRelString(Relation leftPred, Relation rightPred, Set<Relation> modRels, Calendar docTime,
                                               Boolean includeRelWithNoModifier, HashMap<Integer, List<Integer>> tokenMap, List<String> sentUnmergedTokens) {
        VerbModifier verbMod = getModifierNegationAndTense(modRels);
        String modifier = (ConstantsParsing.removeModifiers || includeRelWithNoModifier) ? "" : verbMod.getModifier();
        Pair<String, String> modalNeg = getModalNegationTagsEventRelation(leftPred, verbMod, includeRelWithNoModifier, modRels);
        String neg = modalNeg.second;
        String modal = modalNeg.first;
        String attitudeVerb = "";
        if (includeRelWithNoModifier || modRels.isEmpty()) {
            attitudeVerb = (leftPred.getIsAttitudeSay()) ? "ATT_SAY__" : (leftPred.getIsAttitudeThink()) ? "ATT_THINK__"
                    : (leftPred.getIsAttitudeThink() && leftPred.getIsAttitudeSay()) ? "ATT_BOTH__" : "";
        }
        String conditional = (leftPred.getIsConditional()) ? "COND__" : "";
        String counterfactual = (leftPred.getIsCounterfactual()) ? "COUNT__" : "";
        String startDate = (leftPred.getDate() == null) ? "null" : leftPred.getDate().first.getTime().toString();
        String endDate = (leftPred.getDate() == null) ? "null" : leftPred.getDate().second.getTime().toString();
        String temporalString = (ConstantsParsing.includeTemporal) ? " [" + startDate + "] [" + endDate + "]" : "";
        String tenseString = (ConstantsParsing.includeTenseAndAspect) ? " " + leftPred.getPredTense() : "";
        if (ConstantsParsing.includeTenseAndAspect && leftPred.getPredTense() == null) {
            tenseString = " " + verbMod.getTense();
        }
        String aspectString = (ConstantsParsing.includeTenseAndAspect) ? " " + leftPred.getPredAspect() : "";
        String leftPredicate = leftPred.getPredicate();
        String leftPredicateLemmatised = leftPred.getPredicateLemmatised();
        String rightPredicate = rightPred.getPredicate();
        String rightPredicateLemmatised = rightPred.getPredicateLemmatised();
        String leftPredString;
        String rightPredString;
        String auxString = leftPred.getAuxiliary();
        String auxLemString = leftPred.getAuxiliaryLemmatised();

        // For copular relations involving adjectives: If the left hand side predicate string
        // contains only an adjective, and there is no overlap between the left and right hand side
        // predicate strings, as in the following example:
        // (be.predominant.1,be.in.2) shintoism japan
        // manipulate the right hand side predicate string such that the adjective is included:
        // (be.predominant.1,be.predominant.in.2) shintoism japan
        if (leftPred.getPredIsCopular() && leftPred.getPredContainsAdjective()
                && !rightPredicateLemmatised.contains(leftPredicateLemmatised)) {
            String newPredicateCopular = rightPredicate.split("\\.")[0] + ".";
            rightPredicate = rightPredicate.replace(newPredicateCopular, leftPredicate + ".");
            rightPredicateLemmatised = rightPredicateLemmatised.replace("be.", leftPredicateLemmatised + ".");
            if (ConstantsParsing.removeCopularBeBinaryForAdjectiveRels && rightPredicate.contains(".")) {
                rightPredicate = rightPredicate.substring(rightPredicate.indexOf(".")+1);
                rightPredicateLemmatised = rightPredicateLemmatised.substring(3);
            }
        }

        // Copular verb: if left predicate is just "be", and the second element of the right predicate, immediately
        // following "be" is a noun, use right predicate string for both left and right sides.
        // Can be toggled on/off using ConstantsParsing.replaceLeftBeWithRightPredStringForTwoHops
        // E.g. (be.1,be.part.of.2) sweden european-union
        // ---> (be.part.of.1,be.part.of.2) sweden european-union
        if ((ConstantsParsing.replaceLeftBeWithRightPredStringForTwoHops && rightPred.getPredIsDoubleHopRelationOnEntityOrType()
                || rightPred.getPredContainsAdjective())
                && leftPred.getPredIsCopular() && leftPred.getPredicateLemmatised().equals("be")) {
            leftPredString = (ConstantsParsing.lemmatizePred) ? rightPredicateLemmatised : rightPredicate;
        } else {
            leftPredString = (ConstantsParsing.lemmatizePred) ? leftPredicateLemmatised : leftPredicate;
        }
        rightPredString = (ConstantsParsing.lemmatizePred) ? rightPredicateLemmatised : rightPredicate;
        // Remove "be." for copulars?
        if (!leftPredString.equals("be") && rightPredString.startsWith("be.") && (ConstantsParsing.removeCopularBeBinary || rightPred.getPredIsDoubleHopRelationOnEntityOrType()
                || (ConstantsParsing.removeCopularBeBinaryForAdjectiveRels && (leftPred.getPredContainsAdjective() || rightPred.getPredContainsAdjective())))) {
            rightPredString = rightPredString.substring(3);
        }
        if (leftPredString.startsWith("be.") && (ConstantsParsing.removeCopularBeBinary || rightPred.getPredIsDoubleHopRelationOnEntityOrType()
                || (ConstantsParsing.removeCopularBeBinaryForAdjectiveRels && (leftPred.getPredContainsAdjective() || rightPred.getPredContainsAdjective())))) {
            leftPredString = leftPredString.substring(3);
        }

        // Strip the copular from the auxiliary?
        if (ConstantsParsing.includeCopularBeForBinaryPresentParticiples && ConstantsParsing.includeAuxiliariesInPredicate) {
            if (leftPred.getAuxiliaryLemmatised().contains("be")) {
                Pair<String,String> auxRemd = removeCopularBeFromAuxiliaryString(leftPred);
                auxString = auxRemd.first;
                auxLemString = auxRemd.second;
            }
        }
        // Add "be." to the start of the predicate if there is a copular auxiliary
        if (ConstantsParsing.includeCopularBeForBinaryPresentParticiples && !ConstantsParsing.removeCopularBeBinary
                && leftPred.getAuxiliaryLemmatised().contains("be") && leftPred.getpredCCGFeature().equals("ng")) {
            leftPredString = "be." + leftPredString;
            rightPredString = "be." + rightPredString;
        }

        // Remove any trailing periods leftover from string manipulations, from leftPredString and rightPredString
        leftPredString = leftPredString.replaceAll("\\.+$", "");
        rightPredString = rightPredString.replaceAll("\\.+$", "");
        String openingTag = (ConstantsParsing.includeAuxiliariesInPredicate) ? auxString + "[" : "";
        String closingTag = (ConstantsParsing.includeAuxiliariesInPredicate) ? "] " : " ";
        String leftArg = (ConstantsParsing.lemmatizeArguments) ? leftPred.getArgumentLemmatised() : leftPred.getArgument();
        String rightArg = (ConstantsParsing.lemmatizeArguments) ? rightPred.getArgumentLemmatised() : rightPred.getArgument();

        // Get the predicate and connected modals, and update the modal stats
        if (includeRelWithNoModifier || modRels.isEmpty()) {
            String predStringForStats = "(" + leftPredString + "." + leftPred.getArgSlot().charAt(0) + "," + rightPredString + "." + rightPred.getArgSlot().charAt(0) + ")";
            updateModalStats(leftPred, predStringForStats, modal, neg);
        }

        String tag = getRelationFactivityTag(neg, modal, attitudeVerb, conditional, counterfactual);

        // Format output string
        String relString = "";
        if (ConstantsParsing.nytRelExtraction) {
            // TODO - implement factivity tagging for NYT extraction?
            // Format: (pred.1,pred.2) arg1 arg2 arg1-span arg2-span predIndex EG 0
            // Exclude all tense / temporal information
            leftArg = getUnmergedArgString(leftPred.getArgIndex(), tokenMap, sentUnmergedTokens);
            rightArg = getUnmergedArgString(rightPred.getArgIndex(), tokenMap, sentUnmergedTokens);
            Pair<Integer,Integer> leftArgSpan = getArgSpan(leftPred.getArgIndex(), tokenMap, sentUnmergedTokens, leftArg.length());
            Pair<Integer,Integer> rightArgSpan = getArgSpan(rightPred.getArgIndex(), tokenMap, sentUnmergedTokens, rightArg.length());
            relString =
                    openingTag
                            + neg
                            + modifier
                            + "(" + leftPredString + "." + leftPred.getArgSlot().charAt(0) + ","
                            + rightPredString + "." + rightPred.getArgSlot().charAt(0) + ")"
                            + closingTag
                            + leftArg + " " + rightArg + " "
                            + leftArgSpan + " " + rightArgSpan + " "
                            + leftPred.getPredIndex() + " "
                            + leftPred.getArgumentType() + rightPred.getArgumentType() + " 0";

        }
        else {
            relString =
                    openingTag
                            + tag
                            + modifier
                            + "(" + leftPredString + "." + leftPred.getArgSlot().charAt(0) + ","
                            + rightPredString + "." + rightPred.getArgSlot().charAt(0) + ")"
                            + closingTag
                            + leftArg + " " + rightArg + " "
                            + leftPred.getPredIndex() + " "
                            + leftPred.getArgumentType() + rightPred.getArgumentType() + " 0"
                            + tenseString
                            + aspectString
                            + temporalString;
        }
        return relString;
    }

    // Get the character level argument span from the unmerged (but tokenised) sentence
    public static Pair<Integer, Integer> getArgSpan(Integer argIndex, HashMap<Integer, List<Integer>> tokenMap,
                                                    List<String> sentUnmergedTokens, Integer argLength) {
        List<Integer> argTokenIds = tokenMap.get(argIndex);
        Integer argStartTokIndex = Collections.min(argTokenIds);
        Integer startCharCount = 0;
        if (argStartTokIndex != 0) {
            List<String> preArgIndexParts = sentUnmergedTokens.subList(0, argStartTokIndex);
            String preArgString = String.join(" ", preArgIndexParts) + " ";
            startCharCount = preArgString.length();
        }
        Integer endCharCount = startCharCount + argLength -1;
        Pair<Integer, Integer> span = new Pair(startCharCount, endCharCount);
        return span;
    }

    // Get the unmerged argument string
    public static String getUnmergedArgString(Integer argIndex, HashMap<Integer, List<Integer>> tokenMap, List<String> sentUnmergedTokens) {
        List<String> argParts = new ArrayList<>();
        for (Integer i : tokenMap.get(argIndex)) {
            argParts.add(sentUnmergedTokens.get(i));
        }
        String argString = String.join(" ", argParts).replace(" ", "-");
        return argString;
    }

    // Remove the last .x element in a predicate string (for passive to active conversion)
    public static String removeLastElementOfPredicate(String pred) {
        String clippedPred = "";
        List<String> predPartsList = new ArrayList<String>(Arrays.asList(pred.split("\\.")));
        if (!predPartsList.isEmpty()) {
            clippedPred = String.join(".", predPartsList.subList(0, predPartsList.size() - 1));
        }
        return clippedPred;
    }

    // If there is more than one relation with the same predicate index and the same argument, remove the one
    // with the shortest predicate string
    public static Map<Integer, Set<Relation>> removeOverlappingRelations(Map<Integer, Set<Relation>> indRels) {
        Map<Integer, Set<Relation>> result = new HashMap<>();
        // Find the unique predicate index - argument index pairs
        for (Integer predIndex : indRels.keySet()) {
            Map<Integer, Relation> predArgIndex = new HashMap<>();
            for (Relation r : indRels.get(predIndex)) {
                // No relation with the argument index
                if (!predArgIndex.containsKey(r.getArgIndex())) {
                    predArgIndex.put(r.getArgIndex(), r);
                }
                // Check if the relation has a longer predicate string than the one already in the dictionary
                else {
                    String existingRelPred = predArgIndex.get(r.getArgIndex()).getPredicate();
                    if (r.getPredicate().length() > existingRelPred.length()) {
                        predArgIndex.put(r.getArgIndex(), r);
                    }
                }
            }
            // Recombine the reduced set of relations
            Set<Relation> reducedRels = new HashSet<>();
            for (Integer argIndex : predArgIndex.keySet()) {
                reducedRels.add(predArgIndex.get(argIndex));
            }
            result.put(predIndex, reducedRels);
        }
        return result;
    }

    // Get UNARY relation strings of the form (walk.1) man
    public static List<String> getUnaryRelStrings(Relation element, Set<Relation> modifiedByRelations, Calendar docTime) {
        List<String> result = new ArrayList<>();
        String unaryRelString = formatUnaryRelString(element, modifiedByRelations, docTime, Boolean.FALSE);
        result.add(unaryRelString);
        // If there are modifiers, and removebasicEvnetifEEModifer is set to false, add the same relation without the modifier
        if(!modifiedByRelations.isEmpty() && !ConstantsParsing.removebasicEvnetifEEModifer) {
            String unaryRelStringNoMod = formatUnaryRelString(element, modifiedByRelations, docTime, Boolean.TRUE);
            if (!result.contains(unaryRelStringNoMod)) {
                result.add(unaryRelStringNoMod);
            }
        }
        return result;
    }

    // Get BINARY relation strings of the form (chase.1,chase.2) cat mouse
    public static List<String> getBinaryRelStrings(Relation leftPred, Relation rightPred, Set<Relation> modifiedByRelations,
                                                   Calendar docTime, HashMap<Integer, List<Integer>> tokenMap, List<String> sentUnmergedTokens) {
        List<String> result = new ArrayList<>();
        String binaryRelString = formatBinaryRelString(leftPred, rightPred, modifiedByRelations, docTime, Boolean.FALSE,
                tokenMap, sentUnmergedTokens);
        result.add(binaryRelString);
        // If there are modifiers, and removebasicEvnetifEEModifer is set to false, add the same relation without the modifier
        if(!modifiedByRelations.isEmpty() && !ConstantsParsing.removebasicEvnetifEEModifer) {
            String binaryRelStringNoMod = formatBinaryRelString(leftPred, rightPred, modifiedByRelations, docTime, Boolean.TRUE,
                    tokenMap, sentUnmergedTokens);
            if (!result.contains(binaryRelStringNoMod)) {
                result.add(binaryRelStringNoMod);
            }
        }
        return result;
    }

    // Format the strings for the relations
    public static Map<String, Set<String>> getBinariesAndUnaries(Set<Relation> relations, HashMap<Integer,
            List<Integer>> modifiedBy, Calendar docTime, HashMap<Integer, List<Integer>> tokenMap, List<String> unmergedSentTokens) {
        List<List<Relation>> binaryRelations = new ArrayList<List<Relation>>();
        Set<String> binaryRelationStrings = new HashSet<String>();
        Set<String> binaryRelationWithoutSentNumStrings = new HashSet<String>();
        Set<String> binaryRelationNEOnlyStrings = new HashSet<String>();
        Set<String> unaryRelationStrings = new HashSet<String>();
        Map<String, Set<String>> output = new HashMap<String, Set<String>>();
        Map<Integer, Set<Relation>> indexedRels = new HashMap<Integer, Set<Relation>>();
        Boolean copularFound = Boolean.FALSE;
        Integer upperLimitAcceptableRelation = (ConstantsParsing.acceptGGBinary) ? 3 : 2;
        // Find all relations with the same predicate index
        for (Relation r : relations) {
            if (r.getPredIsCopular()) {
                copularFound = Boolean.TRUE;
            }
            if (ConstantsParsing.excludeExistentialThere && r.getArgumentPOSTag().equals("EX")) {
                continue;
            }
            else {
                if (indexedRels.containsKey(r.getPredIndex())) {
                    indexedRels.get(r.getPredIndex()).add(r);
                } else {
                    Set<Relation> relList = new HashSet<Relation>() {{
                        add(r);
                    }};
                    indexedRels.put(r.getPredIndex(), relList);
                }
            }
        }
        // If there is more than one relation with the same predicate index and the same argument, remove the one
        // with the shortest predicate string e.g. is.1(1:e, 0:tom), is.funny.2(1:e, 0:tom) from "Tom is funny"
        if (copularFound) {
            indexedRels = removeOverlappingRelations(indexedRels);
        }
        // Get modifiers for each predicate (if they exist)
        HashMap<Integer, Set<Relation>> modifiedByRelationsPredIndexed = new HashMap<Integer, Set<Relation>>();
        // Pair up relations to form binaries, or if there is only one relation for a predicate, add it as a unary
        for (Integer predIndex : indexedRels.keySet()) {
            // Get modifier(s)
            Set<Relation> modifiedByRelations = new HashSet<Relation>();
            if (modifiedBy.containsKey(predIndex)) {
                for (Integer mod : modifiedBy.get(predIndex)) {
                    if (indexedRels.containsKey(mod)) {
                        modifiedByRelations.addAll(indexedRels.get(mod));
                        modifiedByRelationsPredIndexed.put(predIndex, modifiedByRelations);
                    }
                }
            }
            // Check if all relations have 1 as the argslot
            Set<Relation> relsForIndex = indexedRels.get(predIndex);
            List<String> relArgSlotsList = new ArrayList<>();
            for (Relation rfi: relsForIndex) {
                relArgSlotsList.add(rfi.getArgSlot());
            }
            Set<String> relArgSlotsSet = new HashSet<String>(relArgSlotsList);
            Boolean allRelsHaveSameArgSlot1 = (relArgSlotsSet.size() == 1 && relArgSlotsSet.contains("1")) ? Boolean.TRUE : Boolean.FALSE;

            if (indexedRels.get(predIndex).size() == 1) { // Unary
                Relation element = Iterables.getOnlyElement(indexedRels.get(predIndex));
                Boolean acceptUnaryRelation = acceptableUnaryRelation(element);
                // Don't accept any unaries where the argument is "e", or according to conditions set out in acceptableUnaryRelation
                if ((!element.getArgument().equals("e")) && acceptUnaryRelation) {
                    // Switch passives to active
                    if (element.getPassive()) {
                        if (String.valueOf(element.getArgSlot().charAt(0)).equals("1")) {
                            element.setArgSlot("2");
                        }
                    }
                    // Machine readable relation output
                    List<String> unRelStrings = getUnaryRelStrings(element, modifiedByRelations, docTime);
                    unaryRelationStrings.addAll(unRelStrings);
                }
            }
            else if (allRelsHaveSameArgSlot1) { // All relations have .1 argslot -> make them unaries
                HashMap<Integer, String> longestPredStrings = new HashMap<>();
                HashMap<Integer, List<String>> longestRelStrings = new HashMap();
                // No binaries to be constructed, add the longest one as a unary instead
                // For example: "Charles seems hungry": seems.hungry.1 charles should be output
                for (Relation element : indexedRels.get(predIndex)) {
                    Integer argIndex = element.getArgIndex();
                    if (!longestRelStrings.containsKey(argIndex)) {
                        longestRelStrings.put(argIndex, new ArrayList<String>());
                        longestPredStrings.put(argIndex, "");
                    }
                    if (element.getPredicate().length() > longestPredStrings.get(argIndex).length()) {
                        longestRelStrings.put(argIndex, getUnaryRelStrings(element, modifiedByRelations, docTime));
                        longestPredStrings.put(argIndex, element.getPredicate());
                    }
                }
                for (Integer argIndex : longestRelStrings.keySet()) {
                    unaryRelationStrings.addAll(longestRelStrings.get(argIndex));
                }
            }
            else { // Binary
                // Get a list of relations and sort by argIndex
                Set<Relation> relsSet = indexedRels.get(predIndex);
                List<Relation> rels = new ArrayList<>(relsSet);
                Collections.sort(rels, new RelationArgIndexComparator());
                // Get all pairs of relations from the list in order i.e. the first relation has the lower argIndex
                for (int i = 0; i < rels.size(); i++) {
                    for (int j = 0; j < rels.size(); j++) {
                        if (i < j) {
                            Boolean differentArgSlotsBeforeChanges = Boolean.FALSE;
                            Relation leftPred = rels.get(i);
                            Relation rightPred = rels.get(j);
                            String leftPredPredicate = leftPred.getPredicate();
                            String rightPredPredicate = rightPred.getPredicate();
                            // Switch passives to active
                            String newPredicate;
                            String newPredicateLemmatised;
                            if (rightPred.getPassive() && rightPred.getPredicate().contains(".by")) {
                                leftPred = rels.get(j);
                                if (leftPred.getPredicate().contains(".")) {
                                    newPredicate = removeLastElementOfPredicate(leftPred.getPredicate());
                                    newPredicateLemmatised = removeLastElementOfPredicate(leftPred.getPredicateLemmatised());
                                }
                                else {
                                    newPredicate = leftPred.getPredicate();
                                    newPredicateLemmatised = leftPred.getPredicateLemmatised();
                                }
                                if (!leftPred.getArgSlot().substring(0,1).equals(rightPred.getArgSlot().substring(0,1))) {
                                    differentArgSlotsBeforeChanges = Boolean.TRUE;
                                }
                                leftPred.setPredicate(newPredicate);
                                leftPred.setPredicateLemmatised(newPredicateLemmatised);
                                leftPred.setArgSlot("1");
                                rightPred = rels.get(i);
                                rightPred.setArgSlot("2");
                            }
                            else if (rightPred.getPassive()) {
                                if (rightPred.getPredicate().contains(".")) {
                                    if (!leftPred.getArgSlot().substring(0,1).equals(rightPred.getArgSlot().substring(0,1))) {
                                        differentArgSlotsBeforeChanges = Boolean.TRUE;
                                    }
                                    leftPred.setArgSlot("2");
                                }
                                else {
                                    Integer acceptPassRelation = acceptableBinaryRelation(leftPred, rightPred, differentArgSlotsBeforeChanges);
                                    if (acceptPassRelation >= 0) {
                                        binaryRelations.add(Arrays.asList(leftPred, rightPred));
                                    }
                                    if (!leftPred.getArgSlot().substring(0,1).equals(rightPred.getArgSlot().substring(0,1))) {
                                        differentArgSlotsBeforeChanges = Boolean.TRUE;
                                    }
                                    leftPred = rels.get(j);
                                    leftPred.setArgSlot("1");
                                    rightPred = rels.get(i);
                                    rightPred.setArgSlot("2");
                                }
                            }

                            // Enforce order of arguments and argslots of possessives
                            if ((leftPred.getPredicate().equals("\'") && rightPred.getPredicate().equals("\'") && leftPred.getArgIndex() > rightPred.getArgIndex())
                                || (leftPred.getPredicate().equals("\'s") && rightPred.getPredicate().equals("\'s") && leftPred.getArgIndex() > rightPred.getArgIndex())) {
                                leftPred = rels.get(j);
                                leftPred.setArgSlot("1");
                                rightPred = rels.get(i);
                                rightPred.setArgSlot("2");
                            }

                            // Determine if the relation should be accepted
                            Integer acceptRelation = acceptableBinaryRelation(leftPred, rightPred, differentArgSlotsBeforeChanges);
                            // Reject the following...
                            // Do not allow binary relations where the predicate contains 's, unless both arguments are named entities
                            if (acceptRelation != 2 && leftPredPredicate.contains("\'s") && rightPredPredicate.contains("\'s")) {
                                acceptRelation = -1;
                            }
                            // Do not allow binary relations where the predicate+argindex of the left and right sides are identical
                            if (leftPredPredicate.equals(rightPredPredicate) && leftPred.getArgIndex().equals(rightPred.getArgIndex())) {
                                acceptRelation = -1;
                            }
                            if ((acceptRelation >= 0 && acceptRelation <= upperLimitAcceptableRelation)
                                    || (acceptRelation == 4 && ConstantsParsing.includeBareCopularBinaries && (!(leftPred.getArgumentType().equals("G") && rightPred.getArgumentType().equals("G") && !ConstantsParsing.acceptGGBinary)))) {
                                // Machine readable relation output
                                List<String> binRelStrings = getBinaryRelStrings(leftPred, rightPred, modifiedByRelations, docTime, tokenMap, unmergedSentTokens);
                                binaryRelationStrings.addAll(binRelStrings);
                                if (acceptRelation.equals(2)) { // If both arguments are NEs, add to a separate list
                                    binaryRelationNEOnlyStrings.addAll(binRelStrings);
                                }
                                // Add ents / gens to master list as appropriate
                                addGensEnts(acceptRelation, leftPred.getArgument(), rightPred.getArgument());
                            }
                            if (acceptRelation >= 0) {
                                // Add binary relation to a list which will be used later to extract two-hop relations
                                binaryRelations.add(Arrays.asList(leftPred, rightPred));
                            }
                            if (acceptRelation == 4 || acceptRelation == 5) {
                                // Convert to a unary
                                Relation binToUnary = convertCopularBinaryToUnary(leftPred, rightPred);
                                List<String> unRelStrings = getUnaryRelStrings(binToUnary, modifiedByRelations, docTime);
                                unaryRelationStrings.addAll(unRelStrings);
                            }
                        }
                    }
                }
            }
        }
        // Extract two-hop relations
        Map<String, Set<String>> twoHopBinaryRelations = getTwoHopRelations(binaryRelations, modifiedByRelationsPredIndexed, docTime, tokenMap, unmergedSentTokens);
        binaryRelationStrings.addAll(twoHopBinaryRelations.get("binary"));
        binaryRelationNEOnlyStrings.addAll(twoHopBinaryRelations.get("binary-OnlyNEs"));
        unaryRelationStrings.addAll(twoHopBinaryRelations.get("unary"));
        // Create the set of relations without the sentence numbers
        binaryRelationWithoutSentNumStrings = getBinRelsWithoutSentNums(binaryRelationStrings);
        // Compile output
        output.put("binary", binaryRelationStrings);
        output.put("binary-OnlyNEs", binaryRelationNEOnlyStrings);
        output.put("binary-without-sent-num", binaryRelationWithoutSentNumStrings);
        output.put("unary", unaryRelationStrings);
        return output;
    }

    public static Relation convertCopularBinaryToUnary(Relation left, Relation right) {
        String predicate = right.getPredicate() + "." + right.getArgument();
        String predicateLemmatised = right.getPredicateLemmatised() + "." + right.getArgument();
        Relation converted = new Relation(left.getNegation(), left.getPassive(), left.getpredCCGFeature(), predicate, predicateLemmatised,
                left.getAuxiliary(), left.getAuxiliaryLemmatised(), left.getHasModal(), left.getConnectedModalsIdxs(),
                left.getIsAttitudeSay(), left.getIsAttitudeThink(), left.getIsConditional(),
                left.getModalStrings(), left.getAttVerbStrings(), left.getConditionalStrings(),
                left.getIsCounterfactual(), left.getCounterfactualStrings(), left.getPredIsCopular(),
                left.getPredIsDoubleHopRelationOnEntityOrType(), left.getPredIsVerb(), left.getPredIsPreposition(),
                left.getPredContainsAdjective(), left.getPredTense(), left.getPredAspect(), left.getPredIndex(),
                right.getCopularBeIndex(),
                left.getArgument(), left.getArgumentLemmatised(), left.getArgumentType(), left.getArgumentPOSTag(),
                left.getArgumentCCGTag(), left.getArgIndex(), left.getArgSlot(), left.getArgSemCatType(),
                left.getDate(), left.getResolvedCoref());
        return converted;
    }

    // Strip the sentence numbers (last token in the string) from the relation strings
    public static Set<String> getBinRelsWithoutSentNums(Set<String> binaryRelStrings) {
        Set<String> relsWithoutSentNums = new HashSet<String>();
        for (String binaryRel : binaryRelStrings) {
            String[] split = binaryRel.split(" ");
            String[] temp = Arrays.copyOf(split, split.length - 1);
            String relWithoutSentNum = String.join(" ", temp);
            relsWithoutSentNums.add(relWithoutSentNum);
        }
        return relsWithoutSentNums;
    }

    // Check if the argument is an entity
    public static boolean isEntity(String pos, Boolean resolvedCoref, String argType) {
        return (pos.equals("NNP") || pos.equals("NNPS")) || (ConstantsParsing.includeCoreference && resolvedCoref)
                || (argType.equals("E"));
    }

    // Check if the argument is a noun or CD (hopefully the CD modifies a noun).
    public static boolean isNoun(String pos, String ccgTag, Boolean resolvedCoref) {
        return (pos.equals("NN") || pos.equals("NNS") || pos.equals("CD")) || (ConstantsParsing.includeCoreference && resolvedCoref || ccgTag.equals("N"));
    }

    // Determine whether the unary relation should be accepted
    public static Boolean acceptableUnaryRelation(Relation rel) {
        Boolean accept = Boolean.TRUE;
        SemanticCategoryType argSemCatType = rel.getArgSemCatType();
        String argCcgTag = rel.getArgumentCCGTag();
        String argType = rel.getArgumentType();
        // Exclude relations where the argument is not of "entity" or "type" semantic category
        if (!(argSemCatType.equals(SemanticCategoryType.ENTITY) || argSemCatType.equals(SemanticCategoryType.TYPE))) {
            accept = Boolean.FALSE;
        }

        // Exclude unaries where the predicate contains an adjective, e.g. be.funny.2 tom (from "Tom is funny"
        if (!ConstantsParsing.acceptAdjectiveUnary && rel.getPredContainsAdjective()) {
            accept = Boolean.FALSE;
        }
        // Exclude relations where the predicate is just a preposition e.g. to.1 london
        if (rel.getPredIsPreposition()) {
            accept = Boolean.FALSE;
        }
        // Exclude 's possessives and ' possessives
        if (rel.getPredicate().equals("\'s") || rel.getPredicate().equals("\'")) {
            accept = Boolean.FALSE;
        }
        if (ConstantsParsing.onlyNounOrNE) { // Exclude pronouns
            Boolean resolvedCoref = rel.getResolvedCoref();
            String argPosTag = rel.getArgumentPOSTag();
            if (!isEntity(argPosTag,resolvedCoref,argType) && !isNoun(argPosTag, argCcgTag, resolvedCoref)) {
                accept = Boolean.FALSE;
            }
        }
        return accept;
    }

	// -1: not good
	// 0: E G
	// 1: G E
	// 2: E E
	// 3: G G
    // 4: both predicates are copular verb e.g. (be.1,be.2) sweden part (which we may need for two-hop relations)
    // 5: predicate contains the copular verb "be" but not as the first element in the string
    // Determine whether the binary relation should be accepted
	public static Integer acceptableBinaryRelation(Relation leftPred, Relation rightPred, Boolean differentArgSlotsBeforeChanges) {
        String leftArgPOS = leftPred.getArgumentPOSTag();
        String rightArgPOS = rightPred.getArgumentPOSTag();
        String leftArgCCG = leftPred.getArgumentCCGTag();
        String rightArgCCG = rightPred.getArgumentCCGTag();
        SemanticCategoryType leftArgSemCatType = leftPred.getArgSemCatType();
        SemanticCategoryType rightArgSemCatType = rightPred.getArgSemCatType();
        Boolean leftResolvedCoref = leftPred.getResolvedCoref();
        Boolean rightResolvedCoref = rightPred.getResolvedCoref();
        String leftArgType = leftPred.getArgumentType();
        String rightArgType = rightPred.getArgumentType();

        // Check if the predicates and argumennts all contain at least one letter

        if (ConstantsParsing.acceptOnlyAlphabeticBinaryRelations) {
            if (!leftPred.getPredicate().matches(".*[a-z].*") || !rightPred.getPredicate().matches(".*[a-z].*") ||
                    !leftPred.getArgument().matches(".*[a-z0-9].*") || !rightPred.getArgument().matches(".*[a-z0-9].*")) {
                return -1;
            }
            if (leftPred.getArgument().contains("1/2") || rightPred.getArgument().contains("1/2")) {
                return -1;
            }
        }

        // Immediately reject relations where the predicate index, predicate string, and argslot values are all identical for left and right sides
        if (leftPred.getPredIndex().equals(rightPred.getPredIndex())
                && leftPred.getArgSlot().substring(0,1).equals(rightPred.getArgSlot().substring(0,1))
                && leftPred.getPredicate().equals(rightPred.getPredicate())
                && !differentArgSlotsBeforeChanges) {
            return -1;
        }

        // Detect copular relations, e.g. (be.1,be.2) sweden part
        if (leftPred.getPredIsCopular() && !leftPred.getPredicate().contains(".")
                && rightPred.getPredIsCopular() && !rightPred.getPredicate().contains(".")) {
            return 4;
        }

        // Detect relations containing a copular, e.g. (seems.1,seems.to.be.2) charles hungry
        if (rightPred.getPredicate().contains(".be")) {
            return 5;
        }

        // Immediately exclude relations where the arguments are not of "entity" or "type" semantic category
        if (ConstantsParsing.onlyNounOrNE) {
            if (!((leftArgSemCatType.equals(SemanticCategoryType.ENTITY) || leftArgSemCatType.equals(SemanticCategoryType.TYPE))
                    && (rightArgSemCatType.equals(SemanticCategoryType.ENTITY) || rightArgSemCatType.equals(SemanticCategoryType.TYPE)))) {
                return -1;
            }
        }

        // Do not allow binary relations where one of the arguments is "e" (event)
        if (leftPred.getArgument().equals("e") || rightPred.getArgument().equals("e")) {
            return -1;
        }

        // Immediately exclude relations where both arguments are the same
        if (leftPred.getArgIndex().equals(rightPred.getArgIndex())) {
            return -1;
        }

		if (!ConstantsParsing.acceptGGBinary) { // Don't allow relations where both arguments are general entities
			if (isEntity(leftArgPOS,leftResolvedCoref,leftArgType) && isEntity(rightArgPOS,rightResolvedCoref,rightArgType)) {// E E
				return 2;
			} else if (isEntity(leftArgPOS,leftResolvedCoref,leftArgType) && isNoun(rightArgPOS, rightArgCCG, rightResolvedCoref)) {
				return 0;
			} else if (isEntity(rightArgPOS,rightResolvedCoref,rightArgType) && isNoun(leftArgPOS, leftArgCCG, leftResolvedCoref)) {
				return 1;
			} else if (isNoun(leftArgPOS, leftArgCCG, leftResolvedCoref) && isNoun(rightArgPOS, rightArgCCG, rightResolvedCoref)) {
			    return 3;
            } else {
				return -1;
			}
		} else {

			if (!ConstantsParsing.onlyNounOrNE) { // Allow arguments to be something other than noun / named entity
				if (isEntity(leftArgPOS,leftResolvedCoref,leftArgType) && isEntity(rightArgPOS,rightResolvedCoref,rightArgType)) {// E E
					return 2;
				} else if (isEntity(leftArgPOS,leftResolvedCoref,leftArgType)) {
					return 0;
				} else if (isEntity(rightArgPOS,rightResolvedCoref,rightArgType)) {
					return 1;
				} else {
					return 3;
				}
			} else { // Arguments must be either noun or named entity
				if (isEntity(leftArgPOS,leftResolvedCoref,leftArgType) && isEntity(rightArgPOS,rightResolvedCoref,rightArgType)) {// E E
					return 2;
				} else if (isEntity(leftArgPOS,leftResolvedCoref,leftArgType) && isNoun(rightArgPOS,rightArgCCG,rightResolvedCoref)) {
					return 0;
				} else if (isEntity(rightArgPOS,rightResolvedCoref,rightArgType) && isNoun(leftArgPOS,leftArgCCG,leftResolvedCoref)) {
					return 1;
				} else if (isNoun(leftArgPOS,leftArgCCG,leftResolvedCoref) && isNoun(rightArgPOS,rightArgCCG,rightResolvedCoref)) {
					return 3;
				} else {
					return -1;
				}
			}
		}
	}

    // Extract the two-hop relations those where the second entity of the first binary relation matches the first
    // entity of the second binary relation
    // e.g. "Libya receives help from the United Nations"
    // (receive.1,receive.2) Libya help
    // (from.1,from.2) help United-Nations
    // -> (receive.1,receive.help.from.2) Libya United-Nations
    public static  Map<String, Set<String>> getTwoHopRelations(List<List<Relation>> binaryRels, HashMap<Integer,
            Set<Relation>> modRelsPRedIndexed, Calendar docTime, HashMap<Integer, List<Integer>> tokenMap, List<String> unmergedSentTokens){
        Map<String, Set<String>> output = new HashMap<String, Set<String>>();
        Set<String> twoHopBinaryRelStrings = new HashSet<String>();
        Set<String> twoHopBinaryRelNEOnlyStrings = new HashSet<String>();
        Set<String> unaryRelationStrings = new HashSet<>();
        // Get a mapping of leftPRed argument to binary relation
        HashMap<Integer, List<List<Relation>>> leftArgToBinRel = new HashMap<Integer, List<List<Relation>>>();
        for (List<Relation> binRel : binaryRels) {
            // Left predicate is the first element in list, right predicate is the second
            Relation leftPred = binRel.get(0);
            if (leftArgToBinRel.containsKey(leftPred.getArgIndex())) {
                leftArgToBinRel.get(leftPred.getArgIndex()).add(binRel);
            }
            else {
                List<List<Relation>> binRels = new ArrayList<>();
                binRels.add(binRel);
                leftArgToBinRel.put(leftPred.getArgIndex(), binRels);
            }
        }
        // Detect two-hop binary relations
        for (List<Relation> binRel : binaryRels) {
            Relation leftPred = binRel.get(0);
            Relation rightPred = binRel.get(1);
            if (leftArgToBinRel.containsKey(rightPred.getArgIndex())) {
                List<List<Relation>> twoHopBinRels = leftArgToBinRel.get(rightPred.getArgIndex());
                for (List<Relation> twoHopBinRel : twoHopBinRels) {
                    Relation temp = twoHopBinRel.get(1);
                    if (!(rightPred.getArgSlot().equals(twoHopBinRel.get(0).getArgSlot()) && rightPred.getPredIndex() > temp.getPredIndex())
                            && !(temp.getPredIsCopular() && !rightPred.getPredIsCopular())
                            && !rightPred.getPredIsPreposition()
                            && !temp.getPredicate().equals("\'s")
                            && !temp.getArgIndex().equals(leftPred.getArgIndex())
                            && !leftPred.getArgIndex().equals(rightPred.getArgIndex())
                            && !(rightPred.getPredicate().equals(temp.getPredicate()) && rightPred.getArgSlot().equals(temp.getArgSlot()))
                            && ((!leftPred.getArgSlot().equals(rightPred.getArgSlot()))
                                || (leftPred.getArgSlot().startsWith("2") && rightPred.getArgSlot().startsWith("2") && leftPred.getPassive()))) {
                        Set<Relation> modRels = new HashSet<Relation>();
                        if (modRelsPRedIndexed.containsKey(leftPred.getPredIndex())) {
                            modRels = modRelsPRedIndexed.get(leftPred.getPredIndex());
                        }
                        // Construct a new right hand side predicate that combines properties of the
                        // two right hand side predicates
                        // e.g.1 (receive.1,receive.2) Libya help -> receive.help
                        // (from.1,from.2) help United-Nations -> receive.help.from
                        // e.g.2 (give.1,give.2) virus rise -> give.rise
                        // (give.2,give.to.2) rise bronchiolitis -> give.rise.to
                        String twoHopRightPredString;
                        String twoHopRightPredLemmaString;
                        Boolean predIsDoubleHopRelationOnEntityOrType = (rightPred.getPredicateLemmatised().equals("be") && (rightPred.getArgSemCatType().equals(SemanticCategoryType.ENTITY) || rightPred.getArgSemCatType().equals(SemanticCategoryType.TYPE) || rightPred.getArgSemCatType().equals(SemanticCategoryType.TYPEMOD))) ? Boolean.TRUE : Boolean.FALSE;
                        // If there is an overlap between the right hand predicates of the first and second relations
                        // then remove the overlap
                        if (temp.getPredIndex().equals(rightPred.getPredIndex())) {
                            // Make a better overlap
                            List<String> tempList = new ArrayList<String>(Arrays.asList(temp.getPredicate().split("\\.")));
                            List<String> rightList = new ArrayList<String>(Arrays.asList(rightPred.getPredicate().split("\\.")));
                            List<String> tempLemmaList = new ArrayList<String>(Arrays.asList(temp.getPredicateLemmatised().split("\\.")));
                            List<String> rightLemmaList = new ArrayList<String>(Arrays.asList(rightPred.getPredicateLemmatised().split("\\.")));
                            tempList.retainAll(rightList);
                            tempLemmaList.retainAll(rightLemmaList);
                            String predReplace = String.join(".", tempList);
                            String predLemmaReplace = String.join(".", tempLemmaList);
                            String dedupPred = temp.getPredicate().replace(predReplace, "").replaceAll("^\\.", "");
                            String dedupPredLemmatised = temp.getPredicateLemmatised().replace(predLemmaReplace, "").replaceAll("^\\.", "");
                            twoHopRightPredString = rightPred.getPredicate() + "." + rightPred.getArgumentLemmatised() + "." + dedupPred;
                            twoHopRightPredLemmaString = rightPred.getPredicateLemmatised() + "." + rightPred.getArgumentLemmatised() + "." + dedupPredLemmatised;
                        } else { // No overlap
                            twoHopRightPredString = rightPred.getPredicate() + "." + rightPred.getArgumentLemmatised() + "." + temp.getPredicate();
                            twoHopRightPredLemmaString = rightPred.getPredicateLemmatised() + "." + rightPred.getArgumentLemmatised() + "." + temp.getPredicateLemmatised();
                        }
                        Boolean predContainsAdjective = (leftPred.getPredContainsAdjective() || rightPred.getPredContainsAdjective()) ? Boolean.TRUE : Boolean.FALSE;
                        Relation twoHopRightPred = new Relation(temp.getNegation(), temp.getPassive(), temp.getpredCCGFeature(),
                                twoHopRightPredString, twoHopRightPredLemmaString, temp.getAuxiliary(),
                                temp.getAuxiliaryLemmatised(), leftPred.getHasModal(), leftPred.getConnectedModalsIdxs(),
                                leftPred.getIsAttitudeSay(), leftPred.getIsAttitudeThink(), leftPred.getIsConditional(),
                                leftPred.getModalStrings(), leftPred.getAttVerbStrings(), leftPred.getConditionalStrings(),
                                leftPred.getIsCounterfactual(), leftPred.getCounterfactualStrings(),
                                leftPred.getPredIsCopular(), predIsDoubleHopRelationOnEntityOrType,
                                temp.getPredIsVerb(), temp.getPredIsPreposition(), predContainsAdjective,
                                temp.getPredTense(), temp.getPredAspect(), temp.getPredIndex(), temp.getCopularBeIndex(), temp.getArgument(),
                                temp.getArgumentLemmatised(), temp.getArgumentType(), temp.getArgumentPOSTag(),
                                temp.getArgumentCCGTag(), temp.getArgIndex(), temp.getArgSlot(), temp.getArgSemCatType(),
                                temp.getDate(), temp.getResolvedCoref());
                        List<String> binRelStrings = getBinaryRelStrings(leftPred, twoHopRightPred, modRels, docTime,
                                tokenMap, unmergedSentTokens);
                        Integer acceptRelation = acceptableBinaryRelation(leftPred, twoHopRightPred, Boolean.FALSE);
                        if (acceptRelation >= 0) {
                            if (ConstantsParsing.acceptGGBinary
                                    || (leftPred.getArgumentType().equals("E") || twoHopRightPred.getArgumentType().equals("E"))) {
                                twoHopBinaryRelStrings.addAll(binRelStrings);
                                // If both arguments are named entities, add to a separate list
                                if (leftPred.getArgumentType().equals("E") && twoHopRightPred.getArgumentType().equals("E")) {
                                    twoHopBinaryRelNEOnlyStrings.addAll(binRelStrings);
                                }
                                // Additionally: Add a two-hop binary involving a copular, as a unary relation
                                if (ConstantsParsing.addTwoHopBinaryCopularAsUnary) {
                                    Relation binToUnary = convertCopularBinaryToUnary(leftPred, twoHopRightPred);
                                    Boolean acceptUnaryRelation = acceptableUnaryRelation(binToUnary);
                                    if (acceptUnaryRelation) {
                                        List<String> unRelStrings = getUnaryRelStrings(binToUnary, modRels, docTime);
                                        unaryRelationStrings.addAll(unRelStrings);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        output.put("binary", twoHopBinaryRelStrings);
        output.put("binary-OnlyNEs", twoHopBinaryRelNEOnlyStrings);
        output.put("unary", unaryRelationStrings);
        return output;
    }

    // Get the set of verbs that act as modifiers for another verb
    public static HashMap<Integer, List<Integer>> getVerbModifiers(MutableValueGraph g, Set<Relation> rels){
        HashMap<Integer, List<Integer>> modifiedBy = new HashMap<Integer, List<Integer>>();
        for (Relation r : rels) {
            LexicalGraphNode predNode = getNodeByIndex(r.getPredIndex(), g);
            LexicalGraphNode argNode = getNodeByIndex(r.getArgIndex(), g);
            Boolean pathExistsBetweenNodes = checkIfPathBetweenNodes(g, predNode, argNode, Boolean.FALSE);
            if (r.getArgument().equals("e") && pathExistsBetweenNodes && (!r.getPredIsCopular()) && (!r.getPredIsPreposition()) && (!r.getPredicate().startsWith("going"))) {
                if (modifiedBy.containsKey(r.getArgIndex())) {
                    modifiedBy.get(r.getArgIndex()).add(r.getPredIndex());
                }
                else {
                    List<Integer> mods = new ArrayList<Integer>() {{
                        add(r.getPredIndex());
                    }};
                    modifiedBy.put(r.getArgIndex(), mods);
                }
            }
        }
        return modifiedBy;
    }

    // Load coreNLP output from string
    private static CoreDocument loadCoreNLPOutputFromString(String coreNLPStr) throws IOException, ClassNotFoundException {
        AnnotationSerializer serializer = new GenericAnnotationSerializer();
        byte[] b = Base64.getDecoder().decode(coreNLPStr);
        InputStream is = new ByteArrayInputStream(b);
        Pair<CoreDocument, InputStream> pair = serializer.readCoreDocument(is);
        pair.second.close();
        is.close();
        CoreDocument doc = pair.first;
        return doc;
    }

    // Force finding method is used for other datasets, where sentences may be incomplete.
    // For example: "Leader, be elected as president of, country" in BreantProcessing.java
	// Do your best to find a good relation from the non-natural sentence.
    // That means, rel, arg1, arg2 should have different indexes
	public String[] extractPredArgsStrsForceFinding(String text, String ccgParseTree, String arg1, String arg2, boolean acceptNP,
			boolean longestRel, boolean debug)
            throws ArgumentValidationException, IOException, InterruptedException,
            ClassNotFoundException, ParserConfigurationException, SAXException {

		String ret = "";
		boolean partlyMatched = false;

		// Text preprocessing
		text = Util.preprocess(text);
		System.out.println("preprocessed text: " + text);
		String sentence = text;

        String[] predArgsStrs = extractPredArgsStrs(text, ccgParseTree, 0, acceptNP, true, null);
        String[] dsStrs;
        if (predArgsStrs.length > 1) {
            if (!ConstantsParsing.restrictToRelsWithoutModifiers) {
                dsStrs = predArgsStrs[2].split("\n");// This might have multiple candidates itself. Let's see
                // if any of those are good
            }
            else {
                String[] allDsStrs = predArgsStrs[2].split("\n");// This might have multiple candidates itself. Let's see
                // if any of those are good
                // First filter out those with modifiers
                List<String> filteredDsStrs = new ArrayList<String>();
                for (int i = 0; i < allDsStrs.length; i++) {
                    if (!(allDsStrs[i].contains("_(") && !allDsStrs[i].contains("NEG__("))) {
                        filteredDsStrs.add(allDsStrs[i]);
                    }
                }
                dsStrs = new String[filteredDsStrs.size()];
                dsStrs = filteredDsStrs.toArray(dsStrs);
            }
            if (debug) {
                System.out.println("cont: " + predArgsStrs[3]);
            }

            boolean argsMatch = false;
            String thisDSStr = "";
            if (debug) {
                System.out.println(text);
                System.out.println("dsStrs: " + predArgsStrs[2]);
            }

            arg1 = Util.getLemma(arg1).replace("_", " ");// ADDED 14 Jan 18
            arg2 = Util.getLemma(arg2).replace("_", " ");
            for (String cand : dsStrs) {
                if (debug) {
                    System.out.println("cand: " + cand);
                }
                if (cand.equals("")) {
                    continue;
                }
                String[] ss = cand.split(" ");
                String[] thisArgs = new String[]{ss[1], ss[2]};
                for (int i = 0; i < thisArgs.length; i++) {
                    thisArgs[i] = thisArgs[i].replace("-", " ");
                    thisArgs[i] = thisArgs[i].toLowerCase();
                    thisArgs[i] = Util.getLemma(thisArgs[i]);
                }

                boolean thisMatch = thisArgs[0].length() > 0 && thisArgs[1].length() > 0
                        && ((arg1.contains(thisArgs[0]) && arg2.contains(thisArgs[1]))
                        || (arg1.contains(thisArgs[1]) && arg2.contains(thisArgs[0])));

                if (thisMatch) {
                    if (debug) {
                        System.out.println("matched for: " + cand + " " + arg1 + " " + arg2 + " " + thisArgs[0]);
                    }
                } else {
                    if (debug) {
                        System.out.println("nope: " + cand + " " + arg1 + " " + arg2 + " " + thisArgs[0]);
                    }
                }

                if (thisDSStr.equals("")) {
                    thisDSStr = cand;
                    argsMatch = thisMatch;
                } else {
                    if (debug) {
                        System.out.println("matched for second time");
                    }
                    if (argsMatch == thisMatch) {// for both cases, it matches
                        thisDSStr += "$$" + cand;
                    } else {
                        if (thisMatch) {// previosly didn't match, this is the
                            // first match!
                            thisDSStr = cand;
                            argsMatch = thisMatch;
                        }
                    }
                }
            }

            boolean thisPartlyMatch = false;

            if (argsMatch && thisDSStr.length() > 0) {

                if (predArgsStrs[3].equals("true")) {// not same indexes
                    if (longestRel) {// to be used for Zeichner's data that wants to trick with implicative verbs,
                        // etc
                        thisDSStr = getLongestRel(thisDSStr);
                    }
                    return new String[]{thisDSStr, "true"};
                } else {
                    thisPartlyMatch = true;
                }
            }

            if (ret.equals("") || (!partlyMatched && (thisPartlyMatch || thisDSStr.length() > ret.length()))
                    || (argsMatch && thisDSStr.length() > ret.length())) {
                ret = thisDSStr;// At least it's something, but if we find
                // a better one for another parse, we accept that one
            }

            if (!acceptNP) {
                if (debug) {
                    System.out.println("not matched: " + text);
                }
            }

            if (longestRel) {// to be used for Zeichner's data that wants to trick with implicative verbs,
                // etc
                ret = getLongestRel(ret);
            }
        }
		return new String[] { ret, "false" };
	}

	// Required for force finding method in Javad's original pipeline
	public String getLongestRel(String ret) {
		String[] ss = ret.split("\\$\\$");
		System.out.println("num $$ split: " + ss.length);
		String l = ss[0];
		for (int i = 1; i < ss.length; i++) {
			if (ss[i].length() > l.length()) {
				System.out.println("using longer rel: " + ss[i]);
				l = ss[i];
			}
		}
		ret = l;
		return ret;
	}

    // Added for backwards compatability with Javad's pipeline (e.g. calls from NewsQAProcessing)
    // We no longer have LexicalGraph objects so "String graphs" is a dummy value
    // syntaxIdx will always be 0 as we use only the 1-best CCG parse
    public String[] extractPredArgsStrs(String text, String ccgParseTree, int syntaxIdx, boolean acceptNP, boolean acceptGG, String graphs)
            throws FileNotFoundException, IOException, ClassNotFoundException, ParserConfigurationException, SAXException {
        String[] ret = new String[0];
        List<String> textList = new ArrayList<String>();
        textList.add(text);
        List<String> ccgParseTreeList = new ArrayList<String>();
        if (!ccgParseTree.equals("")) {
            ccgParseTreeList.add(ccgParseTree);
        }
        Pair<List<String[]>, Pair<Pair<String,String>,String>> result = extractPredArgsStrs(textList, "", ccgParseTreeList, "");

        if (!result.first.isEmpty()) {
            ret = result.first.get(0);
        }
		return ret;
	}

	// Clean tokenised output to replace all instances of one or more < or > (necessary for CCG parsing with EasyCCG)
    public static List<String> cleanTokenisedSentences(List<String> sentences) {
        List cleanSentences = new ArrayList<String>();
        Pattern rightAngleBracketPattern = Pattern.compile("\\s>+\\s");
        Pattern leftAngleBracketPattern = Pattern.compile("\\s<+\\s");
        Pattern htmlTag = Pattern.compile("<+\\S+>+");
        Pattern anyOtherAngleBrackets = Pattern.compile("[<>]+");
        for (String sent : sentences) {
            String paddedSent = " " + sent + " ";
            Matcher rMatch = rightAngleBracketPattern.matcher(paddedSent);
            Matcher lMatch = leftAngleBracketPattern.matcher(paddedSent);
            Matcher htmlMatcher = htmlTag.matcher(paddedSent);
            while (lMatch.find()) {
                paddedSent = paddedSent.replace(lMatch.group(0), " -LRB- ");
            }
            while (rMatch.find()) {
                paddedSent = paddedSent.replace(rMatch.group(0), " -RRB- ");
            }
            while (htmlMatcher.find()) {
                paddedSent = paddedSent.replace(htmlMatcher.group(0), "-html-tag-");
            }
            // The presence of any other angle-bracket will likely be a string of nonsense
            Matcher anyOtherAngleMatcher = anyOtherAngleBrackets.matcher(paddedSent);
            while (anyOtherAngleMatcher.find()) {
                paddedSent = paddedSent.replace(anyOtherAngleMatcher.group(0), "-RRB-");
            }
            cleanSentences.add(paddedSent.trim());
        }
        return cleanSentences;
    }

	// Clean CCG parse trees to replace angle-brackets > and  < which represent tokens, with a placeholder
	public List<String> cleanCCGTrees(List<String> parseTrees) {
        List cleanTrees = new ArrayList<String>();
        Pattern p = Pattern.compile("<L\\s\\S+\\sX\\sX\\s\\S*[<>\\(\\)]+\\S*\\sX>");
        for (String tree : parseTrees) {
            Matcher m = p.matcher(tree);
            while (m.find()) {
                String[] leafElements = m.group(0).split(" ");
                leafElements[4] = "-illegal-sequence-";
                String replacementLeaf = String.join(" ",leafElements);
                tree = tree.replace(m.group(0), replacementLeaf);
            }
             cleanTrees.add(tree);
        }
        return cleanTrees;
    }

    // Return a mapping from the merged tokens and unmerged tokens
    public HashMap<Integer, List<Integer>> getTokenMap(List<String> mergedTokens, List<String> unmergedTokens) {
        HashMap<Integer, List<Integer>> tokenMap = new HashMap<>();
        List<String> tokenSubParts = new ArrayList<>();
        List<Integer> tokenSubInds = new ArrayList<>();
        Integer j = 0; // mergedTokens counter
        for (int i = 0; i < unmergedTokens.size(); i++) {
            tokenSubParts.add(unmergedTokens.get(i));
            tokenSubInds.add(i);
            if (unmergedTokens.get(i).equals(mergedTokens.get(j))) {
                List<Integer> mapTo = new ArrayList<>();
                mapTo.add(i);
                tokenMap.put(j, mapTo);
                tokenSubParts = new ArrayList<>();
                tokenSubInds = new ArrayList<>();
                j += 1;
            }
            else if ((String.join("", tokenSubParts).replace("-","")).equals(mergedTokens.get(j).replace("-",""))) {
                tokenMap.put(j, tokenSubInds);
                tokenSubParts = new ArrayList<>();
                tokenSubInds = new ArrayList<>();
                j += 1;
            }
        }
        return tokenMap;
    }

    // Extract the relations (predicate-argument strings)
    public Pair<List<String[]>, Pair<Pair<String,String>,String>> extractPredArgsStrs(List<String> sentences, String coreNLPStr, List<String> ccgParserStrs, String timeMLStr) throws FileNotFoundException, IOException, ClassNotFoundException, ParserConfigurationException, SAXException {
        List<Map<String, Set<String>>> docOutput = new ArrayList<>();

        CoreDocument doc = null;

        List<String> CCGTrees = new ArrayList<>();

        // CORENLP: TOKENISATION, NER, COREFERENCE RESOLUTION - run for all modes of CCG parsing

        // If reading CoreNLP output from a previous run, load output from file
        if (ConstantsParsing.readCoreNLPFromFile && !coreNLPStr.equals("")) {
            doc = loadCoreNLPOutputFromString(coreNLPStr);
            if (ConstantsParsing.includeCoreference && doc.corefChains() == null) {
                System.out.println("Coreference chains are missing");
                doc = processCoreNLP(sentences);
            }
        }
        else {
            // Run CoreNLP
            doc = processCoreNLP(sentences);
        }

        // List of annotated sentences
        List<CoreSentence> annotatedSents = doc.sentences();

        // Document-level coref chains
        Map<Integer, CorefChain> corefChains = doc.corefChains();

        // Get date of the document
        Date docDate = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            docDate = sdf.parse(doc.docDate());
        }
        catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        Calendar docTimeStamp = Calendar.getInstance();
        docTimeStamp.setTime(docDate);

        // Get internal representation of a sentence
        List<CorenlpSentence> mergedSents = new ArrayList<>();
        List<String> sentsForParsing = new ArrayList<>();
        for (CoreSentence as: annotatedSents) {
            // Get lemmas and coref cluster IDs for words in sentence
            List<String> lemmas = new ArrayList<>();
            List<Integer> corefClusterIDs = new ArrayList<>();
            CoreMap cm = as.coreMap();
            for (CoreLabel token: cm .get(CoreAnnotations.TokensAnnotation.class)) {
                // Lemmas
                String lemma = token.get(CoreAnnotations.LemmaAnnotation.class);
                lemmas.add(lemma);
                // Coref cluster IDs
                Integer corefClustId = token.get(CorefCoreAnnotations.CorefClusterIdAnnotation.class);
                corefClusterIDs.add(corefClustId);
            }
            // Get multi-word named entities
            List<CoreEntityMention> multiWordNEs = getMultiWordNEs(as);
            // Construct internal representation of sentence with merged named entities
            CorenlpSentence mergedSent = getEntityMergedSentences(as, lemmas, corefClusterIDs, multiWordNEs, corefChains, docTimeStamp);
            mergedSents.add(mergedSent);
            // Get text for parsing
            sentsForParsing.add(mergedSent.text());

            // Add time expression to LinesHandler allTimeExprs
            for (int i = 0; i < mergedSent.timeExpressions().size(); i++) {
                if (mergedSent.timeExpressions().get(i) != null) {
                    addTimeExprs(mergedSent.tokens().get(i));
                }
            }
        }

        // Write coreNLP output to string
        AnnotationSerializer serializer = new GenericAnnotationSerializer();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        serializer.writeCoreDocument(doc, baos).close();
        baos.flush();
        String coreNLPOutput = Base64.getEncoder().encodeToString(baos.toByteArray());
        baos.close();

        // If reading CoreNLP output from a previous run, load output from file
        // Only perform CCG parsing and relation extraction steps if running online parsing (with EasyCCG)
        // or reading CCG parse trees from file.
        if (ConstantsParsing.readCCGParsesFromFile || ConstantsParsing.onlineCCGParsing) {

            // CCG PARSING

            List<ParseResultForJava> parsedSents = new ArrayList<ParseResultForJava>();

            if (ConstantsParsing.readCCGParsesFromFile) {
                // Clean up CCG parse tree strings to replace ">" tokens from the original input sentence
                List<String> cleanedCCGParserStrings = cleanCCGTrees(ccgParserStrs);
                // Read in CCG tree strings and construct dependencies only
                parsedSents = getCCGParserDeps(cleanedCCGParserStrings);
            } else { // Online CCG parsing
                // Run the CCG parser and write output to file
                // EasyCCG will return null for any sentence that it cannot parse. If processing at the document
                // level, parse one sentence at a time so that issues can be caught (and ignored) at the sentence
                // level without having to "throw away" an entire document
                // N.B. Milos Stanojevic's Rotating Parser will *never* return a null
                if (ConstantsParsing.docLevelProcessing || docProcessing) {
                    // Document-level processing
                    for (String sent : sentsForParsing) {
                        try {
                            // DO SOMETHING HERE TO RESTRICT SENTENCES OF LENGTH > 100 (EasyCCG does not parse them
                            // and outputs an ugly error message)
                            String[] sentElements = sent.split("\\s+");
                            if (sentElements.length > 100) {
                                parsedSents.add(null);
                            }
                            else {
                                List<String> sentList = new ArrayList<String>() {{ add(sent); }};
                                List<ParseResultForJava> parsedSent = runCCGParser(sentList);
                                parsedSents.add(parsedSent.get(0));
                            }

                        }
                        catch (NullPointerException e) {
                            parsedSents.add(null);
                        }
                    }
                }
                else {
                    // Sentence-level processing
                    try {
                        parsedSents = runCCGParser(sentsForParsing);
                    }
                    catch (NullPointerException e) {
                        parsedSents.add(null);
                    }
                }
            }

            // RELATION EXTRACTION

            // For each sentence: construct LexicalGraph combining information from CoreNLP and the parser
            for (int i = 0; i < parsedSents.size(); i++) {

                // Get corresponding parser output and CoreNLP annotations
                ParseResultForJava ps = parsedSents.get(i);
                CorenlpSentence as = mergedSents.get(i);

                // Get a mapping between merged and unmerged tokens (for NYT data)
                HashMap<Integer, List<Integer>> tokenMap = new HashMap<>();
                if (ConstantsParsing.nytRelExtraction) {
                    tokenMap = getTokenMap(as.tokens(), as.unmergedTokens());
                }

                // Set up storage for results
                Set<Relation> relations = new HashSet<Relation>();
                Set<String> sentenceString = new HashSet<String>();
                Set<String> syntacticTreeString = new HashSet<String>();
                Set<String> semanticParse = new HashSet<String>();
                Map<String, Set<String>> relationOutput = new HashMap<String, Set<String>>();
                Boolean GLUERuleInParse;

                if (ps == null) {
                    // No CCG parse. Skip relation extraction for this sentence
                    sentenceString.add(as.text());
                    syntacticTreeString.add("__no_parse__");
                    relationOutput.put("semantic-parse", semanticParse);
                    relationOutput.put("syntactic-parse", syntacticTreeString);
                    relationOutput.put("sentence", sentenceString);
                    docOutput.add(relationOutput);
                    CCGTrees.add("__no_parse__");
                }
                else {
                    // Construct the lexical graph
                    MutableValueGraph lg = getLexicalGraph(ps, as);

                    // Get semantic parse:

                    // Get a list of negation nodes
                    List<LexicalGraphNode> negNodes = getSemCatTypeNodes(lg, SemanticCategoryType.NEGATION);
                    // Get a list of modal nodes
                    HashMap<LexicalGraphNode, Boolean> modalNodes = getModalNodes(lg, negNodes);
                    // Get a list of attitude nodes
                    List<LexicalGraphNode> attitudeNodes = getAttitudeNodes(lg);
                    // Get a list of counterfactual "had" nodes
                    List<LexicalGraphNode> counterfactualNodes = getCounterfactualNodes(lg);
                    // Get a list of conditional nodes
                    List<LexicalGraphNode> conditionalNodes = getConditionalNodes(lg);
                    // Get a list of all verb nodes
                    List<LexicalGraphNode> allVerbNodes = getSemCatTypeNodes(lg, SemanticCategoryType.EVENT);
                    // Remove auxiliary verbs
                    List<LexicalGraphNode> verbNodes = removeAuxiliaryVerbs(allVerbNodes);
                    // Get all time nodes
                    List<LexicalGraphNode> timeNodes = getTimeNodes(lg);
                    // Copy time expressions to main verbs
                    lg = copyTimeToVerbs(lg, timeNodes, verbNodes);

                    // Get the relations (assuming the list of verb nodes is not empty)
                    if (!verbNodes.isEmpty()) {
                        for (LexicalGraphNode verbNode : verbNodes) {
                            Set<Relation> rels = getSemanticParse(lg, verbNode, negNodes, modalNodes, attitudeNodes, conditionalNodes, counterfactualNodes);
                            relations.addAll(rels);
                        }
                    }

                    // If "generateRelationForAdjectiveCopulas" set to true: handle adjectival copulas that link a second argument to the main verb
                    // E.g. "Arsenal is capable of winning the match"
                    // [is.capable.of.2(1:e, 4:e), winning.2(4:e, 6:match), is.capable.1(1:e, 0:arsenal), is.1(1:e, 0:arsenal)]
                    // generate missing relation --> winning.1 arsenal
                    // to extract the binary:
                    // MOD__(win.1,win.2) arsenal match
                    List<LexicalGraphNode> copulaNodes = getCopulaNodes(lg);
                    if (ConstantsParsing.generateRelationForAdjectiveCopulas) {
                        relations = linkArgumentsAcrossAdjectivalCopula(relations, lg, negNodes, modalNodes, copulaNodes,
                                attitudeNodes, conditionalNodes, counterfactualNodes);
                    }

                    // Detect verbs that modify other verbs
                    HashMap<Integer, List<Integer>> modifiedBy = getVerbModifiers(lg, relations);

                    // Format output
                    if (ConstantsParsing.nytRelExtraction) {
                        sentenceString.add(String.join(" ", as.unmergedTokens()));
                    }
                    else {
                        sentenceString.add(String.join(" ", ps.tokens()));
                    }
                    syntacticTreeString.add(ps.tree());
                    semanticParse = formatSemanticParseOutput(relations);
                    relationOutput = getBinariesAndUnaries(relations, modifiedBy, docTimeStamp, tokenMap, as.unmergedTokens());
                    relationOutput.put("semantic-parse", semanticParse);
                    relationOutput.put("syntactic-parse", syntacticTreeString);
                    relationOutput.put("sentence", sentenceString);
                    GLUERuleInParse = (ps.tree().startsWith("(<T GLUE ")) ? Boolean.TRUE : Boolean.FALSE;

                    if (ConstantsParsing.excludeUnariesFromGLUEParses && GLUERuleInParse) {
                        relationOutput.put("unary", new HashSet<String>());
                    }

                    docOutput.add(relationOutput);

                    CCGTrees.add(ps.tree());
                }
            }
        }

        // OUTPUT - run for all modes of CCG parsing

        // Return results
        String ccgParserOutput = String.join("\n", CCGTrees);
        String mergedSentences = String.join("\n",sentsForParsing);
        Pair<String,String> coreNLPAndMergedSentOutput = new Pair<>(mergedSentences, coreNLPOutput);
        Pair<Pair<String,String>,String> coreNLPAndCCGOutput = new Pair<>(coreNLPAndMergedSentOutput, ccgParserOutput);
        Pair<List<String[]>, Pair<Pair<String,String>,String>> ret = new Pair<>(formatOutputString(docOutput), coreNLPAndCCGOutput);
        return ret;
    }

    // Handle adjectival copulas that link a second argument to the main verb
    // E.g. "Arsenal is capable of winning the match"
    // [is.capable.of.2(1:e, 4:e), winning.2(4:e, 6:match), is.capable.1(1:e, 0:arsenal), is.1(1:e, 0:arsenal)]
    // generate missing relation --> winning.1 arsenal
    // to extract the binary:
    // MOD__(win.1,win.2) arsenal match
    static Set<Relation> linkArgumentsAcrossAdjectivalCopula(Set<Relation> rels,
                                                             MutableValueGraph g, List<LexicalGraphNode> negNodes,
                                                             HashMap<LexicalGraphNode, Boolean> modalNodes,
                                                             List<LexicalGraphNode> copulaNodes,
                                                             List<LexicalGraphNode> attitudeNodes,
                                                             List<LexicalGraphNode> conditionalNodes,
                                                             List<LexicalGraphNode> counterfactualNodes) {
        // Check if there is an adjectival copula
        Boolean adjectivalCopulaFound = Boolean.FALSE;
        Set<Relation> adjCopulaRels = new HashSet<>();
        for (Relation r : rels) {
            if (r.getPredIsCopular() && r.getPredContainsAdjective()) {
                adjectivalCopulaFound = Boolean.TRUE;
                adjCopulaRels.add(r);
            }
        }
        if (!adjectivalCopulaFound) {
            // No linking to be applied, return the original set of relations
            return rels;
        }
        // Handle the linking
        Set<Relation> linkedRels = new HashSet<>();
        Set<Relation> visitedRels = new HashSet<>();
        for (Relation adjCRel : adjCopulaRels) {
            if (adjCRel.getArgument().equals("e")) { // Argument is not an entity, but an event e.g. another relation
                // Argument is an event "e"
                Boolean foundLinkedRel = Boolean.FALSE;
                for (Relation r : rels) {
                    // If the argument index of "adjCRel" matches the predicate index of "r"
                    // then link the two relations and output the merged result
                    // E.g. adjCRel is is.capable.of.2(1:e, 4:e) and r is winning.2(4:e, 6:match)
                    if (r.getPredIndex().equals(adjCRel.getArgIndex())) {
                        foundLinkedRel = Boolean.TRUE;
                        for (Relation adjCRel2 : rels) {
                            // E.g. adjCRel2 is is.capable.1(1:e, 0:arsenal) or is.1(1:e, 0:arsenal)
                            if (adjCRel.getPredIndex().equals(adjCRel2.getPredIndex()) && (!adjCRel.getArgument().equals(adjCRel2.getArgument()))) {
                                // E.g. detected that winning.1(4:e, 0:arsenal) is missing
                                // Construct this as a new relation, by combining information from the verbNode (winning)
                                // and the adjCRel2: is.capable.1(1:e, 0:arsenal)
                                LexicalGraphNode v = getNodeByIndex(r.getPredIndex(), g);
                                Pair<Pair<Integer, String>, Pair<String, String>> adjCRelIndexArgSlotLemma = getAdjCArgumentInformation(rels, adjCRel2);
                                if (adjCRelIndexArgSlotLemma != null) {
                                    Relation verbRel = constructVerbRelationForAdjectivalCopula(v, negNodes, modalNodes,
                                            copulaNodes, attitudeNodes, conditionalNodes, counterfactualNodes, adjCRel2,
                                            adjCRelIndexArgSlotLemma, g);
                                    linkedRels.add(adjCRel);
                                    linkedRels.add(verbRel);
                                }
                                visitedRels.add(adjCRel);
                                break;
                            }
                        }
                    }
                }
                // If no linked relations found
                // E.g. "Arsenal are capable of winning": are.capable.of.2(1:e, 4:e), of.1(3:e, 4:e), are.capable.1(1:e, 0:arsenal), are.1(1:e, 0:arsenal)
                // there is no relation for "winning", but are.capable.of.2(1:e, 4:e) links to the verb "winning" at index 4
                // Construct a relation winning.1 arsenal from the verb "winning" and the relation are.capable.of.2(1:e, 4:e),
                // to be returned as a unary
                if (!foundLinkedRel) {
                    LexicalGraphNode v = getNodeByIndex(adjCRel.getArgIndex(), g);
                    Pair<Pair<Integer, String>, Pair<String, String>> adjCRelIndexArgSlotLemma = getAdjCArgumentInformation(rels, adjCRel);
                    if (adjCRelIndexArgSlotLemma != null) {
                        Relation verbRel = constructVerbRelationForAdjectivalCopula(v, negNodes, modalNodes, copulaNodes,
                                attitudeNodes, conditionalNodes, counterfactualNodes, adjCRel, adjCRelIndexArgSlotLemma, g);
                        linkedRels.add(adjCRel);
                        linkedRels.add(verbRel);
                    }
                    visitedRels.add(adjCRel);
                }
            }
        }
        // Add all non-visitedRels to linkedRels so that we return a complete list of relations,
        // including the newly constructed ones
        for (Relation r: rels) {
            if (!visitedRels.contains(r)) {
                linkedRels.add(r);
            }
        }
        return linkedRels;
    }

    // Get information from the adjectival copula relation adjCRel to be used in the construction of a new (previously missing) relation
    // Returns: argument index of adjCRel (e.g. 0 is the index of Arsenal in "Arsenal is capable of winning"),
    //          the argument string (e.g. "Arsenal"),
    //          the argument slot (e.g. 1, as Arsenal is in subject position),
    //          the lemmatised form of the argument (e.g. "arsenal")
    static Pair<Pair<Integer, String>, Pair<String, String>> getAdjCArgumentInformation(Set<Relation> rels, Relation adjCRel) {
        Pair<Pair<Integer, String>, Pair<String, String>> result = null;
        for (Relation r : rels) {
            if (r.getPredIndex().equals(adjCRel.getPredIndex()) && (!r.getArgument().equals("e"))) {
                Pair<Integer, String> argIdxAndArg = new Pair(r.getArgIndex(), r.getArgument());
                Pair<String, String> argSlotAndLemma = new Pair(r.getArgSlot(), r.getArgumentLemmatised());
                result = new Pair(argIdxAndArg, argSlotAndLemma);
            }
        }
        return result;
    }

    // Construct a relation from the verb nodes using information from the adjectival copula relation
    // E.g. for the example sentence "Arsenal is capable of winning the match"
    //    adjCRel: is.capable.1(1:e, 0:arsenal)
    //    verbNode: winning
    //    generate missing relation --> winning.1 arsenal
    //    so that we can later extract the binary:
    //    MOD__(win.1,win.2) arsenal match
    static Relation constructVerbRelationForAdjectivalCopula(LexicalGraphNode verbNode, List<LexicalGraphNode> negNodes,
                                          HashMap<LexicalGraphNode, Boolean> modalNodes, List<LexicalGraphNode> copulaNodes,
                                          List<LexicalGraphNode> attitudeNodes, List<LexicalGraphNode> conditionalNodes,
                                          List<LexicalGraphNode> counterfactualNodes, Relation adjCRel,
                                          Pair<Pair<Integer, String>, Pair<String, String>> adjCRelIndexArgSlotLemma, MutableValueGraph g) {
        Boolean negation = checkIfNegated(g, verbNode, negNodes);
        Boolean passive = (verbNode.lexItem.getCcgTag().contains("[pss]")) ? Boolean.TRUE : Boolean.FALSE;
        String predCCGFeature = getCCGTagFeature(verbNode.lexItem.getCcgTag());
        Pair<HashMap<LexicalGraphNode, Boolean>, Set<String>> connectedModals = getConnectedModals(g, verbNode, modalNodes, copulaNodes);
        HashMap<LexicalGraphNode, Boolean> connectedModalNodes = connectedModals.first;
        Boolean hasModal = (connectedModalNodes.size() > 0) ? Boolean.TRUE : Boolean.FALSE;
        HashMap<Integer, Pair<String, Integer>> connectedModalsIdxs = constructConnectedModalsIdxs(connectedModalNodes);
        Set<String> modals = connectedModals.second;
        Pair<Set<String>, Set<String>> connectedAttitudeVerbs = getConnectedAttitudeVerbs(g, verbNode, attitudeNodes);
        Boolean isAttitudeSay = (connectedAttitudeVerbs.first.isEmpty()) ? Boolean.FALSE : Boolean.TRUE;
        Boolean isAttitudeThink = (connectedAttitudeVerbs.second.isEmpty()) ? Boolean.FALSE : Boolean.TRUE;
        Set<String> attitudeVerbs = new HashSet<>();
        attitudeVerbs.addAll(connectedAttitudeVerbs.first);
        attitudeVerbs.addAll(connectedAttitudeVerbs.second);
        Set<String> conditionals = getConnectedConditionals(g, verbNode, conditionalNodes);
        Set<String> counterfactuals = getConnectedCounterfactuals(g, verbNode, counterfactualNodes);

        LexicalGraphNode argNode = getNodeByIndex(adjCRelIndexArgSlotLemma.first.first, g);
        SemanticCategoryType semCatType = argNode.lexItem.getSemCatType();
        Relation verbRel = new Relation(negation, passive, predCCGFeature, verbNode.lexItem.getToken(),
                verbNode.lexItem.getLemma(), "", "", hasModal, connectedModalsIdxs, isAttitudeSay,
                isAttitudeThink, adjCRel.getIsConditional(),
                modals, attitudeVerbs, conditionals, adjCRel.getIsCounterfactual(), counterfactuals,
                Boolean.FALSE, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE,
                verbNode.lexItem.getTense(), "simple", verbNode.nodeIndex, null, adjCRelIndexArgSlotLemma.first.second,
                adjCRelIndexArgSlotLemma.second.second, adjCRel.getArgumentType(), adjCRel.getArgumentPOSTag(), adjCRel.getArgumentCCGTag(),
                adjCRelIndexArgSlotLemma.first.first, adjCRelIndexArgSlotLemma.second.first, semCatType, adjCRel.getDate(), adjCRel.getResolvedCoref());
        return verbRel;
    }


    static void updateModalStats(Relation r, String predicateString, String modalTag, String negTag) {
        Integer[] modNegStats = new Integer[13]; // [total, c_mod, attSay, attThink, conditional, counterfactual, c_neg, c_lneg, c_1, c_2, c_3, c_4, c_5]
        Arrays.fill(modNegStats, 0); // Fill array with zeros
        modNegStats[0] = 1; // Computes a total for the number of times each predicate has been observed
        if (!modalTag.equals("")) {
            modNegStats[1] = 1;
        }
        else if (r.getIsAttitudeSay()) {
            modNegStats[2] = 1;
        }
        else if (r.getIsAttitudeThink()) {
            modNegStats[3] = 1;
        }
        else if (r.getIsConditional()) {
            modNegStats[4] = 1;
        }
        else if (r.getIsCounterfactual()) {
            modNegStats[5] = 1;
        }
        else if (negTag.equals("NEG__")){
            modNegStats[6] = 1;
        }
        else if (negTag.equals("LNEG__")){
            modNegStats[7] = 1;
        }
        Integer numModalsIdx = r.getConnectedModalsIdxs().size() + 7;
        if (r.getConnectedModalsIdxs().size() > 0 && numModalsIdx <= 12) {
            if (!modalTag.equals("")) {
                modNegStats[numModalsIdx] = 1;
            }
        }
        else if (r.getConnectedModalsIdxs().size() != 0) {
            System.out.println("Warning: More than 5 modals for predicate: " + predicateString);
        }
        addPredModalStats(predicateString, modNegStats);

        // Get all modal / conditional / attitude verbs
        Set<String> modCondAttVerbs = Util.mergeMultipleSets(Arrays.asList(r.getModalStrings(), r.getAttVerbStrings(),
                r.getConditionalStrings(), r.getCounterfactualStrings()));
        for (String s : modCondAttVerbs) {
            addModalCountStats(s);
        }
    }

    // Add a modal to allModalCountStats
    static void addModalCountStats(String exp) {
        if (!LinesHandler.allModalCountStats.containsKey(exp)) {
            LinesHandler.allModalCountStats.put(exp, 0);
        }
        LinesHandler.allModalCountStats.put(exp, LinesHandler.allModalCountStats.get(exp) + 1);
    }

    // Add a predicate to allPredModalNegStats
    static void addPredModalStats(String pred, Integer[] updateStats) {
        if (!LinesHandler.allPredModalNegStats.containsKey(pred)) {
            Integer [] stats = new Integer[13]; // [total, c_mod, attSay, attThink, conditional, counterfactual, c_neg, c_lneg, c_1, c_2, c_3, c_4, c_5]
            Arrays.fill(stats, 0); // Fill array with zeros
            LinesHandler.allPredModalNegStats.put(pred, stats);
        }
        Integer[] updated = Util.sumValuesInTwoArrays(LinesHandler.allPredModalNegStats.get(pred), updateStats);
        LinesHandler.allPredModalNegStats.put(pred, updated);
    }

    // Add a time expression to allTimeExprs
    static void addTimeExprs(String exp) {
        if (!LinesHandler.allTimeExprs.containsKey(exp)) {
            LinesHandler.allTimeExprs.put(exp, 0);
        }
        LinesHandler.allTimeExprs.put(exp, LinesHandler.allTimeExprs.get(exp) + 1);
    }

    // Add a general entity to allGens
    static void addGens(String arg) {
        if (!LinesHandler.allGens.containsKey(arg)) {
            LinesHandler.allGens.put(arg, 0);
        }
        LinesHandler.allGens.put(arg, LinesHandler.allGens.get(arg) + 1);
    }

    // Add a general entity to allEntities
    static void addEnts(String arg) {
        if (!LinesHandler.allEntities.containsKey(arg)) {
            LinesHandler.allEntities.put(arg, 0);
        }
        LinesHandler.allEntities.put(arg, LinesHandler.allEntities.get(arg) + 1);
    }

    // Add general and named entities to the list
    static void addGensEnts(Integer accepted, String arg1, String arg2) {
        if (accepted >= 0) {
            // accepted:
            // 0 = EG
            // 1 = GE
            // 2 = EE
            // 3 = GG
            if (accepted.equals(1) || accepted.equals(3)) { // Add arg 1 to allGens
                addGens(arg1);
            }
            if (accepted.equals(0) || accepted.equals(3)) { // Add arg2 to allGens
                addGens(arg2);
            }
            if (accepted.equals(0) || accepted.equals(2)) { // Add arg1 to allEnts
                addEnts(arg1);
            }
            if (accepted.equals(1) || accepted.equals(2)) { // Add arg2 to allEnts
                addEnts(arg2);
            }
        }
	}

	// Format the output strings for writing to file
    // mainStr: sentence, syntactic parse (optional), semantic parse, binary relations
    // mainStrOnlyNEs: binary relations that hold only between two named entities
    // dsStr: predicate argument string + entity types (in Javad's pipeline it seems to be used only for binary relations)
    // foundInteresting: always true because argument and predicate indices will not be the same
    // unaryRelsStr: unary relations
    public static List<String[]> formatOutputString(List<Map<String, Set<String>>> output) {
        String mainStr;
        String mainStrOnlyNEs;
        String dsStr;
        boolean foundInteresting;
        String unaryRelsStr;
        String unmergedSentence;

        List<String[]> ret = new ArrayList<>();

        // For each sentence:
        for (Map<String, Set<String>> sentenceOutput : output) {
            mainStr = "";
            mainStrOnlyNEs = "";
            dsStr = "";
            foundInteresting = Boolean.TRUE; // Always true because argument and predicate indices will not be the same
            unaryRelsStr = "";
            unmergedSentence = "";

            Set<String> unaryRels = sentenceOutput.get("unary");
            Set<String> binaryRels = sentenceOutput.get("binary");
            Set<String> binaryRelsOnlyNEs = sentenceOutput.get("binary-onlyNEs");
            Set<String>binaryRelsWithoutSentNum = sentenceOutput.get("binary-without-sent-num");

            // Unmerged sentence (no MWE NE merging)
            unmergedSentence += Iterables.getOnlyElement(sentenceOutput.get("sentence")) + "\n";

            // Main String
            if (ConstantsParsing.writeDebugString) {
                mainStr += Iterables.getOnlyElement(sentenceOutput.get("sentence")) + "\n";
            }
            if (!(binaryRels == null || binaryRels.isEmpty())) {
                mainStr += String.join("\n", binaryRels) + "\n";
            }
            mainStr += "semantic parses:\n";
            mainStr += sentenceOutput.get("semantic-parse") + "\n";
            if (ConstantsParsing.writeDebugString) {
                mainStr += "syntactic parses:\n";
                mainStr += Iterables.getOnlyElement(sentenceOutput.get("syntactic-parse")) + "\n";
            }
            // Named entities only
            if (binaryRelsOnlyNEs != null) {
                //
                mainStrOnlyNEs += String.join("\n", binaryRelsOnlyNEs) + "\n";
            }
            // Unary relations
            if (!(unaryRels == null || unaryRels.isEmpty())) {
                unaryRelsStr += String.join("\n", unaryRels) + "\n";
            }
            // dsStr
            if (binaryRelsWithoutSentNum != null) {
                dsStr += String.join("\n", binaryRelsWithoutSentNum);
            }
            if (ConstantsParsing.filterUntensed) {
                mainStr = filterUntensedRels(mainStr);
            }

            // Add the result strings to the list to be returned
            String[] sentret = new String[] { mainStr, mainStrOnlyNEs, dsStr, foundInteresting + "", unaryRelsStr, unmergedSentence };
            ret.add(sentret);
        }
        return ret;
    }

    // Look for instances of string in a list of strings
    public static boolean stringContainsItemFromList(String inputStr, String[] items) {
		return Arrays.stream(items).parallel().anyMatch(inputStr::contains);
	}

	// Prevents complex tenses from being doubled. Eg returns only
	// [(receiving.2,receiving.will.1) gift obama 4 GE 0]
	// instead of also (receiving.1,receiving.2) obama gift 4 EG 0.
	// Then turns receiving.will.2, receiving.have.2 into receiving.will.have.2
	public static String filterUntensedRels(String mainStr) {

		String[] mainRels = mainStr.split("\n");
		String[] tenseSignals = new String[] { ".will", ".have", ".has", ".had", ".is", ".was" };

		Pattern p = Pattern.compile("(\\(.+\\.)([\\d]+)(,.+\\.)([\\d]+)\\) (\\S+) (\\S+)(.*)([EG])([EG])(.*)");

		for (String rel : mainRels) {
			if (rel.startsWith("semantic parses:")) {
				break;
			}
			if (stringContainsItemFromList(rel, tenseSignals)) {

				for (String str : tenseSignals) {
					rel = rel.replace(str, "");
				}

				Matcher m = p.matcher(rel);
				m.matches();
				String swappedRel = m.group(1) + m.group(4) + m.group(3) + m.group(2) + ") " + m.group(6) + " "
						+ m.group(5) + m.group(7) + m.group(9) + m.group(8) + m.group(10);
				if (Arrays.asList(mainRels).contains(swappedRel)) {
					mainRels = ArrayUtils.removeElement(mainRels, swappedRel);
				}

			}
		}
		mainStr = String.join("\n", mainRels);
		return mainStr;
	}

    // Main
    public static void main(String[] args) {
        List<String> sentences = new ArrayList<>();

        PredicateArgumentExtractor prEx = new PredicateArgumentExtractor("", Boolean.TRUE);

        // BASIC BINARIES
        //sentences.add("Bob likes Sue.");
        //sentences.add("Barack Obama visited Hawaii .");
        //sentences.add("Obama wants Trump to leave .");

        // BASIC UNARIES
        //sentences.add("The man walked .");
        //sentences.add("The men walked");

        // POSSESSIVES
        //sentences.add("Spain 's Barcelona .");
        //sentences.add("The man's dog bit me.");
        //sentences.add("Pluto \'s moon is beautiful .");

        // CONJUNCTION
        //sentences.add("John does n\'t like apples or oranges .");

        // NEGATION
        //sentences.add("Obama doesn't want Trump to leave.");
        //sentences.add("Angela Merkel has not been to London .");
        //sentences.add("Obama does n't like apples or oranges");

        // MODIFIERS
        //sentences.add("He planned to attend the party .");
        //sentences.add("Obama planned to meet Trump .");
        //sentences.add("Spurs managed to win .");

        // PASSIVES
        //sentences.add("Bob was killed .");
        //sentences.add("Bob was killed by John .");
        //sentences.add("Christmas is celebrated in Britain .");

        // AUXILIARIES
        //sentences.add("Angela Merkel has been to London .");
        //sentences.add("Angela Merkel might have been to London .");
        //sentences.add("Barack Obama did not visit Hawaii.");

        // PARTICLES
        //sentences.add("Angela Merkel grew up in Germany .");
        //sentences.add("Angela Merkel stood up for the workers .");

        // COPULARS
        //sentences.add("Tom is the president");
        //sentences.add("Germany is part of the EU");
        //sentences.add("Next monday is the end of Easter");

        // TWO HOP BINARY RELATIONS
        //sentences.add("Libya receives help from the United Nations .");
        //sentences.add("liquor contains amounts of alcohol");

        // MODALS
        sentences.add("Arsenal might beat Spurs");
        sentences.add("Arsenal probably beat Spurs");

        // CONDITIONALS
        //sentences.add("If Arsenal win the match they will play in the finals");
        //sentences.add("If Arsenal lose the match they won't play in the finals");
        //sentences.add("If Arsenal win the match the coach will be happy");

        // MULTI-TOKEN MODALS / CONDITIONALS
        //sentences.add("Obama takes as gospel that Arsenal will beat Manchester"); // MODAL
        //sentences.add("As long as Arsenal train hard they will win"); // CONDITIONAL
        //sentences.add("Arsenal are capable of winning the match"); // MODAL
        //sentences.add("Arsenal pulled off a win against Manchester"); // MODAL

        // PROPOSITIONAL ATTITUDE
        //sentences.add("Bob said Arsenal did not win the match");
        //sentences.add("Bob did not say that Arsenal won the match");
        //sentences.add("Jane claimed Arsenal won the match");
        //sentences.add("Bob claimed Arsenal will win");

        // ADJECTIVAL COPULAS
        //sentences.add("Arsenal are capable of winning");
        //sentences.add("Arsenal is capable of winning the match");
        //sentences.add("Arsenal are capable of winning the match");

        // COUNTERFACTUALS
        //sentences.add("Had Arsenal trained harder they would have won");
        //sentences.add("Arsenal would have won had they trained harder");
        //sentences.add("Arsenal would have won if they had trained harder");


        // Extract the predicate-argument strings (i.e. binary and unary relations)
        List<String[]> docOutput = null;
        try {
            docOutput = prEx.extractPredArgsStrs(sentences, "", new ArrayList<>(), "").first;
        } catch (FileNotFoundException fnf) {
            System.out.println("Process terminating with error: " + fnf.getMessage());
            System.exit(1);
        } catch (IOException io) {
            System.out.println("Process terminating with error: " + io.getMessage());
            System.exit(1);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        // Output to terminal
        for (int i = 0; i < sentences.size(); i++) {
            String [] sentOutput = docOutput.get(i);
            System.out.println("---Sentence---");
            if (!ConstantsParsing.writeDebugString) {
                System.out.println(sentences.get(i));
            }
            System.out.println(sentOutput[0]);
            if (ConstantsParsing.writeUnaryRels && !sentOutput[4].equals("")) {
                System.out.println("#unary rels:");
                System.out.println(sentOutput[4]);
            }
            System.out.println(sentOutput[1]);
            System.out.println("-----");
        }

    }

}

