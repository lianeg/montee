package extraction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

import constants.ConstantsParsing;
import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.CoreSentence;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.parser.ParseException;
import org.jsoup.Jsoup;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ibm.icu.util.StringTokenizer;

import constants.ConstantsAgg;

import edu.stanford.nlp.pipeline.*;

import ac.biu.nlp.normalization.BiuNormalizer;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.logging.RedwoodConfiguration;

public class Util {

	public enum TypeScheme {
		GKG, FIGER, LDA
	}

	public static StanfordCoreNLP stanPipeline;
	public static StanfordCoreNLP stanPipelineSimple;// up to lemma
	public static StanfordCoreNLP stanPipelineSimple2;// up to ssplit
	public static StanfordCoreNLP stanPipelineSentenceSplitter; // For use in convertReleaseToRawJson
	static BiuNormalizer biuNormalizer;
	static Map<String, String> stan2Figer;
	static String[] goodPart2s = new String[] { "1", "2", "3" };
	public static Set<String> pronouns;

	static HashSet<String> goodPart2sSet = new HashSet<String>();
	static {
		for (String s : goodPart2s) {
			goodPart2sSet.add(s);
		}
	}

	static HashSet<String> modals;
	public static Set<String> stopPreds;
	static HashSet<String> prepositions;// I need a predefined list when extending the predArg extraction

	static {

		RedwoodConfiguration.current().clear().apply();

		stan2Figer = new HashMap<>();
		String[] stans, figers;
		if (!ConstantsAgg.updatedTyping) {
			stans = new String[] { "location", "organization", "date", "number", "person", "misc", "time", "ordinal",
					"o" };
			figers = new String[] { "location", "organization", "time", "thing", "person", "thing", "time", "thing",
					"thing" };
		} else {
			stans = new String[] { "location", "organization", "date", "number", "person", "misc", "time", "ordinal",
					"o", "duration", "money", "percent" };
			figers = new String[] { "location", "organization", "time", "thing", "person", "thing", "time", "thing",
					"thing", "time", " money", "percent" };
		}

		for (int i = 0; i < stans.length; i++) {
			stan2Figer.put(stans[i], figers[i]);
		}
		Properties props = new Properties();
		props.put("annotators", "tokenize,ssplit,pos,lemma,ner");
		stanPipeline = new StanfordCoreNLP(props);

		Properties props2 = new Properties();
		props2.put("annotators", "tokenize,ssplit,pos,lemma");
		stanPipelineSimple = new StanfordCoreNLP(props2);

		Properties props3 = new Properties();
		props3.put("annotators", "tokenize,ssplit");
		stanPipelineSimple2 = new StanfordCoreNLP(props3);

		Properties propsSplit = new Properties();
		propsSplit.put("annotators", "tokenize,ssplit");
		propsSplit.setProperty("ssplit.newlineIsSentenceBreak", "always");
        propsSplit.setProperty("tokenize.class", "PTBTokenizer");
        propsSplit.setProperty("tokenize.language", "en");
		propsSplit.setProperty("tokenize.options", "untokenizable=allKeep");
		stanPipelineSentenceSplitter = new StanfordCoreNLP(propsSplit);

		modals = new HashSet<String>();
		String[] modalsList = new String[] { "can", "could", "may", "might", "must", "shall", "should", "will", "would",
				"ought" };

		for (String s : modalsList) {
			modals.add(s);
		}

		if (ConstantsAgg.keepWillTense) {
			modals.remove("will");
		}

		String[] pronounsList = new String[] { "i", "you", "he", "she", "it", "we", "they", "me", "him", "her",
				"them" };
		pronouns = new HashSet<>();
		for (String s : pronounsList) {
			pronouns.add(s);
		}

		try {
			Scanner sc = new Scanner(new File("data/prepositions.txt"));
			prepositions = new HashSet<>();
			while (sc.hasNext()) {
				prepositions.add(sc.nextLine().toLowerCase());
			}
			sc.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

		try {
			biuNormalizer = new BiuNormalizer(new File("lib_data/biu_string_rules.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		stopPreds = new HashSet<>();
		Scanner sc = null;
		try {
			sc = new Scanner(new File("data/stops.txt"));
			while (sc.hasNext()) {
				stopPreds.add(sc.nextLine());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}


	public static String preprocess(String text) {
		text = text.replace("\"", "");
		text = removeHtmlTags(text);

		String[] exceptionStrs = new String[] { "|", ">", "<", "&gt", "&lt" };
		for (String es : exceptionStrs) {
			text = text.replace(es, "--");
		}

		text = removeHeader(text);

		return text;
	}

	public static String removeHeader(String text) {
		int maxAcceptableIdx = 40;
		String[] headerIdentifiers = new String[] { ": ", " - ", " -- ", " — " };

		int maxIdx = -1;
		String splitter = "";
		for (String s : headerIdentifiers) {
			int thisIdx = text.indexOf(s);
			if (thisIdx >= 0 && (thisIdx > maxIdx || maxIdx == -1)) {
				maxIdx = thisIdx + s.length();
				splitter = s;
			}
		}
		if (maxIdx >= 0 && maxIdx < maxAcceptableIdx) {
			String candText = text.substring(maxIdx);

			// For cases like part1 - part2 - part3
			if (splitter.equals(" - ")) {
				int idx = candText.indexOf(" - ");
				if (idx > -1 && idx < maxAcceptableIdx) {
					return text;
				} else {
					text = candText;
				}
			} else {
				text = candText;
			}

		}
		return text;
	}

	public static String removeHtmlTags(String text) {
		return Jsoup.parse(text).text();
	}

	public static String getLemma(String text) {

		StringBuilder ret = new StringBuilder();

		// create an empty Annotation just with the given text
		Annotation document = new Annotation(text);

		// run all Annotators on this text
		stanPipelineSimple.annotate(document);

		// Iterate over all of the sentences found
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for (CoreMap sentence : sentences) {
			// Iterate over all tokens in a sentence
			for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
				// Retrieve and add the lemma for each word into the list of
				// lemmas
				ret.append(token.get(LemmaAnnotation.class) + " ");
			}
		}

		return ret.toString().trim();
	}


	public static List<String> getSentences(String s) {
        List<String> ret = new ArrayList<>();
        String sentence = "";

        // Run CoreNLP
        CoreDocument doc = new CoreDocument(s);
        stanPipelineSentenceSplitter.annotate(doc);

        // Extract sentences
        for (CoreSentence cs : doc.sentences()) {
			sentence = StringUtils.normalizeSpace(cs.text());
            ret.add(sentence);
        }

		return ret;
	}

	// token -> pos
	public static String[] getPOSTaggedTokens(String text) {
		// create an empty Annotation just with the given text

		Annotation document = new Annotation(text);
		String tokenized = "";
		String posList = "";

		// run all Annotators on this text
		stanPipelineSimple.annotate(document);

		// Iterate over all of the sentences found
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for (CoreMap sentence : sentences) {
			// Iterate over all tokens in a sentence
			for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
				// Retrieve and add the lemma for each word into the list of
				// lemmas
				String word = token.get(TextAnnotation.class);
				word = simpleNormalize(word);
				if (word.equals("")) {
					continue;
				}
				String pos = token.get(PartOfSpeechAnnotation.class);
				tokenized += word + " ";
				for (int i = 0; i < word.split(" ").length; i++) {
					posList += pos + " ";
				}
			}
		}
		String[] ret = new String[] { tokenized.trim(), posList.trim() };
		return ret;
	}

	public static Map<String, String> getSimpleNERTypeSent(String text) {

		Map<String, String> tokenToType = new LinkedHashMap<>();

		// special case:
		String[] shortMonths = "jan feb mar apr may jun jul aug sep oct nov dec".split(" ");
		HashSet<String> shortMonthsSet = new HashSet<String>();
		for (String s : shortMonths) {
			shortMonthsSet.add(s);
		}

		// Create an empty Annotation just with the given text
		Annotation document = new Annotation(text);

		// Run all Annotators on this text
		stanPipeline.annotate(document);

		// Iterate over all of the sentences found
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for (CoreMap sentence : sentences) {
			// Iterate over all tokens in a sentence
			for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
				// Retrieve and add the lemma for each word into the list of
				// lemmas
				String currentNEType = token.get(NamedEntityTagAnnotation.class).toLowerCase();
				if (stan2Figer.containsKey(currentNEType)) {
					currentNEType = stan2Figer.get(currentNEType);
				} else {
					currentNEType = "thing";
				}
				String thisToken = simpleNormalize(token.originalText());
				if (currentNEType.equals("thing")) {
					if (shortMonthsSet.contains(thisToken) || (thisToken.endsWith(".")
							&& shortMonthsSet.contains(thisToken.substring(0, thisToken.length() - 1)))) {
						currentNEType = "time";
					}
				}
				if (!currentNEType.equals("thing")) {
					tokenToType.put(thisToken, currentNEType);
				}
			}
		}

		return tokenToType;
	}

	public static String getJsonStringParsedOutput(String s, String date, long articleId, long lineId, String mergedEntitySentences,
												   String coreNLPOutput, String ccgParserOutput) {
		String jsonStr;
		JsonObject parsedOut = new JsonObject();

		parsedOut.addProperty("s", s);
		parsedOut.addProperty("date", date);
		parsedOut.addProperty("articleId", articleId);
		parsedOut.addProperty("lineId", lineId);
		parsedOut.addProperty("mergedEntitySentences", mergedEntitySentences);
		parsedOut.addProperty("coreNLPOutput", coreNLPOutput);
		parsedOut.addProperty("ccgParserOutput", ccgParserOutput);

		jsonStr = parsedOut.toString();

		return jsonStr;
	}

	public static void convertReleaseToRawJson(String[] args) throws ParseException, IOException {
		String fileName = "data/release/crawlbatched_en";
		if (args.length >= 1) {
			fileName = args[0];
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"));
		JsonParser parser = new JsonParser();
		int lineId = 0;
		int lineNumber = 0;
		String line;
		while ((line = br.readLine()) != null) {
			JsonObject obj = null;
			if (lineNumber % 10000 == 0) {
				System.err.println(lineNumber);
			}
			lineNumber++;
			try {
				obj = parser.parse(line).getAsJsonObject();
				String text = obj.get("text").getAsString();
				String date = obj.get("date").getAsString();
				long articleId = obj.get("articleId").getAsLong();

				// Get the sentence strings after splitting the document text
				List<String> lines = getSentences(text);

				// If processing at the document level, i.e. for coreference resolution or temporal expression resolution
				// "s" will be the concatenation of each of the sentences in the list "lines", with a dummy lineId of -1
				if (ConstantsParsing.docLevelProcessing || ConstantsParsing.includeCoreference || ConstantsParsing.includeTemporal) {
					// Process at the document level
					JsonObject myObj = new JsonObject();
					String docText = String.join("\n",lines);
					myObj.addProperty("s", docText);
					myObj.addProperty("date", date);
					myObj.addProperty("articleId", articleId);
					myObj.addProperty("lineId", -1);
					System.out.println(myObj);
				}
				else { // Process at the sentence level
					for (String l : lines) {
						JsonObject myObj = new JsonObject();

						myObj.addProperty("s", l);
						myObj.addProperty("date", date);
						myObj.addProperty("articleId", articleId);
						myObj.addProperty("lineId", lineId);
						System.out.println(myObj);
						lineId++;
					}
				}
			} catch (Exception e) {
				continue;
			}
		}

		br.close();
	}

	static HashMap<String, HashMap<String, String>> loadAidaLinked(String aidaPath) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(aidaPath)));
		JsonParser jsonParser = new JsonParser();
		String line;
		HashMap<String, HashMap<String, String>> artIdToEntToWiki = new HashMap<>();
		int lineNumber = 0;
		while ((line = br.readLine()) != null) {
			JsonObject jo = null;
			try {
				jo = jsonParser.parse(line).getAsJsonObject();
			} catch (Exception e) {
				continue;
			}
			String articleId = jo.get("artId").getAsString();
			JsonArray ews = jo.get("ew").getAsJsonArray();
			HashMap<String, String> thisEntToWiki = null;
			if (ews.size() > 0) {
				thisEntToWiki = new HashMap<>();
				artIdToEntToWiki.put(articleId, thisEntToWiki);
			}
			for (int i = 0; i < ews.size(); i++) {
				JsonObject ew = ews.get(i).getAsJsonObject();
				String ent = ew.get("e").getAsString();
				ent = simpleNormalize(ent);
				String wiki = ew.get("w").getAsString();
				if (wiki.equals("--NME--")) {
					continue;
				}
				thisEntToWiki.put(ent, wiki);
			}
			if (lineNumber++ % 1000 == 0) {
				System.err.println(lineNumber);
			}
		}
		System.err.println("linked NEs loaded!");
		br.close();
		return artIdToEntToWiki;
	}

	static void convertPredArgsToJsonUnsorted(String[] args) throws IOException {
		if (args == null || args.length == 0) {
			args = new String[] { "predArgs9_gen.txt", "true", "true", "-1", "aida/news_linked.json" };
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(args[0]), "UTF-8"));
		boolean shouldLink = Boolean.parseBoolean(args[1]);
		boolean useContext = Boolean.parseBoolean(args[2]);// aidalight
		HashMap<String, HashMap<String, String>> artIdToEntToWiki = null;
		Set<String> lineArticleIds = new HashSet<>();

		String AIDAPath = args[4];

		if (useContext) {
			artIdToEntToWiki = loadAidaLinked(AIDAPath);
		}
		System.err.println("useNamedEntities: " + shouldLink);

		String line;
		int lineNumbers = 0;
		String curLine = null;
		ArrayList<String> curPrArgs = new ArrayList<String>();
		ArrayList<String> curPrArgs_unary = new ArrayList<String>();
		List<String> curSemParses = new ArrayList<>();

		while ((line = br.readLine()) != null) {
			if (line.equals("") || line.contains("e.s.nlp.pipeline")) {
				continue;
			}
			if (line.startsWith("#line:")) {
				curLine = line.substring(7);
				if (curLine != null) {
					try {
						// Add the current line
						// First, we should read other details!
						line = br.readLine();
						String lineId = line.substring(9);
						line = br.readLine();
						String articleId = line.substring(12);
						line = br.readLine();
						String date = line.substring(7);

						JsonObject jObj = new JsonObject();
						jObj.addProperty("s", curLine);
						jObj.addProperty("date", date);
						jObj.addProperty("articleId", articleId);
						jObj.addProperty("lineId", lineId);
						String lineArticleId = lineId+"_"+articleId;
						if (lineArticleIds.contains(lineArticleId)) {
							continue;
						}
						lineArticleIds.add(lineArticleId);
						// Now, let's read all the pred_arg lines
						String prArgLine = null;
						curPrArgs = new ArrayList<String>();
						curPrArgs_unary = new ArrayList<String>();
						curSemParses = new ArrayList<>();

						while ((prArgLine = br.readLine()) != null && !prArgLine.equals("")
								&& !prArgLine.equals("#unary rels:") && !prArgLine.equals("semantic parses:")) {
							String pred = null;
							String arg1 = null;
							String arg2 = null;
							String arg1Span = null;
							String arg2Span = null;
							int eventIdx, sentIdx;
							String GorNE = null;

							try {
								StringTokenizer st = new StringTokenizer(prArgLine);
								pred = st.nextToken();
								arg1 = st.nextToken();
								arg2 = st.nextToken();
								if (ConstantsParsing.nytRelExtraction) {
									arg1Span = st.nextToken();
									arg2Span = st.nextToken();
								}
								eventIdx = Integer.parseInt(st.nextToken());
								GorNE = st.nextToken();
								sentIdx = Integer.parseInt(st.nextToken());
								arg1 = simpleNormalize(arg1);
								arg2 = simpleNormalize(arg2);

								boolean[] isGens = new boolean[2];
								isGens[0] = GorNE.charAt(0) == 'G';
								isGens[1] = GorNE.charAt(1) == 'G';

								if (shouldLink) {
									if (!isGens[0]) {
										if (!useContext) {
										} else {
											if (artIdToEntToWiki.containsKey(articleId)) {
												HashMap<String, String> e2w = artIdToEntToWiki.get(articleId);
												arg1 = e2w.containsKey(arg1) ? e2w.get(arg1) : arg1;
											}
										}
									}
									if (!isGens[1]) {
										if (!useContext) {
										} else {
											if (artIdToEntToWiki.containsKey(articleId)) {
												HashMap<String, String> e2w = artIdToEntToWiki.get(articleId);
												arg2 = e2w.containsKey(arg2) ? e2w.get(arg2) : arg2;
											}
										}
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
								System.err.println("exception for: " + line);
								continue;
							}

							String prArg;
							if (ConstantsParsing.nytRelExtraction) {
								prArg = "(" + pred + "::" + arg1 + "::" + arg2 + "::" + arg1Span + "::"
										+ arg2Span + "::" + GorNE + "::" + sentIdx + "::" + eventIdx + ")";
							}
							else {
								prArg = "(" + pred + "::" + arg1 + "::" + arg2 + "::" + GorNE + "::" + sentIdx + "::"
										+ eventIdx + ")";
							}
							curPrArgs.add(prArg);
						}

						JsonArray rels = new JsonArray();
						for (int i = 0; i < curPrArgs.size(); i++) {
							JsonObject rel = new JsonObject();
							rel.addProperty("r", curPrArgs.get(i));
							rels.add(rel);
						}
						jObj.add("rels", rels);

						if (prArgLine.equals("semantic parses:")) {
							while ((prArgLine = br.readLine()) != null && !prArgLine.equals("") && !prArgLine.equals("#unary rels:")) {

								if (!prArgLine.startsWith("[") || !prArgLine.endsWith("]")) {
									throw new RuntimeException("bad semantic parse");
								} else {
									curSemParses.add(prArgLine);
								}

							}
						}

						JsonArray semParses = new JsonArray();
						for (int i = 0; i < curSemParses.size(); i++) {
							JsonObject semParse = new JsonObject();
							semParse.addProperty("parse", curSemParses.get(i));
							semParses.add(semParse);
						}

						jObj.add("semParses", semParses);

						if (prArgLine.equals("#unary rels:")) {
							while ((prArgLine = br.readLine()) != null && !prArgLine.equals("")
									&& !prArgLine.equals("semantic parses:")) {
								String pred = null;
								String arg = null;
								int eventIdx, sentIdx;
								String GorNE = null;

								try {
									StringTokenizer st = new StringTokenizer(prArgLine);
									pred = st.nextToken();
									arg = st.nextToken();
									eventIdx = Integer.parseInt(st.nextToken());
									GorNE = st.nextToken();
									sentIdx = Integer.parseInt(st.nextToken());
									arg = simpleNormalize(arg);

									boolean isGen = GorNE.charAt(0) == 'G';

									if (shouldLink) {
										if (!isGen) {
											if (!useContext) {
											} else {
												if (artIdToEntToWiki.containsKey(articleId)) {
													HashMap<String, String> e2w = artIdToEntToWiki.get(articleId);
													arg = e2w.containsKey(arg) ? e2w.get(arg) : arg;
												}
											}
										}
									}
								} catch (Exception e) {
									e.printStackTrace();
									System.err.println("exception for: " + line);
									continue;
								}

								String prArg = "(" + pred + "::" + arg + "::" + GorNE + "::" + sentIdx + "::" + eventIdx
										+ ")";
								curPrArgs_unary.add(prArg);
							}
						}

						JsonArray rels_unary = new JsonArray();
						for (int i = 0; i < curPrArgs_unary.size(); i++) {
							JsonObject rel = new JsonObject();
							rel.addProperty("r", curPrArgs_unary.get(i));
							rels_unary.add(rel);
						}
						jObj.add("rels_unary", rels_unary);

						System.out.println(jObj);

					} catch (Exception e) {
						e.printStackTrace();
						continue;
					}
				}

				lineNumbers++;

				if (lineNumbers % 10000 == 0) {
					System.err.println(lineNumbers);
				}
			}
		}
		br.close();
	}

	public static String simpleNormalize(String s) {
		s = s.replace("_", " ");
		s = s.replace("-", " ");

		s = s.toLowerCase().trim();

		return s;
	}

	public static void renewStanfordParser() {
		Properties props = new Properties();
		props.put("annotators", "tokenize,ssplit,pos,lemma,ner");
		StanfordCoreNLP stanfordPipelineTmp = new StanfordCoreNLP(props);
		stanPipeline = stanfordPipelineTmp;
	}

	static Set<String> mergeMultipleSets(List<Set<String>> sets) {
		Set<String> result = new HashSet<>();
		for (Set<String> s : sets) {
			if (s != null) {
				result.addAll(s);
			}
		}
		return result;
	}

	static Integer[] sumValuesInTwoArrays(Integer[] a, Integer[] b) {
		Integer [] c = new Integer[a.length];
		Arrays.fill(c, 0); // Fill array with zeros
		for (int i = 0; i < a.length; i++) {
			c[i] = a[i] + b[i];
		}
		return c;
	}

	public static void main(String[] args) throws ParseException, IOException {
		convertReleaseToRawJson(args);
		//convertPredArgsToJsonUnsorted(args);
	}

}
