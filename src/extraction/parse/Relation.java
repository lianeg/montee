package extraction.parse;

import edu.stanford.nlp.util.Pair;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Set;

public class Relation {
    // Instance variables
    Boolean negation;
    Boolean passive;
    String predCCGFeature;
    String predicate;
    String predicateLemmatised;
    String auxiliary;
    String auxiliaryLemmatised;
    Boolean hasModal;
    HashMap<Integer, Pair<String, Integer>> connectedModalsIdxs;
    Boolean isReportedSay;
    Boolean isReportedThink;
    Boolean isConditional;
    Set<String> modalStrings;
    Set<String> repVerbStrings;
    Set<String> conditionalStrings;
    Boolean isCounterfactual;
    Set<String> counterfactualStrings;
    Boolean predIsCopular;
    Boolean predIsDoubleHopRelationOnEntityOrType;
    Boolean predIsVerb;
    Boolean predIsPreposition;
    Boolean predContainsAdjective;
    String predTense;
    String predAspect;
    Integer predIndex;
    Integer copularBeIndex;
    String argument;
    String argumentLemmatised;
    String argumentType;
    String argumentPOSTag;
    String argumentCCGTag;
    Integer argIndex;
    String argSlot;
    SemanticCategoryType argSemCatType;
    Pair<Calendar,Calendar> date;
    Boolean resolvedCoref;


    // Constructor Declaration of Class
    public Relation(Boolean negation, Boolean passive, String predCCGFeature, String predicate, String predicateLemmatised,
                    String auxiliary, String auxiliaryLemmatised, Boolean hasModal,
                    HashMap<Integer, Pair<String, Integer>> connectedModalsIdxs,
                    Boolean isReportedSay, Boolean isReportedThink, Boolean isConditional,
                    Set<String> modalStrings, Set<String> repVerbStrings, Set<String> conditionalStrings,
                    Boolean isCounterfactual, Set<String> counterfactualStrings, Boolean predIsCopular,
                    Boolean predIsDoubleHopRelationOnEntityOrType, Boolean predIsVerb, Boolean predIsPreposition,
                    Boolean predContainsAdjective, String predTense, String predAspect, Integer predIndex,
                    Integer copularBeIndex, String argument, String argumentLemmatised,
                    String argumentType, String argumentPOSTag, String argumentCCGTag, Integer argIndex, String argSlot,
                    SemanticCategoryType argSemCatType, Pair<Calendar,Calendar> date, Boolean resolvedCoref) {
        this.negation = negation;
        this.passive = passive;
        this.predCCGFeature = predCCGFeature;
        this.predicate = predicate;
        this.predicateLemmatised = predicateLemmatised;
        this.auxiliary = auxiliary;
        this.auxiliaryLemmatised = auxiliaryLemmatised;
        this.hasModal = hasModal;
        this.connectedModalsIdxs = connectedModalsIdxs;
        this.isReportedSay = isReportedSay;
        this.isReportedThink = isReportedThink;
        this.isConditional = isConditional;
        this.modalStrings = modalStrings;
        this.repVerbStrings = repVerbStrings;
        this.conditionalStrings = conditionalStrings;
        this.isCounterfactual = isCounterfactual;
        this.counterfactualStrings = counterfactualStrings;
        this.predIsCopular = predIsCopular;
        this.predIsDoubleHopRelationOnEntityOrType = predIsDoubleHopRelationOnEntityOrType;
        this.predIsVerb = predIsVerb;
        this.predIsPreposition = predIsPreposition;
        this.predContainsAdjective = predContainsAdjective;
        this.predTense = predTense;
        this.predAspect = predAspect;
        this.predIndex = predIndex;
        this.copularBeIndex = copularBeIndex;
        this.argument = argument;
        this.argumentLemmatised = argumentLemmatised;
        this.argumentType = argumentType;
        this.argumentPOSTag = argumentPOSTag;
        this.argumentCCGTag = argumentCCGTag;
        this.argIndex = argIndex;
        this.argSlot = argSlot;
        this.argSemCatType = argSemCatType;
        this.date = date;
        this.resolvedCoref = resolvedCoref;
    }

    public Boolean getNegation() {
        return negation;
    }

    public Boolean getPassive() {
        return passive;
    }

    public String getpredCCGFeature() {
        return predCCGFeature;
    }

    public String getPredicate() {
        return predicate;
    }

    public void setPredicate (String value) {
        predicate = value;
    }

    public String getPredicateLemmatised() {
        return predicateLemmatised;
    }

    public void setPredicateLemmatised(String value) { predicateLemmatised = value; }

    public String getAuxiliary() { return auxiliary; }

    public void setAuxiliary(String value) { auxiliary = value; }

    public String getAuxiliaryLemmatised() { return auxiliaryLemmatised; }

    public void setAuxiliaryLemmatised(String value) { auxiliaryLemmatised = value; }

    public Boolean getHasModal() { return hasModal; }

    public HashMap<Integer, Pair<String, Integer>> getConnectedModalsIdxs() { return connectedModalsIdxs; }

    public Boolean getIsAttitudeSay() { return isReportedSay; }

    public Boolean getIsAttitudeThink() { return isReportedThink; }

    public Boolean getIsConditional() { return isConditional; }

    public Set<String> getModalStrings() { return  modalStrings; }

    public Set<String> getAttVerbStrings() { return repVerbStrings; }

    public Set<String> getConditionalStrings() { return conditionalStrings; }

    public Boolean getIsCounterfactual() { return isCounterfactual; }

    public Set<String> getCounterfactualStrings() { return counterfactualStrings; }

    public Boolean getPredIsCopular() { return predIsCopular; }

    public Boolean getPredIsDoubleHopRelationOnEntityOrType() { return predIsDoubleHopRelationOnEntityOrType; }

    public Boolean getPredIsVerb() { return predIsVerb; }

    public Boolean getPredIsPreposition() {return predIsPreposition; }

    public Boolean getPredContainsAdjective() {return predContainsAdjective; }

    public String getPredTense() { return predTense; }

    public String getPredAspect() { return predAspect; }

    public void setPredAspect(String value) { predAspect = value; }

    public Integer getPredIndex() { return predIndex; }

    public Integer getCopularBeIndex() { return copularBeIndex; }

    public String getArgument() { return argument; }

    public String getArgumentLemmatised() { return argumentLemmatised; }

    public String getArgumentType() { return argumentType; }

    public String getArgumentPOSTag() { return argumentPOSTag; }

    public String getArgumentCCGTag() { return argumentCCGTag; }

    public Integer getArgIndex() { return argIndex; }

    public String getArgSlot() { return argSlot; }

    public void setArgSlot ( String value) { argSlot = value; }

    public SemanticCategoryType getArgSemCatType() { return argSemCatType; }

    public Pair<Calendar,Calendar> getDate() { return date; }

    public Boolean getResolvedCoref() { return resolvedCoref; }

}