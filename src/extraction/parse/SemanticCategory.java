package extraction.parse;

import com.google.common.collect.Sets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SemanticCategory {
    // Instance variables PoS tags
    public static Set<String> eventPosTags = Sets.newHashSet("VB", "VBD", "VBG", "VBN", "VBP",
            "VBZ", "POS", "IN", "TO");
    public static Set<String> typePosTags = Sets.newHashSet("NN", "NNS");
    public static Set<String> entityPosTags = Sets.newHashSet("NNP", "NNPS", "PRP", "PRP$");
    public static Set<String> eventModPosTags = Sets.newHashSet("RB", "RBR", "RBS");
    public static Set<String> typeModPosTags = Sets.newHashSet("JJ", "JJR", "JJS");
    public static Set<String> countPosTags = Sets.newHashSet("CD");
    public static Set<String> closedPosTags = Sets.newHashSet("WDT", "WP", "WP$", "WRB", "SCONJ", "WPRON");
    public static Set<String> questionPosTags = Sets.newHashSet("WP", "WP$", "WDT", "WRB");

    //Instance variables lemmas
    public static Set<String> negationLemmas = Sets.newHashSet("not", "n't", "never");
    public static Set<String> complementLemmas = Sets.newHashSet("no");
    public static Set<String> uniqueLemmas = Sets.newHashSet("the");

    public static Pattern ccgFeaturePattern = Pattern.compile("^\\(*S\\[.*");

    // List of verb phrase features in CCG. Taken from Julia Hockenmaier's PhD thesis:
    // http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.135.8543&rep=rep1&type=pdf
    // Also includes sentential level features "dcl" (declarative) and "emb" (embedded)
    // Also includes "pss" which seems to be the modern equivalent of "pass"
    public static List<String> ccgVerbFeatures = new ArrayList<String>(Arrays.asList("to", "pss", "pass", "pt", "ng",
        "dcl", "emb", "b"));


    public static SemanticCategoryType getSemanticCategoryType(String lemma, String posTag, String ccgTag) {

        // Get the CCG tag feature, e.g. what is contained in the [] tags (sentential or verb phrase)
        String ccgTagFeature = null;
        Matcher matcher = ccgFeaturePattern.matcher(ccgTag);
        // If there is a sentential / verb phrase feature
        if (matcher.find()) {
            ccgTagFeature = matcher.group(0).split("\\[")[1].split("]")[0];
        }

        // Unique
        if(uniqueLemmas.contains(lemma)) {
            return SemanticCategoryType.UNIQUE;
        }

        // Negation
        else if(negationLemmas.contains(lemma)) {
            return SemanticCategoryType.NEGATION;
        }

        // Complements
        else if(complementLemmas.contains(lemma)) {
            return SemanticCategoryType.COMPLEMENT;
        }

        // Counts
        else if(countPosTags.contains(posTag)) {
            return SemanticCategoryType.COUNT;
        }

        // Entities
        else if(entityPosTags.contains(posTag)) {
            return SemanticCategoryType.ENTITY;
        }

        // Events
        else if((eventPosTags.contains(posTag) && (!ccgTag.matches("N")))
                || (ccgTag.matches("^\\(*S.*") && ccgVerbFeatures.contains(ccgTagFeature)
                    && (!ccgTag.matches("\\(*\\(S\\\\NP\\)\\\\\\(S\\\\NP\\).*"))
                    && (!ccgTag.matches("\\(*\\(S\\\\NP\\)\\/\\(S\\\\NP\\).*")))) {
            return SemanticCategoryType.EVENT;
        }

        // Event modifiers
        else if(eventModPosTags.contains(posTag)){
            return SemanticCategoryType.EVENTMOD;
        }

        // Type modifiers
        else if(!ccgTag.equals("N") && (typeModPosTags.contains(posTag) || (ccgTagFeature!=null && ccgTagFeature.equals("adj")))){
            return SemanticCategoryType.TYPEMOD;
        }

        // Types
        else if(typePosTags.contains(posTag) || ccgTag.equals("N")) {
            return SemanticCategoryType.TYPE;
        }

        // Questions
        else if(questionPosTags.contains(posTag) && ccgTag.length() >=5 && ccgTag.substring(0,5).equals("S[wq]")) {
            return SemanticCategoryType.QUESTION;
        }

        else if(closedPosTags.contains(posTag)) {
            return SemanticCategoryType.CLOSED;
        }

        // If no other conditions are met, return UNKNOWN
        else {
            return SemanticCategoryType.UNKNOWN;
        }
    }
}
