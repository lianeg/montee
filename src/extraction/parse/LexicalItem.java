package extraction.parse;

import edu.stanford.nlp.util.Pair;

import java.util.Calendar;

public class LexicalItem {
    // Instance variables
    String token;
    String lemma;
    String posTag;
    String tense;
    String neTag;
    String antecedent;
    SemanticCategoryType semCatType;
    Boolean verb;
    String auxVerb;
    Integer modalStrength;
    String modalPOS;
    String reportedType;
    Boolean conditional;
    Boolean counterfactual;
    String modalString;
    String repVerbString;
    String conditionalString;
    Boolean preposition;
    String ccgTag;
    Pair<Calendar,Calendar> timeExpression;
    Integer index;

    // Constructor Declaration of Class
    public LexicalItem(String token, String lemma, String posTag, String tense, String neTag, String antecedent,
                       SemanticCategoryType semCatType, Boolean verb, String auxVerb, Integer modalStrength,
                       String modalPOS, String reportedType, Boolean conditional, Boolean counterfactual, String modalString,
                       String repVerbString, String conditionalString,
                       Boolean preposition, String ccgTag, Pair<Calendar,Calendar> timeExpression, Integer index) {
        this.token = token;
        this.lemma = lemma;
        this.posTag = posTag;
        this.tense = tense;
        this.neTag = neTag;
        this.antecedent = antecedent;
        this.semCatType = semCatType;
        this.verb = verb;
        this.auxVerb = auxVerb;
        this.modalStrength = modalStrength;
        this.modalPOS = modalPOS;
        this.reportedType = reportedType;
        this.conditional = conditional;
        this.counterfactual = counterfactual;
        this.modalString = modalString;
        this.repVerbString = repVerbString;
        this.conditionalString = conditionalString;
        this.preposition = preposition;
        this.ccgTag = ccgTag;
        this.timeExpression = timeExpression;
        this.index = index;
    }

    public String getToken() {
        return token;
    }

    public String getLemma() {
        return lemma;
    }

    public String getPosTag() {
        return posTag;
    }

    public String getTense() {
        return tense;
    }

    public String getNeTag() {
        return neTag;
    }

    public String getAntecedent() {
        return antecedent;
    }

    public SemanticCategoryType getSemCatType() {
        return semCatType;
    }

    public Boolean getVerb() { return verb; }

    public String getAuxVerb() { return auxVerb; }

    public Integer getModalStrength() { return modalStrength; }

    public String getModalPOS() { return modalPOS; }

    public String getAttitudeType() { return reportedType; }

    public Boolean getConditional() { return conditional; }

    public void setConditional(Boolean value) { conditional = value; }

    public Boolean getCounterfactual() { return counterfactual; }

    public void setCounterfactual(Boolean value) { counterfactual = value; }

    public String getModalString() { return  modalString; }

    public String getAttVerbString() { return repVerbString; }

    public String getConditionalString() { return conditionalString; }

    public Boolean getPreposition() {
        return preposition;
    }

    public String getCcgTag() {
        return ccgTag;
    }

    public Pair<Calendar,Calendar> getTimeExpression() {
        return timeExpression;
    }

    public void setTimeExpression ( Pair<Calendar,Calendar> value) {
        timeExpression = value;
    }

    public Integer getIndex() {
        return index;
    }

}
