package extraction.parse;

import edu.stanford.nlp.pipeline.CoreEntityMention;
import edu.stanford.nlp.util.Pair;

import java.util.Calendar;
import java.util.List;

public class CorenlpSentence {

    // Instance variables
    String text;
    List<String> tokens;
    List<String> lemmas;
    List<String> posTags;
    List<String> verbTenses;
    List<String> nerTags;
    List<Integer> corefCluserIDs;
    List<String> antecedents;
    List<CoreEntityMention> entityMentions;
    List<Pair<Calendar,Calendar>> timeExpressions;
    List<String> unmergedTokens;


    // Constructor Declaration of Class
    public CorenlpSentence(String text, List<String> tokens, List<String> lemmas, List<String> posTags, List<String> verbTenses,
                           List<String> nerTags, List<Integer> corefCluserIDs, List<String> antecedents,
                           List<CoreEntityMention> entityMentions, List<Pair<Calendar,Calendar>> timeExpressions,
                           List<String> unmergedTokens) {
        this.text = text;
        this.tokens = tokens;
        this.lemmas = lemmas;
        this.posTags = posTags;
        this.verbTenses = verbTenses;
        this.nerTags = nerTags;
        this.corefCluserIDs = corefCluserIDs;
        this.antecedents = antecedents;
        this.entityMentions = entityMentions;
        this.timeExpressions = timeExpressions;
        this.unmergedTokens = unmergedTokens;
    }


    public String text() {
        return text;
    }

    public List<String> tokens() {
        return tokens;
    }

    public List<String> lemmas() {
        return lemmas;
    }

    public List<String> posTags() {
        return posTags;
    }

    public List<String> verbTenses() { return verbTenses; }

    public List<String> nerTags() {
        return nerTags;
    }

    public List<Integer> corefCluserIDs() { return corefCluserIDs; }

    public List<String> antecedents() {
        return antecedents;
    }

    public List<CoreEntityMention> entityMentions() {
        return entityMentions;
    }

    public List<Pair<Calendar,Calendar>> timeExpressions() {
        return timeExpressions;
    }

    public List<String> unmergedTokens() { return unmergedTokens; }

}
