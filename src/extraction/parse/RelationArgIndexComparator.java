package extraction.parse;

import java.util.Comparator;

// Compare two relations based on their argument indices
public class RelationArgIndexComparator implements Comparator<Relation> {
    public int compare(Relation r1, Relation r2) {
        int c;
        Integer argSlotR1 = Integer.parseInt(r1.getArgSlot().substring(0,1));
        Integer argSlotR2 = Integer.parseInt(r2.getArgSlot().substring(0,1));
        Integer predLength1 = r1.getPredicate().length();
        Integer predLength2 = r2.getPredicate().length();
        if (r1.getPredicate().contains(".") && r2.getPredicate().contains(".")) {
            if (argSlotR1.equals(argSlotR2)) {
                c = r1.getArgIndex().compareTo(r2.getArgIndex());
            }
            else {
                c = argSlotR1.compareTo(argSlotR2);
            }
        }
        else if (r1.getPredicate().contains(".") || r2.getPredicate().contains(".")) {
            c = predLength1.compareTo(predLength2);
            if (c == 0) {
                c = argSlotR1.compareTo(argSlotR2);
            }
        }
        else {
            c = argSlotR1.compareTo(argSlotR2);
            if (c == 0) {
                c = predLength1.compareTo(predLength2);
            }
        }
        if (c == 0) {
            c = r1.getArgIndex().compareTo(r2.getArgIndex());
        }
        return c;
    }
}