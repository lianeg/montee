package extraction.parse;

public class VerbModifier {
    // Instance variables
    String modifier;
    Boolean modal;
    Boolean negation;
    Boolean modNeg; // The attached modal is negative (e.g. "decline" is negative)
    String tense;

    // Constructor Declaration of Class
    public VerbModifier(String modifier, Boolean modal, Boolean negation, Boolean modNeg, String tense) {
        this.modifier = modifier;
        this.modal = modal;
        this.negation = negation;
        this.modNeg = modNeg;
        this.tense = tense;
    }

    public String getModifier() {
        return modifier;
    }

    public Boolean getModal() { return modal; }

    public Boolean getNegation() {
        return negation;
    }

    public Boolean getModNeg() { return modNeg; }

    public String getTense() {
        return tense;
    }

}