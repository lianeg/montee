package extraction.parse;

import java.util.HashMap;
import java.util.Map;
import java.util.List;

public class LexiconTrie {
    private TrieNode root;

    public LexiconTrie() {
        root = new TrieNode();
    }

    // Inserts a lexicalEntry into the trie.
    public void insert(List<String> lexicalEntry, Integer idx) {
        HashMap<String, TrieNode> children = root.children;

        for (int i = 0; i < lexicalEntry.size(); i++) {
            String token = lexicalEntry.get(i);
            TrieNode tN;
            if(children.containsKey(token)){
                tN = children.get(token);
            }else{
                tN = new TrieNode(token);
                children.put(token, tN);
            }

            children = tN.children;

            //set leaf node
            if(i==lexicalEntry.size()-1)
                tN.isLeaf = true;
                tN.idx = idx;
        }
    }

    // Returns the index (idx) of lexicalEntry, if it is in the trie
    // otherwise returns -1
    public Integer search(List<String> lexicalEntry) {
        TrieNode tN = searchNode(lexicalEntry);

        if(tN != null && tN.isLeaf)
            return tN.idx;
        else
            return -1;
    }

    // Returns if there is any entry in the trie
    // that starts with the given prefix
    public boolean startsWith(List<String> prefix) {
        if(searchNode(prefix) == null)
            return false;
        else
            return true;
    }

    public TrieNode searchNode(List<String> lexicalEntry){
        Map<String, TrieNode> children = root.children;
        TrieNode t = null;
        for(String token : lexicalEntry){
            if (children.containsKey(token)) {
                t = children.get(token);
                children = t.children;
            }
            else {
                return null;
            }
        }
        return t;
    }
}

class TrieNode {
    String token;
    HashMap<String, TrieNode> children = new HashMap<String, TrieNode>();
    boolean isLeaf;
    Integer idx;

    public TrieNode() {}

    public TrieNode(String token){
        this.token = token;
    }
}

