package extraction.parse;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VerbTense {

    private static final HashMap<String, String> verbTenseMap = new HashMap<String, String>() {{
        put("VBD", "past");
        put("VBG", "present");
        put("VBN", "past");
        put("VBP", "present");
        put("VBZ", "present");
    }};

    public static Pattern ccgFeaturePattern = Pattern.compile("^\\(*S\\[.*");

    // Get the tense of the verb from the POS tag / or the CCGTag
    public static String getVerbTense(String posTag, String ccgTag) {
        String tense = verbTenseMap.containsKey(posTag) ? verbTenseMap.get(posTag) : null;
        if (tense == null) {
            Matcher matcher = ccgFeaturePattern.matcher(ccgTag);
            if (matcher.find()) {
                String ccgTagFeature = matcher.group(0).split("\\[")[1].split("]")[0];
                if (ccgTagFeature.equals("pss") || ccgTagFeature.equals("pt")) {
                    tense = "past";
                }
                if (ccgTagFeature.equals("ng")) {
                    tense = "present";
                }
            }
        }
        return tense;
    }

}
