package extraction.parse;

import java.util.Objects;

public final class LexicalGraphNode {
    public Integer nodeIndex;
    public LexicalItem lexItem;

    public LexicalGraphNode(Integer nodeIndex, LexicalItem lexItem) {
        this.nodeIndex = nodeIndex;
        this.lexItem = lexItem;
    }

    public Integer nodeIndex() {
        return nodeIndex;
    }

    public LexicalItem lexItem() {
        return lexItem;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof LexicalGraphNode) {
            LexicalGraphNode that = (LexicalGraphNode) other;
            return this.nodeIndex.equals(that.nodeIndex)
                    && this.lexItem == that.lexItem;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(nodeIndex, lexItem);
    }

    @Override
    public String toString() {
        return "(" + nodeIndex + ", " + lexItem + ")";
    }

}