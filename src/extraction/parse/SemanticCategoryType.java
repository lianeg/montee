package extraction.parse;

import com.google.common.collect.ImmutableSet;

public enum SemanticCategoryType {
    TYPE, TYPEMOD, ENTITY, COMPLEMENT, EVENT, EVENTMOD, NEGATION, CLOSED, UNIQUE, COUNT, QUESTION, UNKNOWN;

    public final static ImmutableSet<String> types = ImmutableSet.of("TYPE", "TYPEMOD", "ENTITY", "COMPLEMENT", "EVENT",
            "EVENTMOD", "NEGATION", "CLOSED", "UNIQUE", "COUNT", "QUESTION", "UNKOWN");
}