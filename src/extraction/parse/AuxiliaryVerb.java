package extraction.parse;

import java.util.*;

public class AuxiliaryVerb {

    private static HashMap<String, String> auxVerbMap = new HashMap<String, String>() {{
        put("be", "primary");
        put("do", "primary");
        put("have", "primary");
        put("can", "modal");
        put("could", "modal");
        put("dare", "modal");
        put("may", "modal");
        put("might", "modal");
        put("must", "modal");
        put("need", "modal");
        put("ought", "modal");
        put("shall", "modal");
        put("should", "modal");
        put("will", "modal");
        put("would", "modal");
    }};

    public static Set<String> futureTenseAdverbials = new HashSet<>(Arrays.asList(
            "will",
            "shall"
    ));

    public static String getAuxVerb(String lemma, SemanticCategoryType semCatType) {
        String aux = null;
        if (semCatType.equals(SemanticCategoryType.EVENT) && auxVerbMap.containsKey(lemma)) {
            aux = auxVerbMap.get(lemma);
        }
        return aux;
    }

    public static Boolean auxVerbMarksFutureTense(String lemma, SemanticCategoryType semCatType) {
        Boolean isFuture = Boolean.FALSE;
        if (semCatType.equals(SemanticCategoryType.EVENT) && futureTenseAdverbials.contains(lemma)) {
            isFuture = Boolean.TRUE;
        }
        return isFuture;
    }
}
