package extraction;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.*;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import constants.ConstantsParsing;
import extraction.SimpleSpot;

public class LinesHandler {

	int numPortionsToSkip;
	BufferedReader br;
	int lineNumber;
	ThreadPoolExecutor threadPool;

	PrintStream opJson;
	BufferedWriter opEnts;
	BufferedWriter opTimeExprs;
	BufferedWriter opModalStats; // Modal use statistics
	BufferedWriter opMainStrs;
	BufferedWriter opMainStrsOnlyNEs;
	BufferedWriter opParsedStrs;
	static CopyOnWriteArrayList<String> mainStrs;
	static CopyOnWriteArrayList<String> mainStrsOnlyNEs;
	static CopyOnWriteArrayList<String> parsedStrs;
	static CopyOnWriteArrayList<String> errStrs;

	static ArrayList<String> lines;
	static ArrayList<ArrayList<String>> spots;
	static ArrayList<ArrayList<String>> wikiNames;
	static Map<String, Integer> allEntities = new ConcurrentHashMap<>();
	static Map<String, Integer> allGens = new ConcurrentHashMap<>();
	static Map<String, Integer> allTimeExprs = new ConcurrentHashMap<>();
	static Map<String, Integer> allModalCountStats = new ConcurrentHashMap<>(); // modal_string : c_occurences , e.g. probably :: 6
	static Map<String, Integer[]> allPredModalNegStats = new ConcurrentHashMap<>(); // predicate_string : [c_mod, c_neg, c_neither, c_1, c_2, c_3, c_4, c_5]
	final int lineOffset;

	public LinesHandler(String[] args) {
		mainStrs = new CopyOnWriteArrayList<>();
		mainStrsOnlyNEs = new CopyOnWriteArrayList<>();
		parsedStrs = new CopyOnWriteArrayList<>();
		errStrs = new CopyOnWriteArrayList<>();

		lineNumber = 0;
		String parsedStrsFileName = "final_output.json";
		if (args == null) {
			try {
				br = new BufferedReader(new InputStreamReader(System.in, "UTF8"));
				// Only create relation output files if extracting relations
				if (ConstantsParsing.readCCGParsesFromFile || ConstantsParsing.onlineCCGParsing) {
					opMainStrs = new BufferedWriter(
							new OutputStreamWriter(new FileOutputStream("predArgs_gen.txt"), "UTF-8"));
					opMainStrsOnlyNEs = new BufferedWriter(
							new OutputStreamWriter(new FileOutputStream("predArgs_NEs.txt"), "UTF-8"));
				}
				opParsedStrs = new BufferedWriter(
						new OutputStreamWriter(new FileOutputStream(parsedStrsFileName), "UTF-8"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			numPortionsToSkip = -1;
		} else {
			try {
				br = new BufferedReader(new InputStreamReader(new FileInputStream(args[0]), "UTF8"));
				String f1, f2, f3;
				if (args.length > 2) {
					f1 = args[2];
					f2 = args[3];
					f3 = parsedStrsFileName;
					numPortionsToSkip = Integer.parseInt(args[1]);
				} else {
					f1 = "predArgs_gen.txt";
					f2 = "predArgs_NEs.txt";
					f3 = parsedStrsFileName;
					numPortionsToSkip = -1;
				}
				// Only create relation output files if extracting relations
				if (ConstantsParsing.readCCGParsesFromFile || ConstantsParsing.onlineCCGParsing) {
					opMainStrs = new BufferedWriter(
							new OutputStreamWriter(new FileOutputStream(f1, numPortionsToSkip > 0), "UTF-8"));
					opMainStrsOnlyNEs = new BufferedWriter(
							new OutputStreamWriter(new FileOutputStream(f2, numPortionsToSkip > 0), "UTF-8"));
				}
				opParsedStrs = new BufferedWriter(
						new OutputStreamWriter(new FileOutputStream(f3, numPortionsToSkip > 0), "UTF-8"));
				if (ConstantsParsing.convToEntityLinked) {
					int dotIdx = args[0].indexOf('.');
					String jsonName = args[0].substring(0, dotIdx) + ".json";
					opJson = new PrintStream(new File(jsonName));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		int ll = -1;
		try {
			Scanner sc = new Scanner(new File("offset.txt"));
			ll = sc.nextInt();
			sc.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		lineOffset = ll;

		System.err.println("numPortionsToSkip: " + numPortionsToSkip);

		// Only create ents.txt output file if extracting relations
		if (ConstantsParsing.readCCGParsesFromFile || ConstantsParsing.onlineCCGParsing) {
			try {
				if (numPortionsToSkip <= 0) {
					opEnts = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("ents.txt")));
				} else {
					opEnts = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("ents.txt", true)));
				}
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
		}

		// Write time expressions - initialise buffer
		try {
			if (numPortionsToSkip <= 0) {
				opTimeExprs = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("timeExprs.txt")));
			} else {
				opTimeExprs = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("timeExprs.txt", true)));
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

		// Write modal stats - initialise buffer
		try {
			if (numPortionsToSkip <= 0) {
				opModalStats= new BufferedWriter(new OutputStreamWriter(new FileOutputStream("modalStats.txt")));
			} else {
				opModalStats = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("modalStats.txt", true)));
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

		if (ConstantsParsing.convToEntityLinked) {
			spots = new ArrayList<ArrayList<String>>();
			wikiNames = new ArrayList<ArrayList<String>>();
			lines = new ArrayList<String>();
		}

		final BlockingQueue<Runnable> queue = new ArrayBlockingQueue<>(ConstantsParsing.numThreads);
		threadPool = new ThreadPoolExecutor(ConstantsParsing.numThreads, ConstantsParsing.numThreads, 600,
				TimeUnit.SECONDS, queue);
		// To silently discard rejected tasks. :add new
		// ThreadPoolExecutor.DiscardPolicy()

		threadPool.setRejectedExecutionHandler(new RejectedExecutionHandler() {
			@Override
			public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
				// this will block if the queue is full
				try {
					executor.getQueue().put(r);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});

	}

	void extractAll() throws IOException, InterruptedException {
		int mb = 1024 * 1024;
		long time0 = System.currentTimeMillis();

		int lineNumber = 0;
		String line = null;

		lineNumber = -1;
		boolean memoryExceed = false;
		int numAskedForRun = 0;

		// Output whether we're processing sentences or documents
		boolean docProcessing = (ConstantsParsing.docLevelProcessing ||ConstantsParsing.includeCoreference
				|| ConstantsParsing.useDocDateForTimeResolution) ? Boolean.TRUE : Boolean.FALSE;
		String docProcessingStr = (docProcessing) ? "Document-level processing" : "Sentence-level processing";
		System.err.println(docProcessingStr);

		// How many "lines" to process before checking memory constraints
		// 50 documents (no coref), 25 documents (with coref), or 1000 sentences
		Integer lineCheck = 1000;
		if (docProcessing) {
			lineCheck = (ConstantsParsing.includeCoreference) ? 25 : 50;
		}

		while ((line = br.readLine()) != null) {
			lineNumber++;
			try {
				if (lineNumber < lineOffset) {
					continue;
				}
				if (!docProcessing && line.length() > 100000) {
					System.err.println("very long line, not processing: " + line);
				}
				if (lineNumber % lineCheck == 0) {
					System.err.println(lineNumber);
				}

				// check memory and see if you wanna exit
				if (lineNumber % lineCheck == 0) {
					long usedMb = (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / mb;
					System.err.println("usedMB: " + usedMb);
					if (usedMb >= ConstantsParsing.maxMBallowd) {
						memoryExceed = true;
						break;
					}
				}

				if (line.trim().equals("")) {
					continue;
				}

				Runnable extractor = new PredicateArgumentExtractor(line, docProcessing);
				threadPool.execute(extractor);
				numAskedForRun++;
			} catch (Exception e) {
				System.err.println("Could not process line: ");
				System.err.println(line);
			}

			if (lineNumber % 5 == 0) {
				// Only write output if extracting relations
				if (ConstantsParsing.readCCGParsesFromFile || ConstantsParsing.onlineCCGParsing) {
					writeRelationOutput();
				}
				writeCCGParserOutput();
				if (ConstantsParsing.convToEntityLinked) {
					writeConvertedToEntityLinked();
				}
			}

			if (lineNumber % (5*lineCheck) == 0) {
				// write the current lineNumber
				PrintStream op = new PrintStream(new File("offset.txt"));
				op.println(lineNumber);
				op.close();
			}
		}

		// Check if memory has exceeded, and we should run for continue!
		if (memoryExceed) {
			// Write the current lineNumber
			PrintStream op = new PrintStream(new File("offset.txt"));
			op.println(lineNumber);
			op.close();
		} else {
			// Delete offset.txt as everything is done
			File f = new File("offset.txt");
			f.delete();
		}

		System.err.println("threadpool: " + threadPool.getActiveCount() + " " + threadPool.getPoolSize() + " "
				+ threadPool.getQueue().size());
		System.err.println("asked for run: " + numAskedForRun);
		System.err.println("num done: " + threadPool.getCompletedTaskCount());
		threadPool.shutdown();
		// Wait. Hopefully all threads are finished. If not, forget about it!
		threadPool.awaitTermination(200, TimeUnit.SECONDS);

		// Only write relation output to file if running online parsing (with EasyCCG) or reading CCG parse trees
		// from file.
		if (ConstantsParsing.readCCGParsesFromFile || ConstantsParsing.onlineCCGParsing) {
			System.err.println("after await");
			// Write relations files
			writeRelationOutput();
			System.err.println("after write output");
			// Clear buffers
			opMainStrs.close();
			opMainStrsOnlyNEs.close();
			// Write ents/gens files
			writeEnts();
			writeGens();
			writeTimeExprs();
			writeModalStats();
		}
		writeCCGParserOutput();
		opParsedStrs.close();
		if (ConstantsParsing.convToEntityLinked) {
			writeConvertedToEntityLinked();
		}
		System.err.println("all time: " + (System.currentTimeMillis() - time0));


		System.exit(0);
	}

	private void writeEnts() {
		System.err.println("in write ents: " + allEntities.size());
		for (String s : allEntities.keySet()) {
			try {
				opEnts.write(s + "::" + allEntities.get(s) + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			opEnts.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void writeGens() throws FileNotFoundException {
		System.err.println("in write gens: " + allGens.size());
		PrintWriter pr = new PrintWriter("gens.txt");
		ArrayList<SimpleSpot> sspots = new ArrayList<>();
		for (String s : allGens.keySet()) {
			sspots.add(new SimpleSpot(s, allGens.get(s)));
		}
		Collections.sort(sspots, Collections.reverseOrder());
		for (SimpleSpot ss : sspots) {
			pr.println(ss.spot + "::" + ss.count);
		}
		pr.close();
	}

	private void writeTimeExprs() {
		System.err.println("in write time expressions: " + allTimeExprs.size());
		for (String s : allTimeExprs.keySet()) {
			try {
				opTimeExprs.write(s + "::" + allTimeExprs.get(s) + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			opTimeExprs.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void writeModalStats(){
		System.err.println("in write modal stats: " + allModalCountStats.size());
		// Modal counts - header
		try {
			opModalStats.write("# Modal / conditional / counterfactual / reporting verb counts\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Modal counts - stats per modal
		for (String s : allModalCountStats.keySet()) {
			try {
				opModalStats.write(s + "::" + allModalCountStats.get(s) + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		// Predicate stats - header
		try {
			opModalStats.write("\n# Predicate stats: count, modal, att_say, att_think, conditional, counterfactual, negation, lexical_negation, 1-5 modals\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Predicate stats - stats per predicate
		Integer [] totals = new Integer[13]; // [total, c_mod, attSay, attThink, conditional, counterfactual c_neg, c_lneg, c_1, c_2, c_3, c_4, c_5]
		Arrays.fill(totals, 0); // Fill array with zeros
		for (String s : allPredModalNegStats.keySet()) {
			try {
				String predStatsString = StringUtils.join(allPredModalNegStats.get(s), "\t");
				opModalStats.write(predStatsString + "\t" + s + "\n");
				totals = Util.sumValuesInTwoArrays(totals, allPredModalNegStats.get(s));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		// Predicate stats - count of distinct predicates and total relations
		try {
			opModalStats.write("Total distinct predicates: " + allPredModalNegStats.size() + "\n");
			opModalStats.write("Total relations: " + totals[0].toString() + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Predicate stats - totals
		try {
			opModalStats.write("\n# Totals:");
			opModalStats.write("\nTotal mod::" + totals[1].toString() + "\n");
			opModalStats.write("Total att_say::" + totals[2].toString() + "\n");
			opModalStats.write("Total att_think::" + totals[3].toString() + "\n");
			opModalStats.write("Total conditional::" + totals[4].toString() + "\n");
			opModalStats.write("Total counterfactuals::" + totals[5].toString() + "\n");
			opModalStats.write("Total neg::" + totals[6].toString() + "\n");
			opModalStats.write("Total LEXICAL neg::" + totals[7].toString() + "\n");
			opModalStats.write("Total 1 modal::" + totals[8].toString() + "\n");
			opModalStats.write("Total 2 modal::" + totals[9].toString() + "\n");
			opModalStats.write("Total 3 modal::" + totals[10].toString() + "\n");
			opModalStats.write("Total 4 modal::" + totals[11].toString() + "\n");
			opModalStats.write("Total 5 modal::" + totals[12].toString() + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Close the buffer
		try {
			opModalStats.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void writeConvertedToEntityLinked() {
		while (spots.size() > 0) {
			ArrayList<String> thisSpots = spots.remove(0);
			ArrayList<String> thisWikiNames = wikiNames.remove(0);
			String line = lines.remove(0);

			JSONObject jObj = new JSONObject();
			jObj.put("s", line);
			JSONArray jsonArray = new JSONArray();
			for (int i = 0; i < thisSpots.size(); i++) {
				JSONObject candObj = new JSONObject();
				candObj.put(thisSpots.get(i), thisWikiNames.get(i));
				jsonArray.add(i, candObj);
			}
			jObj.put("a", jsonArray);
			opJson.println(jObj);
		}
	}

	private void writeRelationOutput() throws IOException {
		while (mainStrs.size() > 0) {
			String s = mainStrs.remove(0);
			opMainStrs.write(s + "\n");
		}
		while (mainStrsOnlyNEs.size() > 0) {
			String s = mainStrsOnlyNEs.remove(0);
			opMainStrsOnlyNEs.write(s + "\n");
		}
		while (errStrs.size() > 0) {
			String s = errStrs.remove(0);
		}
	}

	private void writeCCGParserOutput() throws IOException {
		while (parsedStrs.size() > 0) { // Write CCG / CoreNLP parsed output
			String s = parsedStrs.remove(0);
			opParsedStrs.write(s + "\n");
			opParsedStrs.flush(); // 20th Dec 2019: Added to help with memory build up
		}
	}

	public static void main(String[] args) throws IOException, InterruptedException {
		long t0 = System.currentTimeMillis();
		if (args.length == 0) {
			args = new String[] { "news_raw_doc_1.json" };
		}
		System.err.println("args:" + args[0]);
		LinesHandler lineHandler = new LinesHandler(args);
		lineHandler.extractAll();
		System.err.println("time: " + (System.currentTimeMillis() - t0));
	}
}
