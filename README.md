### MoNTEE

If you use MoNTEE, please cite:

Modality and Negation in Event Extraction. Sander Bijl de Vroe, Liane Guillou, Miloš Stanojević, Nick McKenna, Mark Steedman. Proceedings of the 4th Workshop on Challenges and Applications of Automated Extraction of Socio-political Events from Text (CASE 2021).


### Requirements
    Java 11
    Python 3
    RotatingCCG parser (optional)

### Instructions

Please follow the below instructions to extract binary and unary relations from text.

N.B. Each time you amend a value in constants, you will need to recompile the code.


**Step 1**: Clone the MoNTEE project.

**Step 2** (optional): If you wish to use Miloš Stanojević's RotatingCCG Parser (instead of EasyCCG), clone the following repository and follow the installation instructions: https://github.com/stanojevic/Rotating-CCG.

**Step 3**: It is assumed that you will compile the code locally and execute it remotely. Compile the Java files on your local machine and copy the automatically created bin folder to the server. The remaining steps will be carried out on the server.

**Step 4**: Follow steps 4.1 to 4.4 to parse and link the news_test.json corpus (or your own corpus with the same format) into predicate argument structure using StanfordNLP and either EasyCCG or RotatingCCG.
   
**Step 4.1**: Perform sentence splitting. The output will be sentence split documents with one entry per document. Run extraction.Util.convertReleaseToRawJson(inputJsonAddress) 1>rawJsonAddress (if necessary change the main function in extraction.Util.java and recompile the code), where inputJsonAddress should be by default "data/test/news_test.json". Run the code as 
    
    java -cp lib/*:bin extraction.Util "data/test/news_test.json" 1>news_test_raw.json

**Step 4.2**: Extract binary relations from the input json file. As the RotatingCCG parser can be slow to run on very large corpora, we provide two modes for parsing: online using EasyCCG (fast, less accurate), and offline using the RotatingCCG parser (slower, more accurate). In both modes, you will run the bash script: `prArgs.sh`. Change the input and output address as necessary.

The following applies to both online and offline CCG parsing:

The following are parameters in constants.ConstantsParsing that you might wish to change depending on the aims of your experiment:

General:
    
    numThreads (for parallel processing)
    docLevelParsing (TRUE: process documents, FALSE: process sentence by sentence)
    onlineCCGParsing (TRUE: run EasyCCG online, FALSE: use the RotatingCCG parser offline)
    easyCcgParserModelPath (Set to the directory containing the EasyCCG model)
    readCCGParsesFromFile (TRUE: read parses from file. Supercedes onlineCCGParsing)
    readCoreNLPFromFile (TRUE: read CoreNLP json objects from file)
    onlyNounOrNE (TRUE: remove pronouns in case of binary relations containing two general entities, and all unary relations)
    
Binaries:
    
    acceptGGBinary (TRUE: accept binary relations that contain two general entities. FALSE: only relations with a named entity will be included)
    includeBareCopularBinaries (TRUE: allow (be.1,be,2) washington guest (e.g. in "Washington was occasionally a guest")) 
    removeCopularBeBinary (TRUE: (treatment.1,treatment.for.2) vancomycin infection. FALSE: (be.treatment.1,be.treatment.for.2) vancomycin infection)

Unaries:
    
    writeUnaryRels (TRUE:Include unary relations in the output)
    acceptAdjectiveUnary (TRUE: remove unaries like be.funny.2 tom (where "funny" is an adjective))
    addTwoHopBinaryCopularAsUnary (TRUE: add a unary for a two-hop binary relation involving a copular. E.g. Germany is part of the EU -> be.part.of.EU.1 Germany)
    removeCopularBeUnary (TRUE: funny.1 bob. FALSE: be.funny.1 bob)

prArgs.sh settings:

    fName=news_test_raw.json
    oName1=predArgs_gen.txt (binary relations with at least one Named Entity argument, which is used in our experiments).
    oName2=predArgs_NE.txt (binary relations with two NE arguments).

***Online CCG parsing*** using EasyCCG:
    Set constants.ConstantsParsing.onlineCCGParsing to true, and constants.ConstantsParsing.easyCcgParserModelPath to the location of the EasyCCG model. Run prArgs.sh.

***Offline CCG parsing*** using the RotatingCCG parser:
    1. Ensure that python3 is available (>= 3.6) and then install / set up the demoji package:
       $ pip3 install demoji
       $ python3
       import demoji
       demoji.download_codes()
    2. StanfordNLP preprocessing: Set onlineCCGParsing, readCCGParsesFromFile, and readCoreNLPFromFile in constants.ConstantsParsing to false. Run prArgs.sh to perform StanfordNLP steps (including tokenisation, which is needed by the CCG parser).
    3. CCG parsing: Run ccg_parsing/scripts/rotatingCCG.sh. Note that you may need to run exec $SHELL first to update some environment variables from your bash profile.
    4. Relation extraction: Set both readCCGParsesFromFile and readCoreNLPFromFile in constants.ConstantsParsing to true. Amend fName variable in prArgs.sh to point to the output file from RotatingCCG.sh. Run prArgs.sh. NB. The output file from this step will be the same as from step 1 (StanfordNLP Preprocessing). If you want to retain the output if step 1, you should rename the file.

**Step 4.3**: Download and install AIDA-Light and run it over your corpus. Download link: http://download.mpi-inf.mpg.de/d5/aida/aidalight.zip

**Step 4.4**: Run extraction.Util (change function to convertPredArgsToJsonUnsorted and recompile the code):

    java -cp lib/*:bin extraction.Util predArgs_gen.txt true true 50000000 aida/news_test_linked.json 1>news_test_gen.json

Notes:

    predArgs_gen.txt: output of step 4.2.
    aida/news_test_linked.json: output of step 4.3.
    500000000 is an upper bound on the number of lines of the corpus (this might need to be changed for a very large corpus).
    
**Step 5 (optional)**: Add entity types to relations. Types provided are the level1 FIGER types. Run python command:

    python entity_typing/assign_types_to_entities.py --inFile news_test_gen.json --outFile news_test_gen_typed.json --figerMapFile data/freebase_types/entity2Types.txt 