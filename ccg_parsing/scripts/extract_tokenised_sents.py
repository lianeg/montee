#!/usr/bin/env python

##################################################################
# Extract tokenised sentences from the CoreNLP pre-prccessed
# articles (for input to the RotatingCCG parser)
#
# Liane Guillou and Sander Bijl de Vroe
# University of Edinburgh, 2021
##################################################################


import argparse
import json
import demoji

def extract_sentences(args):
    with open(args.infile, 'r', encoding='utf-8') as ifile, open(args.sentsfile, 'w', encoding='utf-8') as sfile, open(args.mapfile, 'w', encoding='utf-8') as mfile:
        # Read input - JSON format corpus 
        for line in ifile:
            if line != "null\n":
                counter = 0
                json_line = json.loads(line)
                article_id = json_line["articleId"]
                line_id = json_line["lineId"]
                sentences = json_line["mergedEntitySentences"].split("\n")
                for sent in sentences:
                    line_number = counter if line_id == -1 else line_id
                    # Replace emojis in sentence with "<EMOJI>"
                    sent_clean = demoji.replace(sent, "<EMOJI>")
                    # Write output
                    # Sentences:
                    sfile.write(sent_clean+'\n')
                    # Mapping of article/line number to sentence:
                    out_line = str(article_id) + '\t' + str(line_number)
                    mfile.write(out_line+'\n')
                    counter +=1
                

if __name__ == '__main__':

    # Initialise the argument parser
    parser = argparse.ArgumentParser()
    parser.add_argument("--infile", help="Input file (JSON format)")
    parser.add_argument("--sentsfile", help="Raw sentences output file")
    parser.add_argument("--mapfile", help="Mapping file - articleID and lineID for each sentence")

    # read arguments from the command line
    args = parser.parse_args()

    extract_sentences(args)

