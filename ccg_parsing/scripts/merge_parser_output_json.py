#!/usr/bin/env python

##################################################################
# Merge output of the RotatingCCG parser and CoreNLP
#
# Liane Guillou and Sander Bijl de Vroe
# University of Edinburgh, 2021
##################################################################


import argparse
import json


# Get a mapping of articleID, lineID to sentence number
def get_mapping(filename):
    m = {}
    a = {}
    counter = 0
    with open(filename, 'r', encoding='utf-8') as f:
        for line in f:
            elements = line.rstrip('\n').split('\t')
            article_id = int(elements[0])
            line_id = int(elements[1])
            # Get number of lines before article X
            if not article_id in a:
                a[article_id] = counter
            if article_id in m: # articleID
                if line_id in m[article_id]:
                    m[article_id][line_id].append(counter)
                else:
                    m[article_id][line_id] = [counter]
            else:
                m[article_id] = {line_id: [counter]}
            counter += 1
    return (m,a)


# Get a list of parses from the parser output file
def get_parses(filename):
    l = []
    with open(filename, 'r', encoding='utf-8') as f:
        for line in f:
            l.append(line.rstrip('\n'))
    return l


# Merge output of parser with the original file
def merge_in_parser_output(args):
    # Get mapping of article and line ID to sentence
    map_results = get_mapping(args.mapfile)
    mapping = map_results[0]
    article_offset = map_results[1]
    # Get a list of the parse trees
    parses = get_parses(args.parsefile)    
    # Merge parse trees in to the original file
    with open(args.originalfile, 'r', encoding='utf-8') as origfile, open(args.outfile, 'w' ,encoding='utf-8') as outfile:
        # Get original representation of sentence/document from JSON file
        for line in origfile:
            if line != "null\n":
                counter = 0
                json_line = json.loads(line)
                article_id = json_line["articleId"]
                if article_id in mapping:
                    line_id = json_line["lineId"]
                    # Get the parser output for the sentence(s)
                    if line_id == -1:
                        # One line per document
                        sentnums = list(mapping[article_id].keys())
                        sentnums.sort()
                        offset = article_offset[article_id]
                        startnum = sentnums[0]+offset
                        endnum = sentnums[-1]+offset+1
                        p = parses[startnum:endnum]
                        parser_out = '\n'.join(p)
                    else:
                        # One line per sentence
                        sentnum = mapping[article_id][line_id][0]
                        parser_out = parses[sentnum]
                    # Add parser output to json
                    json_line['ccgParserOutput'] = parser_out
                    # Write amended JSON to output file
                    outfile.write(json.dumps(json_line)+'\n')

                
if __name__ == '__main__':

    # Initialise the argument parser
    parser = argparse.ArgumentParser()
    parser.add_argument("--parsefile", help="Parser output (one parse tree per line)")
    parser.add_argument("--originalfile", help="Original input file (JSON format)")
    parser.add_argument("--mapfile", help="Mapping file - articleID and lineID for each sentence")
    parser.add_argument("--outfile", help="Output file (JSON format)")
    
    # read arguments from the command line
    args = parser.parse_args()

    merge_in_parser_output(args)

