#!/bin/bash

##################################################################
# Run the RotatingCCG parser over a file containing tokenised
# sentences, one sentence per line
#
# Ensure that you have the following dependencies:
# Rotating CCG parser (https://github.com/stanojevic/Rotating-CCG)
# python 3
# Python modules: allennlp, thrift
#
# Liane Guillou and Sander Bijl de Vroe
# University of Edinburgh, 2021
##################################################################


# Amend these variables as necessary
workingDir=<Montee dir>
rotatingParserDir=<RotatingCCG dir>
parserModelDir=$rotatingParserDir/models/best_model
inFile=$workingDir/<Input JSON file>
outFile=$workingDir/<Output JSON file>
processes=<N>

# Leave these variables as they are
tempDir=$workingDir/ccg_parsing/temp
partsDir=$tempDir/parts

# Make directories if they don't exist
if [ -d "$tempDir" ]; then rm -r $tempDir; fi
mkdir $tempDir
mkdir $partsDir

# Read inFile and write tokenised sentences to sentsFile
sentsFile=$tempDir/tokenisedSentences.txt
mapFile=$tempDir/mapping.txt
python3 ./ccg_parsing/scripts/extract_tokenised_sents.py --infile $inFile --sentsfile $sentsFile --mapfile $mapFile

# Split sentsFile by number of processes
subFilePrefix=$partsDir/tok_sents
totalLines=$(wc -l <${sentsFile})
((linesPerPart=($totalLines + $processes - 1) / $processes))
split -l ${linesPerPart} ${sentsFile} $subFilePrefix.

# Parse files in $partsDir using the Rotating CCG parser
rawPartFiles=$partsDir/*
process(){
    local fileName=$1;
    $rotatingParserDir/scripts/run.sh edin.ccg.MainParse \
	--model_dirs $parserModelDir \
	--beam-type simple \
    --beam-mid-parsing 1 \
	--beam-out-parsing 1 \
	--input_file $fileName \
	--output_file $fileName.trees
}

# Process files, one per process
for partsFile in $rawPartFiles; do
    process "$partsFile" &
done

wait

# Combine parsed partFiles into a single file
combParseFile=$tempDir/combined_parser_output.trees
cat $partsDir/*.trees > $combParseFile

# Add parser output to outFile
python3 ./ccg_parsing/scripts/merge_parser_output_json.py --parsefile $combParseFile --originalfile $inFile --mapfile $mapFile --outfile $outFile
